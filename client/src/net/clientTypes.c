//
// Created by danandla on 3/18/23.
//

#include <clientTypes.h>
#include <stdlib.h>
#include <string.h>

char* getHeaderStr(QueryHeader header){
    char *ret = malloc(sizeof(char) * 20);
    switch(header){
        case DBQUERY_HEADER:
            strcpy(ret, "dbQuery");
            break;
        case SELECT_NEXT_HEADER:
            strcpy(ret, "selectNext");
            break;
        case INVALID_QUERY_HEADER:
            strcpy(ret, "invalid");
            break;
        default:
            strcpy(ret, "invalid");
            break;
    }
    return ret;
}

char* getDbQueryStr(dbQueryType type){
    char *ret = malloc(sizeof(char) * 20);
    switch(type){
        case DBQUERY_SELECT:
            strcpy(ret, "select");
            break;
        case DBQUERY_SELECT_NEXT:
            strcpy(ret, "select_next");
            break;
        case INVALID_DBQUERY:
            strcpy(ret, "invalid");
            break;
        default:
            strcpy(ret, "invalid");
            break;
    }
    return ret;
}

char* getResponseStatusStr(ResponseStatus st){
    char *ret = malloc(sizeof(char) * 20);
    switch (st){
        case SERVER_OK:
            strcpy(ret, "ok");
            break;
        case SERVER_ERR:
            strcpy(ret, "error");
            break;
    }
    return ret;
}

ResponseStatus getResponseStatus(char* st){
    if(strcmp(st, "ok") == 0) return SERVER_OK;
    if(strcmp(st, "error") == 0) return SERVER_ERR;
    return SERVER_ERR;
}

char* getResponseStr(Response st){
    char *ret = malloc(sizeof(char) * 20);
    switch (st){
        case SELECT_GOOD:
            strcpy(ret, "select_good");
            break;
        case SELECT_NEXT:
            strcpy(ret, "select_next");
            break;
        case SELECT_END:
            strcpy(ret, "select_end");
            break;
        case SELECT_UNKNOWN:
            strcpy(ret, "select_unknown");
            break;
        case QUERY_ERR:
            strcpy(ret, "query err");
            break;
    }
    return ret;
}
Response getResponse(char* st){
    if(strcmp(st, "select_good") == 0) return SELECT_GOOD;
    if(strcmp(st, "select_next") == 0) return SELECT_NEXT;
    if(strcmp(st, "select_end") == 0) return SELECT_END;
    if(strcmp(st, "select_unknown") == 0) return SELECT_UNKNOWN;
    return QUERY_ERR;
}
