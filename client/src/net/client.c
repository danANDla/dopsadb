//
// Created by danandla on 3/18/23.
//

#define MAX_SIZE 4096
#define MAX_LOCAL 4096

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <json-c/json.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <client.h>
#include <jsonDecoder.h>
#include <jsonBuilder.h>

int sendJson(int fd, char* payload){
    char temp_buff[MAX_SIZE];
    if (strcpy(temp_buff, payload) == NULL) {
        perror("strcpy");
        return EXIT_FAILURE;
    }
    temp_buff[strlen(payload)] = '\0';
//    printf("sending %s\n", payload);
    if (write(fd, temp_buff, strlen(payload) + 1) == -1) {
        perror("write");
        return EXIT_FAILURE;
    }
//    printf("Written data, waiting response\n");
    return EXIT_SUCCESS;
}

int receiveJson(int fd, char* temp_buff){
    ssize_t r;
    for(;;){
        r = read(fd, temp_buff, MAX_LOCAL);
        if (r == -1) {
            perror("read");
            return EXIT_FAILURE;
        }
        if (r == 0)
            break;

        if (r > 0) break;
    }
    return EXIT_SUCCESS;
}

int receiveSelect(int fd, char* temp_buff){
    int isEnd = 0;
    while(!isEnd){
        char* payload = makeSelectNext();
        if(sendJson(fd, payload) == EXIT_FAILURE) return EXIT_FAILURE;
        free(payload);
        if(receiveJson(fd, temp_buff) == EXIT_FAILURE) return EXIT_FAILURE;
        SelectResponse selResp = parseSelectResponse(temp_buff);
        if(selResp.respStatus == SERVER_ERR){
            printf("[server error]: %s\n", selResp.data);
            free(selResp.data);
            return EXIT_FAILURE;
        }
        if (selResp.resp == SELECT_NEXT) {
            printf("data: %s\n", selResp.data);
            free(selResp.data);
        }
        else if(selResp.resp == SELECT_END) isEnd = 1;
        else {
            printf("data recieved wrong header, expected selectNext or selectEnd\n");
            EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}

int startSelect(int fd, char* temp_buff, AstSelect* astSelect){
    char* payload = dbQuery(astSelect);
    if(sendJson(fd, payload) == EXIT_FAILURE) return EXIT_FAILURE;
    free(payload);
    if(receiveJson(fd, temp_buff) == EXIT_FAILURE) return EXIT_FAILURE;
    SelectResponse selResp = parseSelectResponse(temp_buff);
    if(selResp.respStatus == SERVER_ERR){
        printf("[server error]: %s\n", selResp.data);
        free(selResp.data);
        return EXIT_FAILURE;
    }
    switch (selResp.resp){
        case SELECT_GOOD:
            return receiveSelect(fd, temp_buff);
        case SELECT_UNKNOWN:
            return EXIT_FAILURE;
        default:
            return EXIT_FAILURE;
    }
}
