%option noyywrap nodefault yylineno case-insensitive noinput nounput

%{
#include "parser.h"
#include <stdio.h>
#include <ast.h>
#include <mytypes.h>

AstNode* ast;
%}

letter [a-zA-z]
digit [0-9]
space [ \t\n]+

symbol {letter}({letter}|{digit})*
int [+-]?{digit}+
float [+-]?({digit}+[\.]?{digit}*)
str (\".*\")|(\'.*\')

%%

select { return SELECT; }
from { return FROM; }
join { return JOIN; }
on { return ON; }
where { return WHERE; }

STR { yylval.dtype = (dataType) STRING_T; return DTYPE; }
BOOL { yylval.dtype = (dataType) BOOL_T; return DTYPE; }
FLOAT { yylval.dtype = (dataType) FLOAT_T; return DTYPE; }
INT32 { yylval.dtype = (dataType) INT32_T; return DTYPE; }

[.,*();] { return yytext[0]; }

or { yylval.optype = (opType) OR; return COMPARE; }
and { yylval.optype = (opType) AND; return COMPARE; }
not { yylval.optype = (opType) NOT; return COMPARE; }
"=" { yylval.optype = (opType) EQ; return COMPARE; }
"!=" { yylval.optype = (opType) NEQ; return COMPARE; }
">" { yylval.optype = (opType) GR; return COMPARE; }
"<" { yylval.optype = (opType) LE; return COMPARE; }

true { yylval.ival = 1; return BOOL; }
false { yylval.ival = 0; return BOOL; }
{int} { yylval.ival = atoi(yytext); return INTNUM; }
{float} { yylval.fval = atof(yytext); return FLOATNUM; }
{symbol} {
    yylval.sval = strdup(yytext);
    return NAME;
}
{str} {
    yylval.sval = strdup(yytext+1);
    yylval.sval[yyleng-2] = 0;
    return STRING;
}

{space} { }

. {yyerror(ast, "[lexer] undefined character: '%s'", yytext); }

%%