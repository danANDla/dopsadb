%parse-param { AstNode** parsed_res }

%code top{
#include <stdio.h>
#include <stdarg.h>
#include "lexer.h"
}

%code requires{
#include <ast.h>
#include <sqlParser.h>
#include <mytypes.h>

void yyerror (AstNode* prased_res, char const *s, ...);
}

%union {
  int ival;
  float fval;
  char *sval;
  opType optype;
  dataType dtype;

  AstNode* nterm;
}

%token <sval> NAME
%token <sval> STRING
%token <ival> INTNUM
%token <fval> FLOATNUM
%token <ival> BOOL
%token <dtype> DTYPE
%left <optype> COMPARE

%token SELECT
%token ';'
%token JOIN
%token ON
%token FROM
%token WHERE

%token ','
%token '.'
%token '('
%token ')'
%token '*'

%nterm <nterm> column column_list columns table join join_list src_table statement value
%nterm <nterm> query query_list queries select_query res expr
%nterm <sval> table_name column_name

%start res
%%

res:
    expr{
      *parsed_res = $1;
    }
expr:
    query ';' { $$ = $1; }
;

query:
    select_query { }

select_query:
    SELECT columns FROM src_table WHERE statement {
         AstColumns* cols = $2;
         AstSrcTable* src = $4;
         AstStatement* st = $6;
         AstSelect* select = new (&_AstSelect, (int) AST_SELECT, 1, cols, src, 1, st);
         delete(cols);
         delete(src);
         delete(st);
         $$ = select;
    }
|   SELECT columns FROM src_table{
         AstColumns* cols = $2;
         AstSrcTable* src = $4;
         AstSelect* select = new (&_AstSelect, (int) AST_SELECT, 1, cols, src, 0);
         delete(cols);
         delete(src);
         $$ = select;
    }
;

src_table:
    table {
        AstTable* table = $1;
        $$ = new(& _AstSrcTable, (int) AST_SRC_TABLE, 1, table, 0);
        delete(table);
    }
|   table join_list {
        AstTable* table = $1;
        AstJoinList* jList = $2;
        $$ = new(& _AstSrcTable, (int) AST_SRC_TABLE, 1, table, 1, jList);
        delete(table);
        delete(jList);
    }
;

table:
    table_name { $$ = new(& _AstTable, (int) AST_TABLE, $1); free($1); }
;

table_name:
    NAME { $$ = $1; }
;

join_list:
    join {
        AstJoin* firstJoin = $1;
        AstJoinList* jList = new(&_AstJoinList, (int) AST_JOIN_LIST, 1,
            (int) AST_JOIN, (int) firstJoin->columnType,
            (int) AST_TABLE, firstJoin->left->name,
            (int) AST_TABLE, firstJoin->right->name,
            (int) AST_COLUMN, firstJoin->leftColumn->columnName, 1, firstJoin->leftColumn->tableName,
            (int) AST_COLUMN, firstJoin->rightColumn->columnName, 1, firstJoin->rightColumn->tableName);
        $$ = jList;
        delete(firstJoin);
    }
|   join_list join {
        AstJoinList* jList = $1;
        jList->push(jList, (AstJoin*) $2);
        $$ = jList;
        delete((AstJoin*) $2);
}
;

join:
    JOIN table ON column COMPARE column {
        AstTable* table = $2;
        AstColumn* leftColumn = $4;
        AstColumn* rightColumn = $6;

        if(!leftColumn->isTableNamed || !rightColumn->isTableNamed){
            delete(leftColumn);
            delete(rightColumn);
            delete(table);
            yyerror(table,"unsupported non-tableNamed columns in multitable query");
            return 1;
        }
        char* lefttablename;
        if(strcmp(leftColumn->tableName, table->name) == 0){
            lefttablename = rightColumn->tableName;
        } else if (strcmp(rightColumn->tableName, table->name) == 0){
            delete(leftColumn);
            delete(rightColumn);
            delete(table);
            yyerror(table,"column from joining table should be placed first");
            return 1;
        } else if(strcmp(leftColumn->tableName,rightColumn->tableName) == 0){
            delete(leftColumn);
            delete(rightColumn);
            delete(table);
            yyerror(table,"columns in the join are from same table");
            return 1;
        } else{
            delete(leftColumn);
            delete(rightColumn);
            delete(table);
            yyerror(table,"join condition doesn't use column from joining table");
            return 1;
        }
        $$ = new(& _AstJoin, (int) AST_JOIN, (int) INT32_T,
                 (int) AST_TABLE, table->name,
                 (int) AST_TABLE, lefttablename,
                 (int) AST_COLUMN, leftColumn->columnName, 1, leftColumn->tableName,
                 (int) AST_COLUMN, rightColumn->columnName, 1, rightColumn->tableName
                 );
        delete(table);
        delete(leftColumn);
        delete(rightColumn);
    }
;

columns:
    '*' { $$ = new(& _AstColumns, (int) AST_COLUMNS, 1, 1); }
|   column_list {
        AstColumnList* colList = $1;
        $$ = new(& _AstColumns, (int) AST_COLUMNS, 1, 0, colList);
        delete(colList);
    }
;

column_list:
    column {
        AstColumn* col = $1;
        if(col->isTableNamed){
            $$ = new (& _AstColumnList, (int) AST_COLUMN_LIST, 1,
                      (int) AST_COLUMN, col->columnName, 1, col->tableName);
        } else {
            $$ = new (& _AstColumnList, (int) AST_COLUMN_LIST, 1,
                      (int) AST_COLUMN, col->columnName, 0);
        }
        delete(col);
    }
| column_list ',' column {
        AstColumnList* colList = $1;
        AstColumn* col = $3;
        colList->push(colList, col);
        delete(col);
        $$ = $1;
    }
;

column:
    column_name '.' table_name { $$ = new (&_AstColumn, (int) AST_COLUMN, $1, 1, $3); free($1); free($3); }
|   column_name { $$ = new (&_AstColumn, (int) AST_COLUMN, $1, 0); free($1); }
;

column_name:
    NAME { $$ = $1; }
;


statement:
    value {
        AstValue* val = $1;
        $$ = new(&_AstStatementImm, AST_STATEMENT, IMMVAL, val);
        delete(val);
    }
|   column {
        AstColumn* col = $1;
        if(col->isTableNamed){
            $$ = new(&_AstStatementColumn, AST_STATEMENT, COLUMN, AST_COLUMN, col->columnName, 1, col->tableName);
        } else{
            $$ = new(&_AstStatementColumn, AST_STATEMENT, COLUMN, AST_COLUMN, col->columnName, 0);
        }
        delete(col);
    }
|   statement COMPARE statement {
        AstStatement* lSt = $1;
        AstStatement* rSt = $3;
        $$ = new(&_AstStatementBinary, AST_STATEMENT, BINARY, 1, $2, lSt, rSt);
        delete(lSt);
        delete(rSt);
    }
|   COMPARE statement{
        AstStatement* st = $2;
        $$ = new(&_AstStatementUnary, AST_STATEMENT, UNARY, 1, $1, st);
        delete(st);
    }
| '(' statement ')'{
        $$ = $2;
    }
;

value:
    INTNUM { $$ = new(&_AstValue, AST_VALUE, INT32_T, $1); }
|   FLOATNUM { $$ = new(&_AstValue, AST_VALUE, FLOAT_T, $1); }
|   BOOL { $$ = new(&_AstValue, AST_VALUE, BOOL_T, $1); }
|   STRING { $$ = new(&_AstValue, AST_VALUE, STRING_T, $1); free($1); }
;


%%

void yyerror (AstNode* parsed_res, char const *s, ...){
    extern int yylineno;
    fprintf(stderr, "in %d line error: %s\n", yylineno, s);
}
