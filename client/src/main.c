//
// Created by danandla on 3/17/23.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <json-c/json.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <jsonBuilder.h>
#include <sqlParser.h>
#include <client.h>

#define MAX_SIZE 4096
#define MAX_LOCAL 4096

int main() {
    int fd = 0;
    struct sockaddr_in demoserverAddr;

    fd = socket(AF_INET, SOCK_STREAM, 0);

    if (fd < 0) {
        printf("Error : Could not create socket\n");
        return 1;
    } else {
        demoserverAddr.sin_family = AF_INET;
        demoserverAddr.sin_port = htons(8888);
        demoserverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
        memset(demoserverAddr.sin_zero, '\0', sizeof(demoserverAddr.sin_zero));
    }


    bool isConnected = false;
    while(!isConnected){
        sleep(2);
        if (connect(fd, (const struct sockaddr *) &demoserverAddr, sizeof(demoserverAddr)) < 0) {
            printf("ERROR connecting to server\n");
        } else{
            isConnected = true;
        }
    }

//    char query[] = "select cocktailName.cocktails, ingredientName.ingredients, amount.recipe from cocktails join recipes on id.cocktails=cocktailId.recipes join ingredients on id.ingredients = ingredientId.recipes where price.cocktails > 100 and price.cocktails < 400;";
//    char query[] = "select * from recipes join cocktails on id.cocktails = cocktailId.recipes;";
//    char query[] = "select * from recipes join cocktails on id.cocktails = cocktailId.recipes where cocktailName.cocktails = \"daiquiri\";";
//    char query[] = "select * from recipes join cocktails on id.cocktails = cocktailId.recipes where (cocktailName.cocktails = \"daiquiri\") and (amount.recipes < 50.0);";
    char str[MAX_LOCAL];
    while (fgets(str, MAX_LOCAL, stdin)) {
        if (strcmp(str, "exit\n") == 0) return 0;
        AstNode *astNode;
        int code = parse_query(str, &astNode);
        if (!code) {
            bool isOk = false;
            switch (astNode->type){
                case AST_SELECT:{
                    isOk = true;
                    char temp_buff[MAX_SIZE];
                    startSelect(fd, temp_buff, (AstSelect*) astNode);
                    break;
                }
                default:{
                    fprintf(stderr, "incorrect query type to send\n");
                    break;
                }
            }
            callDelete(astNode);
            if(!isOk) continue;
        }
    }
    return EXIT_SUCCESS;
}

