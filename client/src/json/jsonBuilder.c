//
// Created by danandla on 3/17/23.
//

#include <json-c/json.h>
#include <jsonBuilder.h>
#include <clientTypes.h>
#include <string.h>
#include <ast.h>
#include <inttypes.h>
#include <stdio.h>

json_object *columnToJson(AstColumn *col) {
    json_object *column = json_object_new_object();
    json_object_object_add(column, "name", json_object_new_string(col->columnName));
    if (col->isTableNamed)
        json_object_object_add(column, "table", json_object_new_string(col->tableName));
    return column;
}

json_object *tableToJson(AstTable *src) {
    json_object *table = json_object_new_object();
    json_object *name = json_object_new_string(src->name);
    json_object_object_add(table, "name", name);
    return table;
}

json_object *joinToJson(AstJoin *join) {
    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, "leftTable", tableToJson(join->left));
    json_object_object_add(jobj, "leftColumn", columnToJson(join->leftColumn));
    json_object_object_add(jobj, "rightTable", tableToJson(join->right));
    json_object_object_add(jobj, "rightColumn", columnToJson(join->rightColumn));
    char* colTypeStr = getDataTypeStr(join->columnType);
    json_object_object_add(jobj, "columnType", json_object_new_string(colTypeStr));
    free(colTypeStr);
    return jobj;
}

json_object *joinListToJson(AstJoinList *jList) {
    json_object *jobj = json_object_new_object();
    json_object *jarray = json_object_new_array();
    for (uint16_t i = 0; i < jList->joinsNumber; ++i) {
        json_object_array_add(jarray, joinToJson(jList->list[i]));
    }
    json_object_object_add(jobj, "joins", jarray);
    return jobj;
}

json_object *joinListToJarray(AstJoinList *jList) {
    json_object *jarray = json_object_new_array();
    for (uint16_t i = 0; i < jList->joinsNumber; ++i) {
        json_object_array_add(jarray, joinToJson(jList->list[i]));
    }
    return jarray;
}


json_object *srcTableToJson(AstSrcTable *src) {
    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, "sourceTable", tableToJson(src->src));
    if (src->isJoin)
        json_object_object_add(jobj, "joins", joinListToJarray(src->joinList));
    return jobj;
}

json_object *columnListToJson(AstColumnList *cList) {
    json_object *jobj = json_object_new_object();
    json_object *jarray = json_object_new_array();
    for (uint16_t i = 0; i < cList->columnsNumber; ++i) {
        json_object_array_add(jarray, columnToJson(cList->list[i]));
    }
    json_object_object_add(jobj, "columns", jarray);
    return jobj;
}

json_object *columnListToJarray(AstColumnList *cList) {
    json_object *jarray = json_object_new_array();
    for (uint16_t i = 0; i < cList->columnsNumber; ++i) {
        json_object_array_add(jarray, columnToJson(cList->list[i]));
    }
    return jarray;
}


json_object *columnsToJson(AstColumns *columns) {
    json_object *jobj = json_object_new_object();
    if (columns->isAll) {
        json_object_object_add(jobj, "isAll", json_object_new_string("yes"));
    } else {
        json_object_object_add(jobj, "isAll", json_object_new_string("no"));
        json_object_object_add(jobj, "columnList", columnListToJarray(columns->columns));
    }
    return jobj;
}

json_object *immvalToJson(AstStatementImm *st) {
    json_object *jobj = json_object_new_object();
    char* dtypeStr = getDataTypeStr(st->value->dtype);
    json_object_object_add(jobj, "dtype", json_object_new_string(dtypeStr));
    free(dtypeStr);
    switch (st->value->dtype) {
        case STRING_T: {
            char *immStr = getImmDataStr(st->value->dtype, st->value->data);
            json_object *jstring = json_object_new_string(immStr);
            free(immStr);
            json_object_object_add(jobj, "data", jstring);
            break;
        }
        case INT32_T: {
            int n = *(int *) st->value->data;
            json_object *jint = json_object_new_int(n);
            json_object_object_add(jobj, "data", jint);
            break;
        }
        case FLOAT_T: {
            float f = *(float *) st->value->data;
            json_object *jdouble = json_object_new_double(f);
            json_object_object_add(jobj, "data", jdouble);
            break;
        }
        case BOOL_T: {
            int n = *(int *) st->value->data;
            json_object *jboolean = json_object_new_boolean(n);
            json_object_object_add(jobj, "data", jboolean);
            break;
        }
    }
    return jobj;
}

json_object *columnStToJson(AstStatementColumn *st) {
    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, "data", columnToJson(st->column));
    return jobj;
}

json_object *binaryToJson(AstStatementBinary *st);

json_object *unaryToJson(AstStatementUnary *st) {
    json_object *jobj = json_object_new_object();
    char *opType = getOpTypeStr(st->operation);
    json_object_object_add(jobj, "operation", json_object_new_string(opType));
    free(opType);
    json_object *operand;
    switch (st->operand->type) {
        case IMMVAL:
            operand = immvalToJson(st->operand);
            break;
        case COLUMN:
            operand = columnStToJson(st->operand);
            break;
        case UNARY:
            operand = unaryToJson(st->operand);
            break;
        case BINARY:
            operand = binaryToJson(st->operand);
            break;
    }
    char *stType = getStatementTypeStr(st->operand->type);
    json_object_object_add(jobj, "operandType", json_object_new_string(stType));
    free(stType);
    json_object_object_add(jobj, "operand", operand);
    return jobj;
}

json_object *binaryToJson(AstStatementBinary *st) {
    json_object *jobj = json_object_new_object();
    char *opType = getOpTypeStr(st->operation);
    json_object_object_add(jobj, "operation", json_object_new_string(opType));
    free(opType);
    json_object *loperand;
    switch (st->leftOperand->type) {
        case IMMVAL:
            loperand = immvalToJson(st->leftOperand);
            break;
        case COLUMN:
            loperand = columnStToJson(st->leftOperand);
            break;
        case UNARY:
            loperand = unaryToJson(st->leftOperand);
            break;
        case BINARY:
            loperand = binaryToJson(st->leftOperand);
            break;
    }
    char *lstType = getStatementTypeStr(st->leftOperand->type);
    json_object_object_add(jobj, "leftOperandType", json_object_new_string(lstType));
    free(lstType);
    json_object_object_add(jobj, "leftOperand", loperand);

    json_object *roperand;
    switch (st->rightOperand->type) {
        case IMMVAL:
            roperand = immvalToJson(st->rightOperand);
            break;
        case COLUMN:
            roperand = columnStToJson(st->rightOperand);
            break;
        case UNARY:
            roperand = unaryToJson(st->rightOperand);
            break;
        case BINARY:
            roperand = binaryToJson(st->rightOperand);
            break;
    }
    char *rstType = getStatementTypeStr(st->rightOperand->type);
    json_object_object_add(jobj, "rightOperandType", json_object_new_string(rstType));
    free(rstType);
    json_object_object_add(jobj, "rightOperand", roperand);
    return jobj;
}

json_object *statementToJson(AstStatement *st) {
    json_object *jobj = json_object_new_object();
    json_object *statement;
    switch (st->type) {
        case IMMVAL:
            statement = immvalToJson(st);
            break;
        case COLUMN:
            statement = columnStToJson(st);
            break;
        case UNARY:
            statement = unaryToJson(st);
            break;
        case BINARY:
            statement = binaryToJson(st);
            break;
    }

    char *stType = getStatementTypeStr(st->type);
    json_object_object_add(jobj, "statementType", json_object_new_string(stType));
    free(stType);
    json_object_object_add(jobj, "statement", statement);
    return jobj;
}

json_object *selectToJson(AstSelect *select) {
    json_object *jobj = json_object_new_object();
    json_object *qname = json_object_new_string("select");
    json_object_object_add(jobj, "query", qname);
    json_object_object_add(jobj, "columns", columnsToJson(select->columns));
    json_object_object_add(jobj, "src", srcTableToJson(select->srcTable));
    if (select->hasFilter) {
        json_object_object_add(jobj, "filter", statementToJson(select->filter));
    }
    return jobj;
}

char *convertAstToJson(AstNode *node) {
    /*Creating a json object*/
    json_object *jobj;

    switch (node->type) {
        case AST_SELECT:
            jobj = selectToJson(node);
            break;
        default:
            printf(stderr, "can't send non query");
            break;
    }

    char *out = json_object_to_json_string(jobj);
    return out;
}

char *dbQuery(AstNode *node) {
    json_object *jobj = json_object_new_object();
    char *headerStr = getHeaderStr(DBQUERY_HEADER);
    json_object_object_add(jobj, "method", json_object_new_string(headerStr));
    free(headerStr);
    switch (node->type) {
        case AST_SELECT: {
            json_object_object_add(jobj, "body", selectToJson(node));
            break;
        }
        default:
            printf(stderr, "can't send non query");
            break;
    }
    const char *outj = json_object_to_json_string(jobj);
    char *out = malloc(sizeof(char) * (strlen(outj) + 1));
    strcpy(out, outj);
    json_object_put(jobj);
    return out;
}

char *makeSelectNext() {
    json_object *jobj = json_object_new_object();
    char *headerStr = getHeaderStr(SELECT_NEXT_HEADER);
    json_object_object_add(jobj, "method", json_object_new_string(headerStr));
    free(headerStr);
    const char *outj = json_object_to_json_string(jobj);
    char *out = malloc(sizeof(char) * (strlen(outj) + 1));
    strcpy(out, outj);
    json_object_put(jobj);
    return out;
}
