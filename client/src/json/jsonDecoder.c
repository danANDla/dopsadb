//
// Created by danandla on 3/18/23.
//


#include <jsonDecoder.h>
#include <json-c/json.h>
#include <string.h>
#include <stdio.h>

#define MAX_DATA 4096

json_object *parseJson(char *jsonString) {
//    json_object *jobj = json_object_new_object();
//    json_object *jparsed = json_object_get(json_tokener_parse(jsonString));
//    json_object_object_add(jobj, "parsed", jparsed);
    return json_tokener_parse(jsonString);
}

SelectResponse parseSelectResponse(char *json) {
    json_object *jobj = json_tokener_parse(json);

    json_object *jst = json_object_object_get(jobj, "status");
    const char *jstStr = json_object_get_string(jst);
    ResponseStatus respSt = getResponseStatus(jstStr);

    if (respSt == SERVER_ERR) {
        json_object *jerr = json_object_object_get(jobj, "errMsg");
        const char *err = json_object_get_string(jerr);
        char *error = malloc(sizeof(char) * (strlen(err) + 1));
        strcpy(error, err);
        SelectResponse ret = {.respStatus = SERVER_ERR, .resp = QUERY_ERR, .data = error};
        json_object_put(jobj);
        return ret;
    }

    json_object *jtype = json_object_object_get(jobj, "type");
    Response resp = getResponse(json_object_get_string(jtype));

    json_object *jdata = json_object_object_get(jobj, "data");
    char *data;

    SelectResponse selResp;
    switch (resp) {
        case SELECT_GOOD:
            selResp = (SelectResponse) {.resp = SELECT_GOOD, .data = NULL};
            break;
        case SELECT_END:
            selResp = (SelectResponse) {.resp = SELECT_END, .data = NULL};
            break;
        case SELECT_NEXT:
            data = malloc(sizeof(char) * MAX_DATA);
            strcpy(data, json_object_get_string(jdata));
            selResp = (SelectResponse) {.resp = SELECT_NEXT, .data = data};
            break;
        default:
            selResp = (SelectResponse) {.resp = SELECT_UNKNOWN, .data = NULL};
            break;
    }
    json_object_put(jobj);
    return selResp;
}
