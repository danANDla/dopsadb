//
// Created by danandla on 3/18/23.
//

#ifndef DOPSADB_CLIENT_H
#define DOPSADB_CLIENT_H

#include <ast.h>

int startSelect(int, char*, AstSelect*);

#endif //DOPSADB_CLIENT_H
