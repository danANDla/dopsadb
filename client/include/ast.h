//
// Created by danandla on 3/15/23.
//

#ifndef SQLPARSER_SQLTREE_H
#define SQLPARSER_SQLTREE_H

#include <stdbool.h>
#include <inttypes.h>
#include <stdarg.h>
#include <assert.h>
#include <stdlib.h>
#include <ooc.h>
#include <mytypes.h>


/* -----------------------------------------------------------------
 * AST base node
 */


struct AstNode;
typedef char*(*fpToString)(struct AstNode*);
typedef char*(*fpToStringExtend)(struct AstNode*, const char *);

typedef struct AstNode {
    const void *class;
    astType type;
    fpToString toString;
    fpToStringExtend toStringExtend;
} AstNode;

void AstNode_ctor(void *_self, va_list *app);
void AstNode_dtor(void *_self);

static const struct Class _AstNode = {
        .size = sizeof (AstNode),
        .ctor = AstNode_ctor,
        .dtor = AstNode_dtor
};


/* -----------------------------------------------------------------
 * AST column
 */

typedef struct AstColumn {
    AstNode base;
    bool isTableNamed;
    char *columnName;
    char *tableName;
    fpToString toString;
} AstColumn;

void AstColumn_ctor(void *_self, va_list *app);
void* AstColumn_clone(const void*_self);
void AstColumn_dtor(void *_self);

static const struct Class _AstColumn = {
        .size = sizeof (AstColumn),
        .ctor = AstColumn_ctor,
        .dtor = AstColumn_dtor,
        .clone = AstColumn_clone
};

/* -----------------------------------------------------------------
 * AST column list
 */

struct AstColumnList;

typedef void (*fpAstColumnListPush) (struct AstColumnList*, AstColumn*);

typedef struct AstColumnList {
    AstNode base;
    uint16_t columnsNumber;
    AstColumn **list;
    fpAstColumnListPush push;
    fpToString toString;
} AstColumnList;

void AstColumnList_ctor(void *_self, va_list *app);
void AstColumnListPush(AstColumnList *self, AstColumn *newColumn);
void *AstColumnList_clone(void *_self);
void AstColumnList_dtor(void *_self);

static const struct Class _AstColumnList = {
        .size = sizeof (AstColumnList),
        .ctor = AstColumnList_ctor,
        .dtor = AstColumnList_dtor,
        .clone = AstColumnList_clone
};

/* -----------------------------------------------------------------
 * AST columns: column list wrapper | allColumns flag (*)
 */

typedef struct AstColumns {
    AstNode base;
    bool isAll;
    AstColumnList* columns;
    fpToString toString;
} AstColumns;

void AstColumns_ctor(void *_self, va_list *app);
void* AstColumns_clone(const void *_self);
void AstColumns_dtor(void *_self);

static const struct Class _AstColumns = {
        .size = sizeof (AstColumns),
        .ctor = AstColumns_ctor,
        .dtor = AstColumns_dtor,
        .clone = AstColumns_clone
};

/* -----------------------------------------------------------------
 * AST table
 */

typedef struct AstTable {
    AstNode base;
    char *name;
    fpToString toString;
} AstTable;

void AstTable_ctor(void *_self, va_list *app);
void* AstTable_clone(void *_self);
void AstTable_dtor(void *_self);

static const struct Class _AstTable = {
        .size = sizeof (AstTable),
        .ctor = AstTable_ctor,
        .dtor = AstTable_dtor,
        .clone = AstTable_clone
};

/* -----------------------------------------------------------------
 * AST join
 */

typedef struct AstJoin {
    AstNode base;

    AstTable* left;
    AstTable* right;

    AstColumn* leftColumn;
    AstColumn* rightColumn;
    dataType columnType;

    fpToString toString;
} AstJoin;

void AstJoin_ctor(void *_self, va_list *app);
void* AstJoin_clone(const void*_self);
void AstJoin_dtor(void *_self);

static const struct Class _AstJoin = {
        .size = sizeof (AstJoin),
        .ctor = AstJoin_ctor,
        .dtor = AstJoin_dtor,
        .clone = AstJoin_clone
};

/* -----------------------------------------------------------------
 * AST joins list
 */

struct AstJoinList;
typedef void (*fpAstJoinListPush)(struct AstJoinList *self, AstJoin *newJoin);

typedef struct AstJoinList {
    AstNode base;
    uint16_t joinsNumber;
    AstJoin **list;
    fpAstJoinListPush push;
    fpToString toString;
} AstJoinList;

void AstJoinList_ctor(void *_self, va_list *app);
void* AstJoinList_clone(const void*_self);
void AstJoinList_dtor(void *_self);

static const struct Class _AstJoinList = {
        .size = sizeof (AstJoinList),
        .ctor = AstJoinList_ctor,
        .dtor = AstJoinList_dtor,
        .clone = AstJoinList_clone,
};

/* -----------------------------------------------------------------
 * AST source table
 */

typedef struct AstSrcTable {
    AstNode base;
    AstTable* src;
    bool isJoin;
    AstJoinList *joinList;
    fpToString toString;
} AstSrcTable;

void AstSrcTable_ctor(void *_self, va_list *app);
void *AstSrcTable_clone(void *_self);
void AstSrcTable_dtor(void *_self);

static const struct Class _AstSrcTable = {
        .size = sizeof (AstSrcTable),
        .ctor = AstSrcTable_ctor,
        .dtor = AstSrcTable_dtor,
        .clone = AstSrcTable_clone
};

/* -----------------------------------------------------------------
 * AST immValue
 */
typedef struct AstValue{
    AstNode base;
    dataType dtype;
    void *data;
    fpToString toString;
} AstValue;

void AstValue_ctor(void *_self, va_list *app);
void* AstValue_clone(void *_self);
void AstValue_dtor(void *_self);
char *getImmDataStr(dataType dtype, void *d);

static const struct Class _AstValue = {
        .size = sizeof (AstValue),
        .ctor = AstValue_ctor,
        .dtor = AstValue_dtor,
        .clone = AstValue_clone
};

/* -----------------------------------------------------------------
 * AST statement
 */
typedef struct AstStatement {
    AstNode base;
    statementType type;
    fpToString toString;
} AstStatement;

void AstStatement_ctor(void *_self, va_list *app);
void AstStatement_dtor(void *_self);

static const struct Class _AstStatement = {
        .size = sizeof (AstStatement),
        .ctor = AstStatement_ctor,
        .dtor = AstStatement_dtor,
};

/* -----------------------------------------------------------------
 * AST statement imm
 */
typedef struct AstStatementImm {
    AstStatement base;
    AstValue* value;
    fpToString toString;
} AstStatementImm;

void AstStatementImm_ctor(void *_self, va_list *app);
void* AstStatementImm_clone(void *_self);
void AstStatementImm_dtor(void *_self);

static const struct Class _AstStatementImm = {
        .size = sizeof (AstStatementImm),
        .ctor = AstStatementImm_ctor,
        .dtor = AstStatementImm_dtor,
        .clone = AstStatementImm_clone
};

/* -----------------------------------------------------------------
 * AST statement column
 */
typedef struct AstStatementColumn {
    AstStatement base;
    AstColumn* column;
    fpToString toString;
} AstStatementColumn;

void AstStatementColumn_ctor(void *_self, va_list *app);
void* AstStatementColumn_clone(void *_self);
void AstStatementColumn_dtor(void *_self);

static const struct Class _AstStatementColumn = {
        .size = sizeof (AstStatementColumn),
        .ctor = AstStatementColumn_ctor,
        .dtor = AstStatementColumn_dtor,
        .clone = AstStatementColumn_clone
};

/* -----------------------------------------------------------------
 * AST statement unary
 */
typedef struct AstStatementUnary {
    AstStatement base;
    opType operation;
    AstStatement* operand;
    fpToString toString;
} AstStatementUnary;

void AstStatementUnary_ctor(void *_self, va_list *app);
void* AstStatementUnary_clone(void *_self);
void AstStatementUnary_dtor(void *_self);

static const struct Class _AstStatementUnary = {
        .size = sizeof (AstStatementUnary),
        .ctor = AstStatementUnary_ctor,
        .dtor = AstStatementUnary_dtor,
        .clone = AstStatementUnary_clone
};

/* -----------------------------------------------------------------
 * AST statement binary
 */
typedef struct AstStatementBinary {
    AstStatement base;
    opType operation;
    AstStatement *leftOperand;
    AstStatement *rightOperand;
    fpToString toString;
} AstStatementBinary;

void AstStatementBinary_ctor(void *_self, va_list *app);
void* AstStatementBinary_clone(void *_self);
void AstStatementBinary_dtor(void *_self);

static const struct Class _AstStatementBinary = {
        .size = sizeof (AstStatementBinary),
        .ctor = AstStatementBinary_ctor,
        .dtor = AstStatementBinary_dtor,
        .clone = AstStatementBinary_clone
};

/* -----------------------------------------------------------------
 * AST select
 */
typedef struct AstSelect {
    AstNode base;
    bool hasFilter;

    AstSrcTable* srcTable;
    AstColumns* columns;
    AstStatement* filter;

    fpToString toString;
} AstSelect;

void AstSelect_ctor(void *_self, va_list *app);
void AstSelect_dtor(void *_self);
char *AstSelect_toString(AstSelect *self);

static const struct Class _AstSelect = {
        .size = sizeof (AstSelect),
        .ctor = AstSelect_ctor,
        .dtor = AstSelect_dtor,
};

char* callToString(AstNode* ast);
char *callDelete(AstNode *ast);
#endif //SQLPARSER_SQLTREE_H
