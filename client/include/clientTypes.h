//
// Created by danandla on 3/18/23.
//

#ifndef DOPSADB_CLIENTTYPES_H
#define DOPSADB_CLIENTTYPES_H

typedef enum QueryHeader{
    DBQUERY_HEADER = 0,
    SELECT_NEXT_HEADER,
    INVALID_QUERY_HEADER
}QueryHeader;

typedef enum dbQueryType{
    DBQUERY_SELECT = 0,
    DBQUERY_SELECT_NEXT,
    INVALID_DBQUERY
}dbQueryType;

typedef enum ResponseStatus{
    SERVER_OK,
    SERVER_ERR
}ResponseStatus;

typedef enum Response{
    SELECT_GOOD,
    SELECT_NEXT,
    SELECT_END,
    SELECT_UNKNOWN,
    QUERY_ERR
}Response;

typedef struct SelectResponse{
    ResponseStatus respStatus;
    Response resp;
    char* data;
}SelectResponse;

char* getHeaderStr(QueryHeader);
char* getDbQueryStr(dbQueryType);
char* getResponseStatusStr(ResponseStatus);
char* getResponseStr(Response);

Response getResponse(char* st);
ResponseStatus getResponseStatus(char* st);
#endif //DOPSADB_CLIENTTYPES_H
