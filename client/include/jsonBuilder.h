//
// Created by danandla on 3/17/23.
//

#ifndef DOPSADB_JSONBUILDER_H
#define DOPSADB_JSONBUILDER_H

#include <ast.h>

char* convertAstToJson(AstNode* node);
char* dbQuery(AstNode* node);
char* makeSelectNext();

#endif //DOPSADB_JSONBUILDER_H
