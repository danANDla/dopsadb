//
// Created by danandla on 3/18/23.
//

#ifndef DOPSADB_JSONDECODER_H
#define DOPSADB_JSONDECODER_H

#include <clientTypes.h>

SelectResponse parseSelectResponse(char* json);

#endif //DOPSADB_JSONDECODER_H
