//
// Created by danandla on 3/9/23.
//

#ifndef DBAPP_BASICOPERATIONS_H
#define DBAPP_BASICOPERATIONS_H

#include <inttypes.h>
#include <schema_util.h>
#include <test.h>

enum test_status init_db(char *fname, uint16_t pgSize);
enum test_status seeSuperHeader(char *fname);
enum test_status getTable(char *fname, uint64_t id);
uint32_t getNumberOfTuplesInTable(int64_t tablePos, FILE* out);
enum test_status doSelectQuery(char *fname, uint64_t id);
enum test_status doUpdateQuery(char *fname, uint64_t id);
enum test_status doDeleteQuery(char *fname, uint64_t id);
enum test_status joinCheck(char *fname);
enum test_status joinTreeCheck(char *fname);
enum test_status fill3TablesCocktails();
enum test_status fill3TablesFootball();
enum test_status fillTablesNorthwind();
char * getTestResPath(char* fname);
char * getTestDataPath(char* fname);

enum test_status readDataFile(char *fname, struct _tbSchema *schema, void ***arr, uint32_t *n);
void addItemToTable(void *item, FILE *out, uint64_t id, struct _tbSchema *schema, int withId);
void destructItem(struct _tbSchema *schema, void *item);
#endif //DBAPP_BASICOPERATIONS_H
