//
// Created by danandla on 2/11/23.
//

#ifndef DBAPP_STRING_UTIL_H
#define DBAPP_STRING_UTIL_H

#include <page_writer.h>
#include "table_internals.h"

enum readStatus readString(FILE *in, struct tid id, char** str);
enum writeStatus writeString(FILE *out, char *str, struct tid *id);

#endif //DBAPP_STRING_UTIL_H
