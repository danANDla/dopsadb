//
// Created by danandla on 12/13/22.
//

#ifndef DBAPP_PAGE_WRITER_H
#define DBAPP_PAGE_WRITER_H

#include <inttypes.h>
#include <fileUtil.h>
#include "page_internals.h"
#include "table_internals.h"

enum writeStatus {
    WRITE_OK = 0,
    WRITE_STRING_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_PADDING_ERROR,
    WRITE_ERROR
};

enum readStatus {
    READ_OK = 0,
    READ_STRING_ERROR,
    READ_ERROR
};

void printPageHeader(struct dataPageHeader *);
void printSuperHeader(struct superHeader *);
enum readStatus readSuperHeader(FILE *const in, struct superHeader *header);
enum readStatus readFreePageHeader(FILE *in, const int64_t pagePos, struct freePageHeader *freePage);
enum readStatus readDataPageHeader(FILE *in, const int64_t pagePos, struct dataPageHeader *dataPage);
enum writeStatus initDB(FILE *out, uint16_t pgSize);
enum writeStatus updateSuperHeader(FILE *out, const struct superHeader *const super);
enum writeStatus updateDataPageHeader(FILE *out, int64_t dataPagePos, const struct dataPageHeader *const dataPage);
enum writeStatus getFreePage(FILE *file, int64_t *freePagePos);
enum writeStatus allocFreePage(FILE *file, int64_t *freePagePos);
enum writeStatus freeDataPage(FILE *out, const int64_t *dataPagePos);
enum writeStatus addDataToPage(FILE* out, int64_t pagePos, size_t dataSize, void * data, struct tid* id);
void printAllDataOnPage(FILE* out, const int64_t pagePos);

#endif //DBAPP_PAGE_WRITER_H
