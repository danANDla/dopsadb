//
// Created by danandla on 2/14/23.
//

#ifndef DBAPP_QUERY_H
#define DBAPP_QUERY_H

#include <inttypes.h>

#include <iter.h>
#include <schema_util.h>

enum condConnective{
    AND_CONNECTIVE,
    OR_CONNECTIVE,
    CMP_CONNECTIVE,
    JOIN_CONNECTIVE
};

enum cmpType{
    CMP_EQUAL,
    CMP_NEQUAL,
    CMP_GREATER,
    CMP_LOWER
};

enum queryAction{
    ACTION_SELECT,
    ACTION_UPDATE,
    ACTION_DELETE
};

//typedef struct dbCond{
//    enum condConnective connective;
//    struct dbCond* left;
//    struct dbCond* right;
//    void* tableRef;
//    void* fieldRef;
//    void* value;
//} dbCond;

typedef struct atom{
    enum condConnective connective;
    enum cmpType cmp;
    struct atom* leftOp;
    struct atom* rightOp;

    uint16_t fieldN;
    enum data_type type;
    void* data;
} atom;

typedef struct dbQuery {
    uint16_t **fields;
    tupleIterator* srcTable;
    atom condition;
    enum queryAction action;
    void* updateData;
} dbQuery;

void doQuery(dbQuery query);
#endif //DBAPP_QUERY_H
