//
// Created by danandla on 3/9/23.
//

#ifndef DBAPP_PERFORMANCETEST_H
#define DBAPP_PERFORMANCETEST_H

#include <inttypes.h>
#include <schema_util.h>

enum test_status insertTest(char *fileName, struct _tbSchema *schema, uint32_t times, int32_t* id);
enum test_status separateInsertTest(void ***arr, uint32_t arrSz,  struct _tbSchema *schema, uint32_t times, int32_t * tableId, float* testTime);
enum test_status graphPointsInsertTest(char *dataFileName, char * outputDataFileName, struct _tbSchema * schema, int32_t id);
enum test_status graphPointsSelectTest(char *dataFileName, char * outputDataFileName, struct _tbSchema * schema, int32_t id);
enum test_status graphPointsUpdateTest(char *dataFileName, char * outputDataFileName, struct _tbSchema * schema, int32_t id);
enum test_status graphPointsDeleteTest(char *dataFileName, char * outputDataFileName, struct _tbSchema * schema, int32_t id);
struct _tbSchema* getCocktailsSchema();
#endif //DBAPP_PERFORMANCETEST_H
