//
// Created by danandla on 10/18/22.
//

#ifndef DBAPP_TABLE_INTERNALS_H
#define DBAPP_TABLE_INTERNALS_H

#include <inttypes.h>
#include <page_internals.h>

struct tid{
    uint16_t pageId;
    uint16_t tupleId;
};

struct tableHeader{
    uint16_t  table_id;
    uint32_t pages_cnt;
    int64_t next;
    int64_t  lastPage;
    int32_t id;
    struct tid schema;
    struct tid tableName;
};

#endif //DBAPP_TABLE_INTERNALS_H
