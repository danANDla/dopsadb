//
// Created by danandla on 10/17/22.
//

#ifndef DBAPP_PAGE_INTERNALS_H
#define DBAPP_PAGE_INTERNALS_H

#include <inttypes.h>
#include <table_internals.h>

struct linePointer{
    uint16_t offset;
    uint16_t length;
};

struct dataPageHeader {
    int64_t next;
    uint32_t tuplesNumber;
    uint16_t pgLower;  // start of free space
    uint16_t pgUpper;  // end of free space
    uint16_t firstTuple;
};

struct freePageHeader{
    int64_t next;
};

struct superHeader{
    int64_t firstFree;
    int64_t lastFree;
    int64_t firstTable;
    int64_t lastTable;
    int64_t schemasPage;
    int64_t stringsPage;
    uint64_t tableId;
    uint64_t freePagesNumber;
    uint64_t dataPagesNumber;
    uint16_t pageSize;
    uint16_t lastStrPageId;
    int64_t lastStrPage;
};

#endif //DBAPP_PAGE_INTERNALS_H
