//
// Created by danandla on 3/8/23.
//

#ifndef DBAPP_JOINTREE_H
#define DBAPP_JOINTREE_H

#include <join.h>

struct joinTree;

typedef bool(*fpJoinTreeMoveNext)(struct joinTree*);

typedef struct joinTreeColumnId{
    uint16_t nodeId;
    int32_t column;
}joinTreeColumnId;

typedef struct joinTree{
    fpJoinTreeMoveNext (*moveNext)();

    uint16_t nodeId;
    bool isJoin;
    bool isLeaf;
    bool hasLeft;
    bool hasRight;

    struct joinTree* leftLeaf;
    struct joinTree* rightLeaf;

    joinTreeColumnId  leftColumn;
    joinTreeColumnId  rightColumn;

    enum data_type column_type;
    FILE* fd;

    tupleIterator* iterSrc;
    int64_t iterSrcPos;
    joinOperator* joinSrc;
}joinTree;

void bfsRestartTreeNode(joinTree *source);
void bfsPrintTree(joinTree *source);
bool joinTreeMoveNext(joinTree *tree);
#endif //DBAPP_JOINTREE_H
