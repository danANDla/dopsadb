//
// Created by danandla on 11/15/22.
//

#ifndef DBAPP_TABLE_UTIL_H
#define DBAPP_TABLE_UTIL_H

#include <page_writer.h>
#include <schema_util.h>
#include "page_internals.h"
#include "table_internals.h"

void printTableHeader(const struct tableHeader *header);
void printAllPagesAddresses(FILE* in, const int64_t tablePagePos);
enum readStatus readTableHeader(FILE *in, const int64_t pagePos, struct tableHeader *header);
enum writeStatus updateTableHeader(FILE *out, int64_t tablePos, const struct tableHeader *const table);
enum readStatus getTableById(FILE * const in, struct tableHeader * table, uint64_t id, int64_t* tablePos);
enum writeStatus createTable(FILE* out, struct _tbSchema* schema, char* tbName);
enum writeStatus pushPageToTable(FILE *out, const int64_t tablePagePos);
enum writeStatus deletePageFromTable(FILE *out, const int64_t tablePagePos, uint32_t pageNumber);
enum writeStatus addDataToTable(FILE* out, const uint64_t tableId, void** data, int32_t* tupleId);

enum readStatus getTableIdByName(FILE*, char*, uint64_t*);
enum readStatus getColumnIdByName(FILE*, uint64_t, char*, uint16_t*);
enum readStatus getSchemaByTableId(FILE *in, uint64_t id, struct _tbSchema **schema);

#endif //DBAPP_TABLE_UTIL_H
