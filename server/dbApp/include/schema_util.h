//
// Created by danandla on 11/15/22.
//

#ifndef DBAPP_TB_SCHEMA_H
#define DBAPP_TB_SCHEMA_H
#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>
#include "table_internals.h"

enum schema_status{
    SCHEMA_OK = 0,
    READ_SCHEMA_OK,
    READ_SCHEMA_ERROR,
    EXPAND_SCHEMA_OK,
    EXPAND_SCHEMA_ERROR
};

enum data_type{
    FIELD_TYPE_INT32 = 0,
    FIELD_TYPE_FLOAT32,
    FIELD_TYPE_BOOL,
    FIELD_TYPE_STRING
};

struct _tbSchema{
    uint16_t fields_number;
    enum data_type* fields;
    char** field_labels;
};

struct _tbSchema* init_table_schema();
enum schema_status expand_table_schema(struct _tbSchema* schema, enum data_type field_type, char* field_label);
void print_schema(const struct _tbSchema* const schema);
uint16_t getSchemaSize(const struct _tbSchema* const schema);
enum writeStatus writeSchema(FILE *out, struct _tbSchema* schema, struct tid *id);
enum schema_status readSchema(FILE *in, struct tid id, struct _tbSchema *schema);
void schemaDestruct(struct _tbSchema* schema);
size_t getFieldTypeSize(enum data_type* f);
size_t getTupleSize(struct _tbSchema* schema);

int32_t getIntField(uint16_t fieldN, struct _tbSchema* schema, void* tupleAddr);
char* getStrField(uint16_t fieldN, struct _tbSchema* schema, void* tupleAddr, FILE* fd);
float getFloatField(uint16_t fieldN, struct _tbSchema* schema, void* tupleAddr);
uint8_t getBoolField(uint16_t fieldN, struct _tbSchema* schema, void* tupleAddr);

uint8_t strEqualTest(void *leftStr, void *rightStr);
uint8_t floatEqualTest(void *left, void *right);
uint8_t intEqualTest(void *left, void *right);
uint8_t boolEqualTest(void *left, void *right);

void printRow(struct _tbSchema *schema, void *data, FILE *fd);
void printSchemaHeader(struct _tbSchema *schema, FILE *fd);

int schemaHeaderToStr(struct _tbSchema *schema, char* buff, size_t buffSz);
int rowToStr(struct _tbSchema *schema, void *data, FILE *fd, char* buff, size_t buffSz);
#endif //DBAPP_TB_SCHEMA_H
