//
// Created by danandla on 3/5/23.
//

#ifndef DBAPP_ITERTREE_H
#define DBAPP_ITERTREE_H

#include <iter.h>

struct joinOperator;

typedef bool(*fpJoinMoveNext)(struct joinOperator*);

typedef struct joinOperator{
    fpJoinMoveNext (*moveNext)();

    tupleIterator* leftSrc;
    tupleIterator* rightSrc;

    int64_t leftSrcPos;
    int64_t rightSrcPos;
    uint16_t leftColumn;
    uint16_t rightColumn;
    enum data_type column_type;

    void* leftRecord;
    void* rightRecord;

    bool isLeftFilter;
    filterInfo leftFilter;

    bool isRightFilter;
    filterInfo rightFilter;
} joinOperator;

typedef enum joinStatus{
    JOIN_OK = 0,
    JOIN_DIFFERENT_COLUMN_TYPES,
    JOIN_ERR
}joinStatus;

joinStatus joinInit(joinOperator *join,
                    tupleIterator *leftIter,
                    tupleIterator *rightIter,
                    uint16_t leftColumnId,
                    uint16_t rightColumnId,
                    bool isLeftFilter,
                    bool isRightFilter,
                    filterInfo leftFilter,
                    filterInfo rightFilter);
bool fieldCmp(uint16_t leftColumn, void *leftField, struct _tbSchema *leftSchema,
              uint16_t rightColumn, void *rightField, struct _tbSchema *rightSchema,
              enum data_type dataType, FILE* fd);
bool joinCmp(joinOperator *join);


void getColumnNames(uint16_t tablesNumber, tupleIterator **arr, char ***columns, uint16_t *colsNumber);
void getColumnNamesJoin( joinOperator *join, char ***columns, uint16_t *colsNumber);
void destructColumnNames(char **columns, uint16_t columnsNumber);
int joinedRowsHeaderToStr(joinOperator *join, FILE *fd, char *buff, size_t buffSz);
int joinedRowsToStr(joinOperator* join, FILE *fd, char* buff, size_t buffSz);
#endif //DBAPP_ITERTREE_H
