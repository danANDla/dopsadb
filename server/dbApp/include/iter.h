//
// Created by danandla on 2/12/23.
//

#ifndef DBAPP_ITER_H
#define DBAPP_ITER_H

#include <inttypes.h>
#include <schema_util.h>
#include <stdbool.h>
#include "table_internals.h"

struct iterator;

typedef bool(*fpMoveNext)(struct iterator *it);

typedef struct iterator {
    void *current;

    fpMoveNext (*moveNext)();
} iterator;

typedef struct {
    iterator base;
    void **arr;
    size_t *offts;
    uint32_t index;
    uint32_t count;
    FILE *fd;
    int64_t tablePos;
    int64_t curPagePos;
    uint32_t pageN;
    struct _tbSchema *schema;
    void *page;
    bool isDirty;
} tupleIterator;

typedef void(*fpConsume)(void *);

typedef bool(*fpTupleTest)(void *tuple, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);

typedef struct {
    iterator base;
    iterator *from;

    fpTupleTest (*test)();

    struct _tbSchema *schema;
    uint16_t fieldN;
    FILE *fd;
    void *param;
} filterTupleIterator;

typedef struct filterInfo {
    uint16_t colId;
    fpTupleTest test;
    void* param;
} filterInfo;

bool strEqualFieldTest(void *tuple, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);
bool strNotEqualFieldTest(void *tuple, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);

bool boolEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);
bool boolNotEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);

bool intEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);
bool intNotEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);
bool intGreaterThanParamFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);
bool intLessThanParamFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);

bool floatEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);
bool floatNotEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);
bool floatGreaterThanParamFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);
bool floatLessThanParamFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param);


filterTupleIterator *filterTupleIt(tupleIterator *from, fpTupleTest test, uint16_t n, void *param);
tupleIterator *tupleIt(int64_t tablePos, FILE *fd);
bool updateTuple(tupleIterator *it, void *newData, uint16_t newFieldN);
bool deleteTuple(tupleIterator *it);

void getColumnNames(uint16_t tablesNumber, tupleIterator **arr, char ***columns, uint16_t *colsNumber);
void destructColumnNames(char **columns, uint16_t columnsNumber);
#endif //DBAPP_ITER_H
