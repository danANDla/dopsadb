//
// Created by danandla on 3/9/23.
//

#include <string.h>

#include <inttypes.h>
#include <performanceTest.h>
#include <test.h>
#include <basicOperations.h>
#include <fileUtil.h>
#include <page_writer.h>
#include <table_util.h>
#include <query.h>


#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)  || __CYGWIN__

#include <Windows.h>
#include <stdint.h>
typedef struct timeval {
    long tv_sec;
    long tv_usec;
} timeval;
int gettimeofday(struct timeval* tp, struct timezone* tzp){
    // Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
    // This magic number is the number of 100 nanosecond intervals since January 1, 1601 (UTC)
    // until 00:00:00 January 1, 1970 
    static const uint64_t EPOCH = ((uint64_t)116444736000000000ULL);

    SYSTEMTIME  system_time;
    FILETIME    file_time;
    uint64_t    time;

    GetSystemTime(&system_time);
    SystemTimeToFileTime(&system_time, &file_time);
    time = ((uint64_t)file_time.dwLowDateTime);
    time += ((uint64_t)file_time.dwHighDateTime) << 32;

    tp->tv_sec = (long)((time - EPOCH) / 10000000L);
    tp->tv_usec = (long)(system_time.wMilliseconds * 1000);
    return 0;
}

#else
#include <sys/time.h>
#endif

enum test_status insertTest(char *dataFileName, struct _tbSchema *schema, uint32_t times, int32_t* tableId){
    struct timeval tStart, tFinish;

    FILE *out;
    if (open_u_file("database", &out) == WRITE_FILE_ERROR) return TEST_ERROR;

    uint64_t id;
    if(*tableId < 0){
        if (createTable(out, schema, "testTable") != WRITE_OK) return TEST_ERROR;
        struct superHeader s;
        readSuperHeader(out, &s);
        id = s.tableId - 1;
        *tableId = id;
    } else {
        id = (uint64_t) 0 + *tableId;
        schema->fields_number += 1;
    }

    struct superHeader super;
    readSuperHeader(out, &super);

    void **arr;
    uint32_t n;
    char* dataFname = getTestDataPath(dataFileName);
    readDataFile(dataFname, schema, &arr, &n);
    free(dataFname);
    gettimeofday(&tStart, NULL);
    for (uint32_t j = 0; j < times; ++j) {
        for (uint32_t i = 0; i < n; ++i) {
            void *item = arr[i];

            addItemToTable(item, out, (uint64_t) id, schema, 0);
        }
    }
    gettimeofday(&tFinish, NULL);

    for (uint32_t i = 0; i < n; ++i) {
        destructItem(schema, arr[i]);
    }
    free(arr);
    free(schema->field_labels);
    free(schema->fields);
    free(schema);
    close_file(out);


    printf("Did %u inserts in %.2g seconds\n", n*times, tFinish.tv_sec - tStart.tv_sec + 1E-6 * (tFinish.tv_usec - tStart.tv_usec));
    return TEST_OK;
}

enum test_status separateInsertTest(void ***arr, uint32_t arrSz,  struct _tbSchema *schema, uint32_t times, int32_t * tableId, float* testTime){
    struct timeval tStart, tFinish;
    FILE *out;
    if (open_u_file("database", &out) == WRITE_FILE_ERROR) return TEST_ERROR;
    struct superHeader super;
    readSuperHeader(out, &super);
    uint64_t id;
    if(*tableId < 0) return TEST_ERROR;
    else id = (uint64_t) 0 + *tableId;
    for (uint32_t j = 0; j < times; ++j) {
        for (uint32_t i = 0; i < arrSz; ++i) {
            void *item = (*arr)[i];
            if(times - j == 1 && arrSz - i == 1){
                gettimeofday(&tStart, NULL);
                addItemToTable(item, out, id, schema, 0);
                gettimeofday(&tFinish, NULL);
            } else{
                addItemToTable(item, out, id, schema, 0);
            }
        }
    }
    *testTime = tFinish.tv_sec - tStart.tv_sec + 1E-6 * (tFinish.tv_usec - tStart.tv_usec);
    close_file(out);
    return TEST_OK;
}

enum test_status graphPointsInsertTest(char *dataFileName, char * outputDataFileName, struct _tbSchema * schema, int32_t id){
    uint32_t times = 1000;

    FILE* output;
    open_w_file(outputDataFileName, &output);

    FILE* in;
    open_u_file("database", &in);
    struct tableHeader table;
    int64_t  tablePos;
    getTableById(in, &table, 0, &tablePos);
    uint64_t tuples = getNumberOfTuplesInTable(tablePos, in);
    close_file(in);

    float time;
    void **arr;
    uint32_t n;

    if(id < 0) return TEST_ERROR;
    schema->fields_number += 1;
    char* dataFname = getTestDataPath(dataFileName);
    readDataFile(dataFname, schema, &arr, &n);
    free(dataFname);
    for(int i = 0; i < 20000; i+=20){
        enum test_status testRes = separateInsertTest(&arr, n, schema, times, &id, &time);

        open_u_file("database", &in);
        if(testRes != TEST_OK) return TEST_ERROR;
        fseek(in, 0L, SEEK_END);
        long sz = ftell(in);
        close_file(in);

        tuples +=  times * n;
        if(output != NULL){
            fwrite(&time, sizeof (float), 1, output);
            fwrite(&tuples, sizeof (uint32_t), 1, output);
            fwrite(&sz, sizeof (long), 1, output);
        }
        printf("%d,%.2g,%u,%lu\n", i, time, tuples, sz);
    }
    close_file(output);

    for (uint32_t i = 0; i < n; ++i) {
        destructItem(schema, arr[i]);
    }
    free(arr);

    free(schema->field_labels);
    free(schema->fields);
    free(schema);
    return TEST_OK;
}

enum test_status separateQueryTest(dbQuery query, float* time){
    struct timeval tStart, tFinish;
    gettimeofday(&tStart, NULL);
    doQuery(query);
    gettimeofday(&tFinish, NULL);
    *time = tFinish.tv_sec - tStart.tv_sec + 1E-6 * (tFinish.tv_usec - tStart.tv_usec);
    return TEST_OK;
}

enum test_status graphQueryTest(FILE* in, uint32_t packsN, uint32_t packSz, dbQuery query, struct _tbSchema* schema, int64_t id, char* dataFileName, char* outputDataFileName){
    struct tableHeader table;
    int64_t  tablePos;
    getTableById(in, &table, 0, &tablePos);
    uint32_t tuples = getNumberOfTuplesInTable(tablePos, in);
    struct superHeader super;
    readSuperHeader(in, &super);
    if (super.dataPagesNumber < 1) return TEST_ERROR;

    schema->fields_number += 1;
    float time;
    void **arr;
    uint32_t n;
    char* dataFname = getTestDataPath(dataFileName);
    readDataFile(dataFname, schema, &arr, &n);
    free(dataFname);
    float timearr[200];
    uint32_t tuplesarr[200];
    for(int i = 0; i < packsN; ++i){
        printf("%d\n", i);
        float ttime;
        enum test_status testRes = separateInsertTest(&arr, n, schema, packSz, &id, &ttime);
        if(testRes != TEST_OK) return TEST_ERROR;
        tuples += n * packSz;
        separateQueryTest(query, &time);
        query.srcTable = tupleIt(tablePos, in);
        printf("Query took %.2g seconds, in table with %u rows\n", time, tuples);
        timearr[i] = time;
        tuplesarr[i] = tuples;

        if(i == packsN / 2){
            FILE* output;
            char* outputDataFname = getTestResPath(outputDataFileName);
            if(open_u_file(outputDataFname, &output) != WRITE_FILE_OK) return TEST_ERROR;
            free(outputDataFname);
            if(output != NULL){
                for(int t = 0; t < packsN / 2 - 1; ++t){
                    fwrite(&timearr[t], sizeof (float), 1, output);
                    fwrite(&tuplesarr[t], sizeof (uint32_t), 1, output);
                }
            }
            close_file(output);
        }

    }
    FILE* output;
    char* outputDataFname = getTestResPath(outputDataFileName);
    if(open_u_file(outputDataFname, &output) != WRITE_FILE_OK) return TEST_ERROR;
    free(outputDataFname);
    if(output != NULL){
        for(int t = 0; t < packsN; ++t){
            fwrite(&timearr[t], sizeof (float), 1, output);
            fwrite(&tuplesarr[t], sizeof (uint32_t), 1, output);
        }
    }
    close_file(output);
    for (uint32_t i = 0; i < n; ++i) {
        destructItem(schema, arr[i]);
    }
    free(arr);
    free(schema->field_labels);
    free(schema->fields);
    free(schema);
    return TEST_OK;
};

enum test_status graphPointsSelectTest(char *dataFileName, char * outputDataFileName, struct _tbSchema * schema, int32_t id){
    FILE* in;
    open_u_file("database", &in);
    struct tableHeader table;
    int64_t  tablePos;
    getTableById(in, &table, 0, &tablePos);
    tupleIterator *src = tupleIt(tablePos, in);
    char d[] = "sour";
    float price = 100.0f;
    atom cond = {
            .connective = CMP_CONNECTIVE,
            .leftOp = NULL,
            .rightOp = NULL,
            .type = FIELD_TYPE_FLOAT32,
            .data = &price,
            .cmp = CMP_GREATER,
            .fieldN = 1
    };
    dbQuery query = {
            .srcTable = src,
            .condition = cond,
            .action = ACTION_SELECT,
    };
    graphQueryTest(in, 100, 100, query, schema, id, dataFileName, outputDataFileName);
    close_file(in);
}

enum test_status graphPointsUpdateTest(char *dataFileName, char * outputDataFileName, struct _tbSchema * schema, int32_t id){
    FILE* in;
    open_u_file("database", &in);
    struct tableHeader table;
    int64_t  tablePos;
    getTableById(in, &table, 0, &tablePos);
    char str[] = "sour";
    float price = 100.0f;
    char newStr[] = "spiritCitrusSugar";
    float newPrice = 200.0f;

    float time;
    uint32_t tuples = getNumberOfTuplesInTable(tablePos, in);

    schema->fields_number += 1;
    void **arr;
    uint32_t n;
    char* dataFname = getTestDataPath(dataFileName);
    readDataFile(dataFname, schema, &arr, &n);
    free(dataFname);
    uint32_t packSz = 0;

    FILE* output;
    char* outputDataFname = getTestResPath(outputDataFileName);
    if(open_u_file(outputDataFname, &output) != WRITE_FILE_OK) return TEST_ERROR;
    free(outputDataFname);
    close_file(in);
    for(int i = 0; i < 100; i++){
        init_db("database", 4096);
        fill3TablesCocktails();
        open_u_file("database", &in);
        float ttime;
        packSz += 100;
        enum test_status testRes = separateInsertTest(&arr, n, schema, packSz, &id, &ttime);
        if(testRes != TEST_OK) return TEST_ERROR;
        tupleIterator *src = tupleIt(tablePos, in);
        atom cond = {
                .connective = CMP_CONNECTIVE,
                .leftOp = NULL,
                .rightOp = NULL,
                .type = FIELD_TYPE_FLOAT32,
                .data = &price,
                .cmp = CMP_GREATER,
                .fieldN = 1
        };
        dbQuery query = {
                .srcTable = src,
                .condition = cond,
                .action = ACTION_UPDATE,
                .updateData = &newPrice,
        };
        separateQueryTest(query, &time);
        tuples = n * packSz;
        printf("%d,%.2g,%u\n", i, time, tuples);
        if(output != NULL){
            fwrite(&time, sizeof (float), 1, output);
            fwrite(&tuples, sizeof (uint32_t), 1, output);
        }
        close_file(in);
    }
    close_file(output);
    return TEST_OK;
}

enum test_status graphPointsDeleteTest(char *dataFileName, char * outputDataFileName, struct _tbSchema * schema, int32_t id){
    FILE* in;
    open_u_file("database", &in);
    struct tableHeader table;
    int64_t  tablePos;
    getTableById(in, &table, 0, &tablePos);
    char str[] = "sour";
    float price = 100.0f;
    char newStr[] = "spiritCitrusSugar";
    float newPrice = 200.0f;

    float time;
    uint32_t tuples = getNumberOfTuplesInTable(tablePos, in);

    schema->fields_number += 1;
    void **arr;
    uint32_t n;
    char* dataFname = getTestDataPath(dataFileName);
    readDataFile(dataFname, schema, &arr, &n);
    free(dataFname);
    uint32_t packSz = 0;

    FILE* output;
    char* outputDataFname = getTestResPath(outputDataFileName);
    if(open_u_file(outputDataFname, &output) != WRITE_FILE_OK) return TEST_ERROR;
    free(outputDataFname);
    close_file(in);
    for(int i = 0; i < 100; i+=20){
        init_db("database", 4096);
        fill3TablesCocktails();
        open_u_file("database", &in);
        float ttime;
        packSz += 100;
        enum test_status testRes = separateInsertTest(&arr, n, schema, packSz, &id, &ttime);
        if(testRes != TEST_OK) return TEST_ERROR;
        tupleIterator *src = tupleIt(tablePos, in);
        atom cond = {
                .connective = CMP_CONNECTIVE,
                .leftOp = NULL,
                .rightOp = NULL,
                .type = FIELD_TYPE_FLOAT32,
                .data = &price,
                .cmp = CMP_GREATER,
                .fieldN = 1
        };
        dbQuery query = {
                .srcTable = src,
                .condition = cond,
                .action = ACTION_DELETE,
        };
        separateQueryTest(query, &time);
        tuples = n * packSz;
        printf("%d,%.2g,%u\n", i, time, tuples);
        if(output != NULL){
            fwrite(&time, sizeof (float), 1, output);
            fwrite(&tuples, sizeof (uint32_t), 1, output);
        }
        close_file(in);
    }
    close_file(output);
    return TEST_OK;
}

struct _tbSchema* getCocktailsSchema(){
    struct _tbSchema *schema = init_table_schema();
    expand_table_schema(schema, 3, "cocktailName");
    expand_table_schema(schema, 1, "price");
    expand_table_schema(schema, 3, "cocktailType");
    return schema;
}
