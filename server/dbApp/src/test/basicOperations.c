//
// Created by danandla on 3/9/23.
//

#include <string.h>
#include <test.h>
#include <basicOperations.h>

#include <fileUtil.h>
#include <page_writer.h>
#include <table_util.h>
#include <query.h>
#include <iter.h>
#include <join.h>
#include <joinTree.h>
#include <string_util.h>

#define VAL(str) #str
#define TOSTRING(str) VAL(str)

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__) || __CYGWIN__
#define TESTDATADIR "G:\\Mine\\BOTAY\\llp\\lab1\\pyScripts\\"
#define TESTRESDIR "G:\\Mine\\BOTAY\\llp\\lab1\\pyScripts\\graphPoints\\"
#else
#define TESTDATADIR "/home/danandla/BOTAY/llpYuko/dbApp/pyScripts/"
#define TESTRESDIR "/home/danandla/BOTAY/llpYuko/dbApp/pyScripts/graphPoints/"
#endif

enum test_status init_db(char *fname, uint16_t pgSize) {
    enum test_status res = TEST_OK;

    FILE *out;
    if (open_w_file(fname, &out) == WRITE_FILE_ERROR) return TEST_ERROR;
    clearFile(fname, out);

    if (initDB(out, pgSize) != WRITE_OK) res = TEST_ERROR;

    close_file(out);
    return res;
}

enum test_status seeSuperHeader(char *fname) {
    FILE *in;
    if (open_r_file(fname, &in) == READ_FILE_ERROR) return TEST_ERROR;
    struct superHeader super;
    if (readSuperHeader(in, &super) != READ_OK) return TEST_ERROR;
    printSuperHeader(&super);
    close_file(in);
    return TEST_OK;
}

enum test_status seeFreePage(char *fname) {
    FILE *out;
    if (open_u_file(fname, &out) == WRITE_FILE_ERROR) return TEST_ERROR;
    int64_t someFree;
    if (getFreePage(out, &someFree) != WRITE_OK) return TEST_ERROR;
    printf("got free page on %" PRId64 "\n", someFree);
    close_file(out);
    return TEST_OK;
}

enum test_status allocPage(char *fname) {
    FILE *out;
    if (open_u_file(fname, &out) == WRITE_FILE_ERROR) return TEST_ERROR;
    int64_t someFree;
    if (allocFreePage(out, &someFree) != WRITE_OK) return TEST_ERROR;
    printf("allocated page on %" PRId64 "\n", someFree);
    close_file(out);
    return TEST_OK;
}

uint32_t getNumberOfTuplesInTable(int64_t tablePos, FILE *out) {
    struct dataPageHeader page;
    int64_t pagePos = tablePos;
    readDataPageHeader(out, pagePos, &page);
    uint32_t tuplesN = page.tuplesNumber;
    while (page.next != -1) {
        pagePos = page.next;
        readDataPageHeader(out, pagePos, &page);
        tuplesN += page.tuplesNumber;
    }
    return tuplesN;
}

enum test_status getTable(char *fname, uint64_t id) {
    FILE *out;
    if (open_u_file(fname, &out) == WRITE_FILE_ERROR) return TEST_ERROR;
    struct superHeader super;
    readSuperHeader(out, &super);

    struct tableHeader table;
    int64_t tablePos;
    if (getTableById(out, &table, id, &tablePos) == READ_OK) {
        printf("Found table with id %" PRIu64 " on %" PRId64"\n", id, tablePos);
        printTableHeader(&table);
        struct _tbSchema *schema = init_table_schema();
        readSchema(out, table.schema, schema);
        print_schema(schema);
        schemaDestruct(schema);

        struct dataPageHeader page;
        int64_t pagePos = tablePos;
        readDataPageHeader(out, pagePos, &page);
        uint32_t tuplesN = page.tuplesNumber;
        printf("pages %" PRId64 " -> ", pagePos);
        while (page.next != -1) {
            pagePos = page.next;
            readDataPageHeader(out, pagePos, &page);
            tuplesN += page.tuplesNumber;
            printf("%" PRId64 " -> ", pagePos);
        }
        printf("\n");
        printf("tuplesNumber = %" PRIu32"\n", tuplesN);

    } else {
        printf("Haven't found table with such id\n");
    }

    close_file(out);
    return TEST_OK;
}

enum test_status doSelectQuery(char *fname, uint64_t id) {
    FILE *out;
    if (open_u_file(fname, &out) == WRITE_FILE_ERROR) return TEST_ERROR;
    struct superHeader super;
    readSuperHeader(out, &super);

    if (super.dataPagesNumber < 1) return TEST_ERROR;
    struct tableHeader table;
    int64_t tablePos;
    if (getTableById(out, &table, id, &tablePos) == READ_OK) {

        tupleIterator *src = tupleIt(tablePos, out);
        char d[] = "";
        atom cond = {
                .connective = CMP_CONNECTIVE,
                .leftOp = NULL,
                .rightOp = NULL,
                .type = 0,
                .data = d,
                .cmp = CMP_NEQUAL,
                .fieldN = 1
        };
        dbQuery query = {
                .srcTable = src,
                .condition = cond,
                .action = ACTION_SELECT,
        };

        doQuery(query);
    } else {
        printf("Haven't found table with such id\n");
    }

    close_file(out);
    return TEST_OK;
}

enum test_status doUpdateQuery(char *fname, uint64_t id) {
    FILE *out;
    if (open_u_file(fname, &out) == WRITE_FILE_ERROR) return TEST_ERROR;
    struct superHeader super;
    readSuperHeader(out, &super);

    if (super.dataPagesNumber < 1) return TEST_ERROR;
    struct tableHeader table;
    int64_t tablePos;
    if (getTableById(out, &table, id, &tablePos) == READ_OK) {

        tupleIterator *src = tupleIt(tablePos, out);
        char d[] = "highball";
        char newVal[] = "bruh";
        atom cond = {
                .connective = CMP_CONNECTIVE,
                .leftOp = NULL,
                .rightOp = NULL,
                .type = FIELD_TYPE_STRING,
                .data = d,
                .cmp = CMP_NEQUAL,
                .fieldN = 2
        };
        dbQuery query = {
                .srcTable = src,
                .condition = cond,
                .action = ACTION_UPDATE,
                .updateData = newVal
        };

        doQuery(query);
    } else {
        printf("Haven't found table with such id\n");
    }

    close_file(out);
    return TEST_OK;
}

enum test_status doDeleteQuery(char *fname, uint64_t id) {
    FILE *out;
    if (open_u_file(fname, &out) == WRITE_FILE_ERROR) return TEST_ERROR;
    struct superHeader super;
    readSuperHeader(out, &super);

    if (super.dataPagesNumber < 1) return TEST_ERROR;
    struct tableHeader table;
    int64_t tablePos;
    if (getTableById(out, &table, id, &tablePos) == READ_OK) {

        tupleIterator *src = tupleIt(tablePos, out);
        char d[] = "";
        atom cond = {
                .connective = CMP_CONNECTIVE,
                .leftOp = NULL,
                .rightOp = NULL,
                .type = FIELD_TYPE_STRING,
                .data = d,
                .cmp = CMP_NEQUAL,
                .fieldN = 2
        };
        dbQuery query = {
                .srcTable = src,
                .condition = cond,
                .action = ACTION_DELETE,
        };

        doQuery(query);
    } else {
        printf("Haven't found table with such id\n");
    }

    close_file(out);
    return TEST_OK;
}


void printJoinedRows(tupleIterator *left, tupleIterator *right) {
    printRow(left->schema, *(void **) left->base.current, left->fd);
    printRow(right->schema, *(void **) right->base.current, left->fd);
    printf("\n");
}

void printRowsHeader(uint16_t colsNumber, char **columnNames) {
    for (uint16_t i = 0; i < colsNumber; ++i) {
        printf("%-15s", columnNames[i]);
    }
    printf("\n");
}


enum test_status joinCheck(char *fname) {
    int64_t leftId = 0;
    int64_t rightId = 2;

    FILE *out;
    if (open_u_file(fname, &out) == WRITE_FILE_ERROR) return TEST_ERROR;
    struct superHeader super;
    readSuperHeader(out, &super);

    struct tableHeader leftTable;
    int64_t leftTablePos;
    struct tableHeader rightTable;
    int64_t rightTablePos;

    if (getTableById(out, &leftTable, leftId, &leftTablePos) == READ_OK &&
        getTableById(out, &rightTable, rightId, &rightTablePos) == READ_OK) {

        joinOperator join;
        tupleIterator *leftTuple = tupleIt(leftTablePos, out);
        tupleIterator *rightTuple = tupleIt(rightTablePos, out);
//        joinInit(&join, leftTuple, rightTuple, 3, 0);

        char **cols;
        uint16_t colsNumber;
        tupleIterator *tuples[2];
        tuples[0] = leftTuple;
        tuples[1] = rightTuple;
        getColumnNames(2, tuples, &cols, &colsNumber);
        printRowsHeader(colsNumber, cols);
        while (join.moveNext(&join))
            printJoinedRows(join.leftSrc, join.rightSrc);
    } else {
        printf("Haven't found table with such id\n");
    }
    close_file(out);
    return TEST_OK;
}

enum test_status joinTreeCheck(char *fname) {
    int64_t leftId = 0;
    int64_t midId = 1;
    int64_t rightId = 2;

    FILE *out;
    if (open_u_file(fname, &out) == WRITE_FILE_ERROR) return TEST_ERROR;
    struct superHeader super;
    readSuperHeader(out, &super);

    struct tableHeader leftTable;
    int64_t leftTablePos;
    struct tableHeader midTable;
    int64_t midTablePos;
    struct tableHeader rightTable;
    int64_t rightTablePos;

    if (getTableById(out, &leftTable, leftId, &leftTablePos) == READ_OK &&
        getTableById(out, &rightTable, rightId, &rightTablePos) == READ_OK &&
        getTableById(out, &midTable, midId, &midTablePos) == READ_OK) {


        joinOperator join;
        tupleIterator *midTuple = tupleIt(midTablePos, out);
        tupleIterator *rightTuple = tupleIt(rightTablePos, out);
//        joinInit(&join, midTuple, rightTuple, 1, 1);

        tupleIterator *leftTuple = tupleIt(leftTablePos, out);

        joinTree leftLeaf = {
                .nodeId = 1,
                .moveNext = joinTreeMoveNext,
                .isLeaf = true,
                .isJoin = false,
                .iterSrcPos =leftTablePos,
                .iterSrc = leftTuple,
                .fd = out,
        };
        joinTree rightLeaf = {
                .nodeId = 2,
                .moveNext = joinTreeMoveNext,
                .isLeaf = true,
                .isJoin = true,
                .joinSrc = &join,
                .fd = out
        };

        joinTree tree = {
                .nodeId = 0,
                .moveNext = joinTreeMoveNext,
                .leftLeaf = &leftLeaf,
                .rightLeaf = &rightLeaf,
                .hasLeft = false,
                .hasRight = true,
                .leftColumn = {.nodeId = 1, .column = 3},
                .rightColumn = {.nodeId = 2, .column = 0},
                .fd = out,
                .column_type = FIELD_TYPE_INT32
        };

        while (tree.moveNext(&tree)) {
            bfsPrintTree(&tree);
            printf("\n");
        }
    } else {
        printf("Haven't found table with such id\n");
    }
    close_file(out);
    return TEST_OK;
}

size_t getItemSize(struct _tbSchema *schema) {
    size_t sz = 0;
    for (uint16_t i = 0; i < schema->fields_number - 1; ++i) {
        switch (schema->fields[i]) {
            case FIELD_TYPE_INT32:
                sz += sizeof(int32_t);
                break;
            case FIELD_TYPE_FLOAT32:
                sz += sizeof(float);
                break;
            case FIELD_TYPE_BOOL:
                sz += sizeof(uint8_t);
                break;
            case FIELD_TYPE_STRING:
                sz += sizeof(char *);
                break;
        }
    }
    return sz;
}

void readItemFromFile(FILE *in, void *dst, size_t *offt, struct _tbSchema *schema) {
    size_t localOfft = 0;
    for (uint16_t i = 0; i < schema->fields_number; ++i) {
        switch (schema->fields[i]) {
            case FIELD_TYPE_STRING: {
                char *name;
                fscanf(in, "%m[^\n]s", &name);
                *offt += strlen(name) + 1;
                fseek(in, *offt, SEEK_SET);
                memcpy((char *) dst + localOfft, &name, sizeof(char **));
                localOfft += sizeof(char **);
                break;
            }
            case FIELD_TYPE_FLOAT32: {
                fread((char *) dst + localOfft, sizeof(float), 1, in);
                *offt += sizeof(float);
                localOfft += sizeof(float);
                break;
            }
            case FIELD_TYPE_INT32: {
                fread((char *) dst + localOfft, sizeof(int32_t), 1, in);
                *offt += sizeof(int32_t);
                localOfft += sizeof(int32_t);
                break;
            }
            case FIELD_TYPE_BOOL: {
                fread((char *) dst + localOfft, sizeof(uint8_t), 1, in);
                *offt += sizeof(uint8_t);
                localOfft += sizeof(uint8_t);
                break;
            }
        }
    }
}

enum test_status readDataFile(char *fname, struct _tbSchema *schema, void ***arr, uint32_t *n) {
    FILE *in;
    if (open_u_file(fname, &in) == WRITE_FILE_ERROR) return TEST_ERROR;
    size_t offt = 4;

    fread(n, sizeof(*n), 1, in);
    *arr = malloc(sizeof(void *) * *n);
    for (int i = 0; i < *n; ++i) {
        void *item = calloc(1, getItemSize(schema));
        readItemFromFile(in, item, &offt, schema);
        (*arr)[i] = item;
    }
    close_file(in);
    return TEST_OK;
}

void addItemToTable(void *item, FILE *out, uint64_t id, struct _tbSchema *schema, int withId) {
    if (!withId) schema->fields_number -= 1;
    void *data = calloc(1, getTupleSize(schema));
    size_t offt = 0;
    size_t itemOfft = 0;
    for (uint16_t i = 0; i < schema->fields_number; ++i) {
        switch (schema->fields[i]) {
            case FIELD_TYPE_STRING: {
                struct tid strTid;
                writeString(out, *(char **) ((char *) item + itemOfft), &strTid);
                memcpy((char *) data + offt, &strTid, sizeof strTid);
                offt += sizeof strTid;
                itemOfft += sizeof(char **);
                break;
            }
            case FIELD_TYPE_FLOAT32: {
                memcpy((char *) data + offt, (char *) item + itemOfft, sizeof(float));
                offt += sizeof(float);
                itemOfft += sizeof(float);
                break;
            }
            case FIELD_TYPE_INT32: {
                memcpy((char *) data + offt, (char *) item + itemOfft, sizeof(int32_t));
                offt += sizeof(int32_t);
                itemOfft += sizeof(int32_t);
                break;
            }
            case FIELD_TYPE_BOOL: {
                memcpy((char *) data + offt, (char *) item + itemOfft, sizeof(int8_t));
                offt += sizeof(int8_t);
                itemOfft += sizeof(int8_t);
                break;
            }
        }
    }
    if (!withId) {
        addDataToTable(out, id, &data, NULL);
        schema->fields_number += 1;
    } else {
        int32_t *itemId = item + itemOfft - sizeof(int32_t);
        addDataToTable(out, id, &data, itemId);
    }
    free(data);
}

void destructItem(struct _tbSchema *schema, void *item) {
    size_t offt = 0;
    for (uint16_t i = 0; i < schema->fields_number - 1; ++i) {
        if (schema->fields[i] == FIELD_TYPE_STRING) {
            char *str = *(char **) ((char *) item + offt);
            free(str);
            offt += sizeof(char **);
        }
        switch (schema->fields[i]) {
            case FIELD_TYPE_INT32:
                offt += sizeof(int32_t);
                break;
            case FIELD_TYPE_BOOL:
                offt += sizeof(uint8_t);
                break;
            case FIELD_TYPE_FLOAT32:
                offt += sizeof(float);
                break;
        }
    }
    free(item);
}

char *concatL(const char *s1, const char *s2) {
    char *buffer = malloc(strlen(s1) + strlen(s2) + 1);
    strcpy(buffer, s1);
    strcat(buffer, s2);
    return buffer;
}

char *getTestResPath(char *fname) {
    char *dataFname = concatL(TESTRESDIR, fname);
    return dataFname;
}

char *getTestDataPath(char *fname) {
    char *dataFname = concatL(TESTDATADIR, fname);
    return dataFname;
}

struct _tbSchema *copySchemaWithoutId(struct _tbSchema *self) {
    struct _tbSchema *ret = init_table_schema();
    for (uint16_t i = 0; i < self->fields_number - 1; ++i) {
        expand_table_schema(ret, self->fields[i], self->field_labels[i]);
    }
    return ret;
}

enum test_status fillBoozyTable(char *fname, struct _tbSchema *schema, uint32_t times, char *tName, int withId) {
    FILE *out;
    char dbname[] = "/home/danandla/BOTAY/llpYuko/dbApp/cmake-build-debug/database";
    if (open_u_file(dbname, &out) == WRITE_FILE_ERROR) return TEST_ERROR;
    struct _tbSchema *copy;
    if (!withId) {
        if (createTable(out, schema, tName) != WRITE_OK) return TEST_ERROR;
    } else {
        copy = copySchemaWithoutId(schema);
        if (createTable(out, copy, tName) != WRITE_OK) return TEST_ERROR;
    }

    struct superHeader super;
    readSuperHeader(out, &super);
    uint64_t id = super.tableId - 1;

    void **arr;
    uint32_t n;
    char *dataFname = getTestDataPath(fname);
    readDataFile(dataFname, schema, &arr, &n);
    for (uint32_t j = 0; j < times; ++j) {
        for (uint32_t i = 0; i < n; ++i) {
            void *item = arr[i];
            addItemToTable(item, out, id, schema, withId);
        }
    }
    free(dataFname);

    for (uint32_t i = 0; i < n; ++i) {
        destructItem(schema, arr[i]);
    }
    free(arr);
    free(schema->field_labels);
    free(schema->fields);
    free(schema);
    close_file(out);
    return TEST_OK;
}

enum test_status fillCocktails() {
    struct _tbSchema *schema = init_table_schema();
    expand_table_schema(schema, 3, "cocktailName");
    expand_table_schema(schema, 1, "price");
    expand_table_schema(schema, 3, "cocktailType");
    return fillBoozyTable("bufferCocktails", schema, 1, "cocktails", 0);
}

enum test_status fillIngredients() {
    struct _tbSchema *schema = init_table_schema();
    expand_table_schema(schema, 3, "ingredientName");
    return fillBoozyTable("bufferIngredients", schema, 1, "ingredients", 0);
}

enum test_status fillRecipes() {
    struct _tbSchema *schema = init_table_schema();
    expand_table_schema(schema, FIELD_TYPE_INT32, "cocktailId");
    expand_table_schema(schema, FIELD_TYPE_INT32, "ingredientId");
    expand_table_schema(schema, FIELD_TYPE_FLOAT32, "amount");
    return fillBoozyTable("bufferRecipes", schema, 1, "recipes", 0);
}

enum test_status fill3TablesCocktails() {
    enum test_status testRes;
    testRes = fillCocktails();
    if (testRes != TEST_OK) return testRes;
    testRes = fillIngredients();
    if (testRes != TEST_OK) return testRes;
    testRes = fillRecipes();
    return testRes;
}

enum test_status fillClubs() {
    struct _tbSchema *schema = init_table_schema();
    expand_table_schema(schema, 3, "clubName");
    expand_table_schema(schema, 0, "id");
    return fillBoozyTable("bufferClubs", schema, 1, "clubs", 1);
}

enum test_status fillPlayers() {
    struct _tbSchema *schema = init_table_schema();
    expand_table_schema(schema, 3, "playerName");
    expand_table_schema(schema, 0, "id");
    return fillBoozyTable("bufferPlayers", schema, 1, "players", 1);
}

enum test_status fillTransfers() {
    struct _tbSchema *schema = init_table_schema();
    expand_table_schema(schema, 0, "player");
    expand_table_schema(schema, 0, "clubFrom");
    expand_table_schema(schema, 0, "clubTo");
    expand_table_schema(schema, 1, "transferFee");
    expand_table_schema(schema, 0, "id");
    return fillBoozyTable("bufferTransfers", schema, 1, "transfers", 1);
}

enum test_status fillCategories() {
    struct _tbSchema *schema = init_table_schema();
    expand_table_schema(schema, 3, "categoryName");
    expand_table_schema(schema, 0, "id");
    return fillBoozyTable("bufferCategories", schema, 1, "categories", 1);
}

enum test_status fillCustomers() {
    struct _tbSchema *schema = init_table_schema();
    expand_table_schema(schema, 3, "city");
    expand_table_schema(schema, 3, "contactName");
    expand_table_schema(schema, 0, "id");
    return fillBoozyTable("bufferCustomers", schema, 1, "customers", 1);
}

enum test_status fillProducts() {
    struct _tbSchema *schema = init_table_schema();
    expand_table_schema(schema, 3, "productName");
    expand_table_schema(schema, 0, "categoryId");
    expand_table_schema(schema, 1, "unitPrice");
    expand_table_schema(schema, 0, "unitsInStock");
    expand_table_schema(schema, 0, "id");
    return fillBoozyTable("bufferProducts", schema, 1, "products", 1);
}

enum test_status fill3TablesFootball() {
    enum test_status testRes;
    testRes = fillPlayers();
    if(testRes != TEST_OK) return testRes;
    testRes = fillClubs();
    if(testRes != TEST_OK) return testRes;
    testRes = fillTransfers();
    if(testRes != TEST_OK) return testRes;
    return testRes;
}

enum test_status fillTablesNorthwind() {
    enum test_status testRes;
    testRes = fillCategories();
    if(testRes != TEST_OK) return testRes;
    testRes = fillCustomers();
    if(testRes != TEST_OK) return testRes;
    testRes = fillProducts();
    if(testRes != TEST_OK) return testRes;
    return testRes;
}
