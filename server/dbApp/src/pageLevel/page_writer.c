//
// Created by danandla on 12/13/22.
//

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <page_writer.h>

void printPageHeader(struct dataPageHeader *header) {
    printf("pgUpper: %" PRIu16 "\n", header->pgUpper);
    printf("pgLower: %" PRIu16 "\n", header->pgLower);
}

enum writeStatus initDB(FILE *out, uint16_t pgSize) {
    struct superHeader super = {
            .firstFree = -1,
            .lastFree = -1,
            .freePagesNumber = 0,
            .firstTable = -1,
            .dataPagesNumber = 0,
            .pageSize = pgSize,
            .tableId = 0,
            .schemasPage = pgSize,
            .stringsPage = 2*pgSize,
            .lastStrPageId = 0,
            .lastStrPage = 2*pgSize
    };
    void* freePage = calloc(1, super.pageSize);
    if (freePage == NULL) return WRITE_ERROR;
    memcpy(freePage, &super, sizeof(struct superHeader));
    if (fwrite(freePage, super.pageSize, 1, out) < 1) return WRITE_ERROR;


    struct dataPageHeader schemasPage = {
            .pgLower = sizeof(struct dataPageHeader),
            .pgUpper = super.pageSize - 1,
            .next = -1,
            .tuplesNumber = 0
    };
    memcpy(freePage, &schemasPage, sizeof(struct dataPageHeader));
    fseek(out, 0, SEEK_END);
    if (fwrite(freePage, super.pageSize, 1, out) < 1) return WRITE_ERROR;

    fseek(out, 0, SEEK_END);
    if (fwrite(freePage, super.pageSize, 1, out) < 1) return WRITE_ERROR;

    free(freePage);
    return WRITE_OK;
}

void printSuperHeader(struct superHeader *super) {
    printf("page size: %" PRIu16 "\n", super->pageSize);
    printf("-------PAGES-------\n");
    printf("free pages number: %" PRIu16 "\n", super->freePagesNumber);
    printf("first free: %" PRId64 "\n", super->firstFree);
    printf("last free: %" PRId64 "\n", super->lastFree);
    printf("-------------------\n");
    printf("-------TABLES-------\n");
    printf("data pages number: %" PRIu16 "\n", super->dataPagesNumber);
    printf("first table: %" PRId64 "\n", (uintptr_t) super->firstTable);
    printf("last table: %" PRId64 "\n", (uintptr_t) super->lastTable);
    printf("next table id: %" PRIu64 "\n", super->tableId);
    printf("--------------------\n");
}

enum readStatus readSuperHeader(FILE* in, struct superHeader *header) {
    fseek(in, 0, SEEK_SET);
    if (!fread(header, sizeof(struct superHeader), 1, in)) return READ_ERROR;
    return READ_OK;
}

enum writeStatus pushFreePage(FILE *out, struct superHeader *super, int64_t *const freePagePos) {
    struct freePageHeader freeHeader = {
            .next = -1
    };
    void *freePage = calloc(1, super->pageSize);
    if (freePage == NULL) return WRITE_ERROR;
    memcpy(freePage, &freeHeader, sizeof(struct freePageHeader));
    fseek(out, 0, SEEK_END);
    *freePagePos = ftell(out);
    if (fwrite(freePage, super->pageSize, 1, out) < 1) return WRITE_ERROR;

    if (super->freePagesNumber < 1) {
        super->freePagesNumber = 0;
        super->firstFree = *freePagePos;
        super->lastFree = *freePagePos;
    }
    super->freePagesNumber += 1;

    if (updateSuperHeader(out, super) != WRITE_OK) return WRITE_ERROR;
    free(freePage);
    return WRITE_OK;
}

enum writeStatus getFreePage(FILE *out, int64_t *freePagePos) {
    struct superHeader super;
    if (readSuperHeader(out, &super) != READ_OK) return WRITE_ERROR;
    if (super.freePagesNumber < 1) {
        if (pushFreePage(out, &super, freePagePos) != WRITE_OK) return WRITE_ERROR;
    } else {
        *freePagePos = super.firstFree;
    }
    return WRITE_OK;
}

enum writeStatus freeDataPage(FILE *out, const int64_t *dataPagePos) {
    struct superHeader super;
    if (readSuperHeader(out, &super) != READ_OK) return WRITE_ERROR;

    struct freePageHeader free = {
            .next = -1
    };
    fseek(out, *dataPagePos, SEEK_SET);
    if (fwrite(&free, sizeof(struct freePageHeader), 1, out) < 1) return WRITE_ERROR;

    fseek(out, 0, SEEK_END);
    if (*dataPagePos + super.pageSize >= ftell(out)) {
        // ftruncate(fileno(out), *dataPagePos);
    } else {
        if (readSuperHeader(out, &super) != READ_OK) return WRITE_ERROR;
        if (super.freePagesNumber == 0) super.firstFree = *dataPagePos;
        else {
            struct freePageHeader lastFree = {
                    .next = *dataPagePos
            };
            fseek(out, super.lastFree, SEEK_SET);
            if (fwrite(&lastFree, sizeof(struct freePageHeader), 1, out) < 1) return WRITE_ERROR;
        }
        super.freePagesNumber += 1;
        super.lastFree = *dataPagePos;
        updateSuperHeader(out, &super);
    }
    return WRITE_OK;
}

enum readStatus readFreePageHeader(FILE *in, const int64_t pagePos, struct freePageHeader *freePage) {
    fseek(in, pagePos, SEEK_SET);
    if (!fread(freePage, sizeof(struct freePageHeader), 1, in)) return READ_ERROR;
    return READ_OK;
}

enum readStatus readDataPageHeader(FILE *in, const int64_t pagePos, struct dataPageHeader *dataPage) {
    fseek(in, pagePos, SEEK_SET);
    if (!fread(dataPage, sizeof(struct dataPageHeader), 1, in)) return READ_ERROR;
    return READ_OK;
}

enum writeStatus updateDataPageHeader(FILE *out, int64_t dataPagePos, const struct dataPageHeader *const dataPage) {
    fseek(out, dataPagePos, SEEK_SET);
    if (fwrite(dataPage, sizeof(struct dataPageHeader), 1, out) < 1) return WRITE_ERROR;
    return WRITE_OK;
}


enum file_status readLinePointer(FILE *in, const int64_t pointerPos, struct linePointer *itemDataId) {
    fseek(in, pointerPos, SEEK_SET);
    if (!fread(itemDataId, sizeof(struct linePointer), 1, in)) return READ_FILE_ERROR;
    return READ_FILE_OK;
}

enum writeStatus updateSuperHeader(FILE *out, const struct superHeader *const super) {
    fseek(out, 0, SEEK_SET);
    if (fwrite(super, sizeof(struct superHeader), 1, out) < 1) return WRITE_ERROR;
    return WRITE_OK;
}

enum writeStatus allocFreePage(FILE *out, int64_t *pagePos) {
    struct superHeader super;
    if (readSuperHeader(out, &super) != READ_OK) return WRITE_ERROR;

    if (getFreePage(out, pagePos) != WRITE_OK) return WRITE_ERROR;

    struct dataPageHeader dataPage = {
            .pgLower = sizeof(struct dataPageHeader),
            .pgUpper = super.pageSize - 1,
            .next = -1,
            .tuplesNumber = 0,
            .firstTuple = sizeof (struct dataPageHeader)
    };

    struct freePageHeader freePage;
    if (readFreePageHeader(out, *pagePos, &freePage) != READ_OK) return WRITE_ERROR;

    fseek(out, *pagePos, SEEK_SET);
    if (fwrite(&dataPage, sizeof(struct dataPageHeader), 1, out) != 1) return WRITE_HEADER_ERROR;

    if (readSuperHeader(out, &super) != READ_OK) return WRITE_ERROR;
    super.freePagesNumber -= 1;
    super.firstFree = freePage.next;
    if (super.firstFree == -1) super.lastFree = -1;

    if (updateSuperHeader(out, &super) != WRITE_OK) return WRITE_ERROR;
    return WRITE_OK;
}

enum writeStatus addDataToPage(FILE *out, const int64_t pagePos, const size_t dataSize, void *data, struct tid *id) {
    struct dataPageHeader page;
    if (readDataPageHeader(out, pagePos, &page) != READ_OK) return WRITE_ERROR;

    if (page.pgLower + sizeof(struct linePointer) >= page.pgUpper - dataSize) return WRITE_ERROR;

    struct linePointer newItemId = {
            .length = (uint16_t) dataSize,
            .offset =  page.pgUpper - dataSize + 1
    };

    fseek(out, pagePos + page.pgLower, SEEK_SET);
    if (fwrite(&newItemId, sizeof(struct linePointer), 1, out) < 1) return WRITE_ERROR;
    fseek(out, pagePos + (int64_t) newItemId.offset, SEEK_SET);
    if (fwrite(data, dataSize, 1, out) < 1) return WRITE_ERROR;

    page.pgUpper = newItemId.offset - 1;
    page.pgLower = page.pgLower + sizeof(struct linePointer);
    page.tuplesNumber += 1;
    updateDataPageHeader(out, pagePos, &page);

    if (id != NULL) {
        id->pageId = 0;
        id->tupleId = page.tuplesNumber - 1;
    }

    return WRITE_OK;
}

void printAllDataOnPage(FILE *out, const int64_t pagePos) {
    struct dataPageHeader page;
    readDataPageHeader(out, pagePos, &page);
    printf("found %" PRIu16 " tuples on the page\n", page.tuplesNumber);

    struct linePointer itemDataId;
    int64_t pointerPos = pagePos + page.pgLower - page.tuplesNumber * sizeof(struct linePointer);
    printf("first tuple on %" PRId64 " \n", pointerPos);
    for (uint32_t i = 0; i < page.tuplesNumber; ++i) {
        readLinePointer(out, pointerPos, &itemDataId);
        printf("tuple[%" PRIu32 "].length = %" PRIu16 ", ", i, itemDataId.length);

        printf("reading data from %" PRId64 ": ", pagePos + itemDataId.offset);
        uint8_t *buff = malloc(itemDataId.length);
        fseek(out, pagePos + itemDataId.offset, SEEK_SET);
        fread(buff, itemDataId.length, 1, out);

        for (uint16_t j = 0; j < itemDataId.length; ++j) printf("%02X ", buff[j]);
        printf("\n");

        free(buff);

        pointerPos += sizeof(struct linePointer);
    }
}
