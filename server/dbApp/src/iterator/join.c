//
// Created by danandla on 3/5/23.
//

#include <string.h>
#include <join.h>
#include <table_util.h>
#include <ast.h>

bool fieldCmp(uint16_t leftColumn, void *leftField, struct _tbSchema *leftSchema,
              uint16_t rightColumn, void *rightField, struct _tbSchema *rightSchema,
              enum data_type dataType, FILE *fd) {
    bool res = false;
    switch (dataType) {
        case FIELD_TYPE_STRING: {
            char *left = getStrField(leftColumn, leftSchema, leftField, fd);
            char *right = getStrField(rightColumn, rightSchema, rightField, fd);
            res = strEqualTest(left, right);
            break;
        }
        case FIELD_TYPE_FLOAT32: {
            float left = getFloatField(leftColumn, leftSchema, leftField);
            float right = getFloatField(rightColumn, rightSchema, rightField);
            res = floatEqualTest(&left, &right);
            break;
        }
        case FIELD_TYPE_INT32: {
            int left = getIntField(leftColumn, leftSchema, leftField);
            int right = getIntField(rightColumn, rightSchema, rightField);
            res = intEqualTest(&left, &right);
            break;
        }
        case FIELD_TYPE_BOOL: {
            uint8_t left = getBoolField(leftColumn, leftSchema, leftField);
            uint8_t right = getBoolField(rightColumn, rightSchema, rightField);
            res = boolEqualTest(&left, &right);
            break;
        }
    }
    return res;
}

bool joinCmp(joinOperator *join) {
    struct _tbSchema *lschema;
    struct _tbSchema *rschema;
    FILE *fd;
    if (join->isLeftFilter) {
        lschema = ((filterTupleIterator *) join->leftSrc)->schema;
        fd = ((filterTupleIterator *) join->leftSrc)->fd;
    } else {
        lschema = join->leftSrc->schema;
        fd = join->leftSrc->fd;
    }
    if (join->isRightFilter) rschema = ((filterTupleIterator *) join->rightSrc)->schema;
    else rschema = join->rightSrc->schema;
    return fieldCmp(join->leftColumn, join->leftSrc->base.current, lschema,
                    join->rightColumn, join->rightSrc->base.current, rschema,
                    join->column_type, fd);
}

bool joinMoveNext(joinOperator *join) {
    while (true) {
        if (join->leftRecord == NULL) {
            if (join->leftSrc->base.moveNext((iterator *) join->leftSrc)) {
                join->leftRecord = join->leftSrc->base.current;
                if (join->rightSrc == NULL) {
                    if (join->isRightFilter) {
                        tupleIterator *tuple = tupleIt(join->rightSrcPos, join->leftSrc->fd);
                        join->rightSrc = filterTupleIt(tuple, join->rightFilter.test, join->rightFilter.colId,
                                                       join->rightFilter.param);
                    } else {
                        if (join->isLeftFilter)
                            join->rightSrc = tupleIt(join->rightSrcPos, ((filterTupleIterator *) join->leftSrc)->fd);
                        else
                            join->rightSrc = tupleIt(join->rightSrcPos, join->leftSrc->fd);
                    }
                }
            } else return false;
        }
        while (join->rightSrc->base.moveNext((iterator *) join->rightSrc)) {
            join->rightRecord = join->rightSrc->base.current;
            if (joinCmp(join)) {
                return true;
            }
        }
        join->rightSrc = NULL;
        join->leftRecord = NULL;
    }
}

joinStatus joinInit(joinOperator *join,
                    tupleIterator *leftIter,
                    tupleIterator *rightIter,
                    uint16_t leftColumnId,
                    uint16_t rightColumnId,
                    bool isLeftFilter,
                    bool isRightFilter,
                    filterInfo leftFilter,
                    filterInfo rightFilter) {
    join->moveNext = joinMoveNext;
    join->leftSrc = leftIter;
    join->rightSrc = rightIter;
    join->leftColumn = leftColumnId;
    join->rightColumn = rightColumnId;
    join->leftRecord = NULL;
    join->isLeftFilter = isLeftFilter;
    join->isRightFilter = isRightFilter;
    if (isLeftFilter) join->leftFilter = leftFilter;
    if (isRightFilter) join->rightFilter = rightFilter;

    FILE *fd;
    enum data_type dataTypeL;
    enum data_type dataTypeR;
    int64_t leftTablePos;
    int64_t rightTablePos;
    struct _tbSchema *lSchema;
    struct _tbSchema *rSchema;
    if (isLeftFilter) {
        lSchema = ((filterTupleIterator *) leftIter)->schema;
        fd = ((filterTupleIterator *) leftIter)->fd;
        leftTablePos = ((tupleIterator *) ((filterTupleIterator *) leftIter)->from)->tablePos;
    } else {
        lSchema = leftIter->schema;
        leftTablePos = leftIter->tablePos;
        fd = leftIter->fd;
    }
    dataTypeL = lSchema->fields[leftColumnId];

    if (isRightFilter) {
        rSchema = ((filterTupleIterator *) rightIter)->schema;
        rightTablePos = ((tupleIterator *) ((filterTupleIterator *) rightIter)->from)->tablePos;
    } else {
        rSchema = rightIter->schema;
        rightTablePos = rightIter->tablePos;
    }
    dataTypeR = rSchema->fields[rightColumnId];

    join->leftSrcPos = leftTablePos;
    join->rightSrcPos = rightTablePos;

    if (dataTypeL != dataTypeR) {
        printf("%d,%d\n", dataTypeL, dataTypeR);
        return JOIN_DIFFERENT_COLUMN_TYPES;
    } else {
        join->column_type = dataTypeL;
    }

    printf("JOIN on tables:\n");
    struct tableHeader table;
    readTableHeader(fd, leftTablePos, &table);
    int32_t leftId = table.table_id;
    readTableHeader(fd, rightTablePos, &table);
    int32_t rightId = table.table_id;
    printf("%d %16d\n", leftId, rightId);
    printf("%"PRIu16" %16"PRIu16"\n", leftColumnId, rightColumnId);
    printf("%s %8s\n", lSchema->field_labels[leftColumnId], rSchema->field_labels[rightColumnId]);
    char fieldType[10];
    switch (join->column_type) {
        case FIELD_TYPE_STRING:
            strcpy(fieldType, "string");
            break;
        case FIELD_TYPE_FLOAT32:
            strcpy(fieldType, "float");
            break;
        case FIELD_TYPE_INT32:
            strcpy(fieldType, "int");
            break;
        case FIELD_TYPE_BOOL:
            strcpy(fieldType, "bool");
            break;
    }
    printf("%s\n", fieldType);
    return JOIN_OK;
}


int joinedRowsToStr(joinOperator *join, FILE *fd, char *buff, size_t buffSz) {
    struct _tbSchema* lschema;
    if(join->isLeftFilter) lschema = ((filterTupleIterator *) join->leftSrc)->schema;
    else lschema = join->leftSrc->schema;
    struct _tbSchema* rschema;
    if(join->isRightFilter) rschema = ((filterTupleIterator *) join->rightSrc)->schema;
    else rschema = join->rightSrc->schema;

    char tmp[buffSz];
    if (rowToStr(lschema, *(void **) join->leftSrc->base.current, fd, tmp, buffSz) != EXIT_SUCCESS) return EXIT_FAILURE;
    char *t = malloc(sizeof(char) * (strlen(tmp) + 1));
    strcpy(t, tmp);
    if (rowToStr(rschema, *(void **) join->rightSrc->base.current, fd, tmp, buffSz) != EXIT_SUCCESS) return EXIT_FAILURE;
    t = concat(t, tmp);
    if (strlen(t) > buffSz) {
        free(t);
        return EXIT_FAILURE;
    }
    strcpy(buff, t);
    free(t);
    return EXIT_SUCCESS;
}

int joinedRowsHeaderToStr(joinOperator *join, FILE *fd, char *buff, size_t buffSz) {
    char **cols;
    uint16_t colsNumber;
    getColumnNamesJoin(join, &cols, &colsNumber);

    char *tmp = malloc(sizeof(char) * buffSz);
    sprintf(tmp, "%-15s", cols[0]);

    for (uint16_t i = 1; i < colsNumber; ++i) {
        char *t = malloc(sizeof(char) * buffSz);
        sprintf(t, "%-15s", cols[i]);
        tmp = concat(tmp, t);
        free(t);
    }
    if (strlen(tmp) > buffSz) {
        free(tmp);
        return EXIT_FAILURE;
    }
    strcpy(buff, tmp);
    free(tmp);
    destructColumnNames(cols, colsNumber);
    return EXIT_SUCCESS;
}

void getColumnNamesJoin(joinOperator *join, char ***columns, uint16_t *colsNumber) {
    size_t columnsNumber = 0;
    struct _tbSchema *lschema;
    struct _tbSchema *rschema;

    if (join->isLeftFilter) lschema = ((filterTupleIterator *) join->leftSrc)->schema;
    else lschema = join->leftSrc->schema;

    if (join->isRightFilter) rschema = ((filterTupleIterator *) join->rightSrc)->schema;
    else rschema = join->rightSrc->schema;
    uint16_t lcolN = lschema->fields_number;
    uint16_t rcolN = rschema->fields_number;

    columnsNumber += lcolN + rcolN;
    *columns = malloc(columnsNumber * sizeof(char **));
    size_t id = 0;

    uint16_t idCol = lschema->fields_number - 1;
    char *columnId = malloc(sizeof(char) * (strlen(lschema->field_labels[idCol]) + 1));
    strcpy(columnId, lschema->field_labels[idCol]);
    (*columns)[id] = columnId;
    id++;
    for (uint16_t j = 0; j < idCol; ++j) {
        char *column = malloc(sizeof(char) * (strlen(lschema->field_labels[j]) + 1));
        strcpy(column, lschema->field_labels[j]);
        (*columns)[id] = column;
        id++;
    }

    uint16_t rIdCol = rschema->fields_number - 1;
    char *rColumnId = malloc(sizeof(char) * (strlen(rschema->field_labels[rIdCol]) + 1));
    strcpy(rColumnId, rschema->field_labels[rIdCol]);
    (*columns)[id] = rColumnId;
    id++;
    for (uint16_t j = 0; j < rIdCol; ++j) {
        char *column = malloc(sizeof(char) * (strlen(rschema->field_labels[j]) + 1));
        strcpy(column, rschema->field_labels[j]);
        (*columns)[id] = column;
        id++;
    }

    *colsNumber = columnsNumber;
}
