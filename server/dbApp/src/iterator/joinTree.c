//
// Created by danandla on 3/8/23.
//

#include <joinTree.h>
#include <join.h>

#define MAX_VERTICES 50

typedef struct joinGraph {
    void *data;
    struct _tbSchema *schema;
    joinTreeColumnId id;
} joinGraph;

bool bfsFindJoinTreeColumnId(joinGraph *g, joinTree *source) {
    bool visited[MAX_VERTICES];
    for (uint16_t i = 0; i < MAX_VERTICES; ++i) visited[i] = false;

    joinTree *queue[MAX_VERTICES];
    uint8_t front = 0, rear = 0;

    visited[source->nodeId] = true;
    queue[rear++] = source;

    while (front != rear) {
        source = queue[front++];

        if (source->isLeaf) {
            if (source->nodeId == g->id.nodeId) {
                if (source->isJoin) {
                    if (g->id.column < 0) {
                        g->data = source->joinSrc->leftSrc->base.current;
                        g->schema = source->joinSrc->leftSrc->schema;
                    } else {
                        g->data = source->joinSrc->rightSrc->base.current;
                        g->schema = source->joinSrc->rightSrc->schema;
                    }
                } else {
                    g->data = source->iterSrc->base.current;
                    g->schema = source->iterSrc->schema;
                }
                return true;
            }
            continue;
        }

        if (!visited[source->leftLeaf->nodeId]) {
            visited[source->leftLeaf->nodeId] = true;
            queue[rear++] = source->leftLeaf;
        }

        if (!visited[source->rightLeaf->nodeId]) {
            visited[source->rightLeaf->nodeId] = true;
            queue[rear++] = source->rightLeaf;
        }
    }
    return false;
}

uint16_t getColumn(joinTreeColumnId treeColumnId) {
    if (treeColumnId.column < 0) return (uint16_t) -1 - treeColumnId.column;
    else return (uint16_t) treeColumnId.column;
}

bool joinTreeCmp(joinTree *tree) {
    if (tree->isLeaf) {
        if (tree->isJoin) return joinCmp(tree->joinSrc);
        else return true;
    }

    joinGraph graph = {.id = tree->leftColumn};
    if (!bfsFindJoinTreeColumnId(&graph, tree)) {
        printf("column wasn't found");
        return false;
    }
    void *leftData = graph.data;
    struct _tbSchema *leftSchema = graph.schema;
    graph.id = tree->rightColumn;
    if (!bfsFindJoinTreeColumnId(&graph, tree)) {
        printf("column wasn't found");
        return false;
    }
    void *rightData = graph.data;
    struct _tbSchema *rightSchema = graph.schema;

    return fieldCmp(getColumn(tree->leftColumn), leftData, leftSchema,
                    getColumn(tree->rightColumn), rightData, rightSchema,
                    tree->column_type, tree->fd);
}

void bfsRestartTreeNode(joinTree *source) {
    bool visited[MAX_VERTICES];
    for (uint16_t i = 0; i < MAX_VERTICES; ++i) visited[i] = false;

    joinTree *queue[MAX_VERTICES];
    uint8_t front = 0, rear = 0;

    visited[source->nodeId] = true;
    queue[rear++] = source;

    while (front != rear) {
        source = queue[front++];

        if (source->isLeaf) {
            if(source->isJoin){
                source->joinSrc->leftSrc = tupleIt(source->joinSrc->leftSrcPos, source->fd);
                source->joinSrc->rightSrc = tupleIt(source->joinSrc->rightSrcPos, source->fd);
            } else source->iterSrc = tupleIt(source->iterSrcPos, source->fd);
            continue;
        }

        if (!visited[source->leftLeaf->nodeId]) {
            visited[source->leftLeaf->nodeId] = true;
            queue[rear++] = source->leftLeaf;
        }

        if (!visited[source->rightLeaf->nodeId]) {
            visited[source->rightLeaf->nodeId] = true;
            queue[rear++] = source->rightLeaf;
        }
    }
}
void bfsPrintTree(joinTree *source){
    bool visited[MAX_VERTICES];
    for (uint16_t i = 0; i < MAX_VERTICES; ++i) visited[i] = false;

    joinTree *queue[MAX_VERTICES];
    uint8_t front = 0, rear = 0;

    visited[source->nodeId] = true;
    queue[rear++] = source;

    while (front != rear) {
        source = queue[front++];

        if (source->isLeaf) {
            if(source->isJoin){
                printRow(source->joinSrc->leftSrc->schema, *(void**)source->joinSrc->leftSrc->base.current, source->fd);
                printRow(source->joinSrc->rightSrc->schema, *(void**)source->joinSrc->rightSrc->base.current, source->fd);
            } else printRow(source->iterSrc->schema, *(void**)source->iterSrc->base.current, source->fd);
            continue;
        }

        if (!visited[source->leftLeaf->nodeId]) {
            visited[source->leftLeaf->nodeId] = true;
            queue[rear++] = source->leftLeaf;
        }

        if (!visited[source->rightLeaf->nodeId]) {
            visited[source->rightLeaf->nodeId] = true;
            queue[rear++] = source->rightLeaf;
        }
    }
}

bool joinTreeMoveNext(joinTree *tree) {
    if (tree->isLeaf) {
        if (!tree->isJoin) {
            if (tree->iterSrc->base.moveNext(tree->iterSrc)) return true;
        } else {
            if (tree->joinSrc->moveNext(tree->joinSrc)) return true;
        }
        return false;
    }

    while (true) {
        if (!tree->hasLeft) {
            if (joinTreeMoveNext(tree->leftLeaf)) {
                tree->hasLeft = true;
                if (!tree->hasRight)
                    bfsRestartTreeNode(tree->rightLeaf);
            } else return false;
        }
        while (tree->rightLeaf->moveNext(tree->rightLeaf)) {
            if (joinTreeCmp(tree)) return true;
        }
        tree->hasLeft = false;
        tree->hasRight = false;
    }
}
