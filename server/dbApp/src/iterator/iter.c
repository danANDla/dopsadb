//
// Created by danandla on 2/12/23.
//

#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <iter.h>
#include <page_writer.h>
#include <table_util.h>
#include <string_util.h>

#define EPS 0.000001

bool filterTupleItStep(filterTupleIterator *it) {
    while (it->from->moveNext(it->from)) {
        void *item = it->from->current;
        if ((bool)(it->test(item, it->fd, it->schema, it->fieldN, it->param)) == true) {
            it->base.current = item;
            return true;
        }
    }
    free(it);
    return false;
}

filterTupleIterator *filterTupleIt(tupleIterator *from, fpTupleTest test, uint16_t n, void *param) {
    filterTupleIterator *it = (filterTupleIterator *) malloc(sizeof(filterTupleIterator));
    it->base.current = 0;
    it->base.moveNext = (fpMoveNext (*)()) (fpMoveNext) filterTupleItStep;
    it->from = (iterator *) &(from->base);
    it->test = (fpTupleTest (*)()) test;
    it->schema = from->schema;
    it->fieldN = n;
    it->fd = from->fd;
    it->param = param;
    return it;
}

typedef struct {
    iterator base;
    int *arr;
    int index;
    int count;
} arrayIterator;

bool arrayItStep(arrayIterator *it) {
    if (it->index < it->count) {
        it->base.current = &(it->arr[it->index]);
        it->index++;
        return true;
    } else {
        free(it);
        return false;
    }
}

iterator *arrayIt(int *arr, int count) {
    arrayIterator *it = malloc(sizeof(arrayIterator));
    it->arr = arr;
    it->index = 0;
    it->count = count;
    it->base.current = 0;
    it->base.moveNext = (fpMoveNext (*)()) arrayItStep;
    return it;
}


void forEachIt(iterator *it, fpConsume action) {
    while (it->moveNext(it)) {
        action(it->current);
    }
}

void fillTuplesArr(void *pageV, void **arr, size_t *offts, size_t sz) {
    char *page = (char *) pageV;
    struct dataPageHeader dataPage;
    memcpy(&dataPage, page, sizeof(struct dataPageHeader));
    size_t offt = dataPage.firstTuple;
    for (int j = 0; j < dataPage.tuplesNumber; ++j) {
        struct linePointer l;
        memcpy(&l, page + offt, sizeof(struct linePointer));
        while (l.length == 0) {
            offt += sizeof(struct linePointer);
            memcpy(&l, page + offt, sizeof(struct linePointer));
        }
        arr[j] = page + (size_t) l.offset;
        offts[j] = offt;
        offt += sizeof(struct linePointer);
    }
}

void rewriteDirtyPage(tupleIterator *it, struct tableHeader *table, struct superHeader *super) {
    struct dataPageHeader page;
    memcpy(&page, it->page, sizeof(struct dataPageHeader));
    if (page.tuplesNumber == 0 && it->tablePos != it->curPagePos) {
        struct dataPageHeader tablePage;
        int64_t tablePagePos = it->tablePos;
        readDataPageHeader(it->fd, tablePagePos, &tablePage);
        while (tablePage.next != it->curPagePos) {
            tablePagePos = tablePage.next;
            readDataPageHeader(it->fd, tablePagePos, &tablePage);
        }
        tablePage.next = page.next;
        fseek(it->fd, tablePagePos, SEEK_SET);
        fwrite(&tablePage, sizeof(struct dataPageHeader), 1, it->fd);
        freeDataPage(it->fd, &it->curPagePos);
        super->dataPagesNumber -= 1;
        table->pages_cnt -= 1;
        if (table->lastPage == it->curPagePos) table->lastPage = tablePagePos;
        updateTableHeader(it->fd, it->tablePos, table);
        updateSuperHeader(it->fd, super);
    } else {
        fseek(it->fd, it->curPagePos, SEEK_SET);
        fwrite(it->page, super->pageSize, 1, it->fd);
    }
}

bool tupleItStep(tupleIterator *it) {
    if (it->index < it->count) {
        it->base.current = &(it->arr[it->index]);
        it->index++;
        return true;
    } else {
        struct superHeader super;
        readSuperHeader(it->fd, &super);
        struct tableHeader table;
        readTableHeader(it->fd, it->tablePos, &table);
        struct dataPageHeader page;

        readDataPageHeader(it->fd, it->curPagePos, &page);
        if (it->pageN + 1 < table.pages_cnt) {
            struct _tbSchema *schema = init_table_schema();
            readSchema(it->fd, table.schema, schema);

            if (it->isDirty) rewriteDirtyPage(it, &table, &super);

            fseek(it->fd, page.next, SEEK_SET);
            fread(it->page, super.pageSize, 1, it->fd);

            it->curPagePos = page.next;
            memcpy(&page, it->page, sizeof(page));
            it->arr = (void **) realloc(it->arr, sizeof(void *) * page.tuplesNumber);
            it->offts = (size_t *) realloc(it->offts, sizeof(size_t) * page.tuplesNumber);
            fillTuplesArr(it->page, it->arr, it->offts, getSchemaSize(schema));


            it->pageN += 1;
            it->index = 1;
            it->count = page.tuplesNumber;
            it->base.current = &(it->arr[0]);
            schemaDestruct(schema);
            return true;
        } else {
            if (it->isDirty) rewriteDirtyPage(it, &table, &super);
            free(it->page);
            schemaDestruct(it->schema);
            free(it->arr);
            free(it->offts);
            free(it);
            return false;
        }
    }
}

tupleIterator *tupleIt(int64_t tablePos, FILE *fd) {
    struct superHeader super;
    readSuperHeader(fd, &super);

    void *page = malloc(super.pageSize);
    fseek(fd, tablePos, SEEK_SET);
    fread(page, super.pageSize, 1, fd);

    struct dataPageHeader tablePage;
    memcpy(&tablePage, page, sizeof(tablePage));
    struct tableHeader table;
    memcpy(&table, (char *) page + sizeof(struct dataPageHeader), sizeof(table));
    struct _tbSchema *schema = init_table_schema();
    readSchema(fd, table.schema, schema);
    uint16_t sz = getSchemaSize(schema);

    void **arr = malloc(sizeof(void *) * tablePage.tuplesNumber);
    size_t *offts = malloc(sizeof(size_t) * tablePage.tuplesNumber);
    fillTuplesArr(page, arr, offts, sz);


    tupleIterator *it = malloc(sizeof(tupleIterator));
    it->arr = arr;
    it->index = 0;
    it->count = tablePage.tuplesNumber;
    it->tablePos = tablePos;
    it->curPagePos = tablePos;
    it->pageN = 0;
    it->fd = fd;
    it->base.current = 0;
    it->base.moveNext = tupleItStep;
    it->page = page;
    it->schema = schema;
    it->offts = offts;
    it->isDirty = false;
    return it;
}

bool updateTuple(tupleIterator *it, void *newData, uint16_t newFieldN) {
    size_t sz = getFieldTypeSize(&(it->schema->fields[newFieldN]));
    size_t offt = 0;
    for (uint16_t i = 0; i < newFieldN; ++i) offt += getFieldTypeSize(&(it->schema->fields[i]));
    void **oldDataAdr = it->base.current;
    char *oldData = *((char **) oldDataAdr) + offt;
    if (it->schema->fields[newFieldN] == FIELD_TYPE_STRING) {
        struct tid strTid;
        writeString(it->fd, *(char **) ((void *) &newData), &strTid);
        memcpy(oldData, &strTid, sz);
    } else {
        memcpy(oldData, newData, sz);
    }
    it->isDirty = true;
    return true;
}

bool deleteTuple(tupleIterator *it) {
    struct dataPageHeader dataPage;
    memcpy(&dataPage, it->page, sizeof(struct dataPageHeader));
    struct linePointer l = {0, 0};
    memcpy((char *) it->page + it->offts[it->index - 1], &l, sizeof(struct linePointer));
    if (it->index == it->count) {
        dataPage.pgLower -= sizeof(struct linePointer);
        dataPage.pgUpper += getTupleSize(it->schema);
    }
    dataPage.tuplesNumber -= 1;
    memcpy(it->page, &dataPage, sizeof(dataPage));
    it->isDirty = true;
}


bool strEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param) {
    char *data = getStrField(fieldN, schema, tupleAddr, fd);
    if (strcmp(data, (char *) param) == 0) {
        free(data);
        return true;
    }
    free(data);
    return false;
}

bool strNotEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param) {
    char *data = getStrField(fieldN, schema, tupleAddr, fd);
    if (strcmp(data, (char *) param) != 0) {
        free(data);
        return true;
    }
    free(data);
    return false;
}

bool intEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param) {
    return getIntField(fieldN, schema, tupleAddr) == *(uint32_t *) param;
}

bool intNotEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param) {
    return getIntField(fieldN, schema, tupleAddr) != *(uint32_t *) param;
}

bool intGreaterThanParamFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param) {
    return getIntField(fieldN, schema, tupleAddr) > *(uint32_t *) param;
}

bool intLessThanParamFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param) {
    return getIntField(fieldN, schema, tupleAddr) < *(uint32_t *) param;
}

bool floatEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param) {
    bool res = fabs(getFloatField(fieldN, schema, tupleAddr) - *(float *) param) <= EPS;
    return res;
}

bool floatNotEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param) {
    bool res = fabs(getFloatField(fieldN, schema, tupleAddr) - *(float *) param) > EPS;
    return res;
}

bool floatGreaterThanParamFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param) {
    bool res = getFloatField(fieldN, schema, tupleAddr) > *(float *) param;
    return res;
}

bool floatLessThanParamFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param) {
    bool res =  *(float *) param > getFloatField(fieldN, schema, tupleAddr);
    return res;
}

bool boolEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param) {
    return (getBoolField(fieldN, schema, tupleAddr) == 0) == (*(uint8_t *) param == 0);
}

bool boolNotEqualFieldTest(void *tupleAddr, FILE *fd, struct _tbSchema *schema, uint16_t fieldN, void *param) {
    return (getBoolField(fieldN, schema, tupleAddr) == 0) != (*(uint8_t *) param == 0);
}

void getColumnNames(uint16_t tablesNumber, tupleIterator **arr, char ***columns, uint16_t *colsNumber) {
    size_t columnsNumber = 0;
    for (uint16_t i = 0; i < tablesNumber; ++i) columnsNumber += arr[i]->schema->fields_number;
    *columns = malloc(columnsNumber * sizeof(char **));
    size_t id = 0;
    for (uint16_t i = 0; i < tablesNumber; ++i) {
        uint16_t idCol = arr[i]->schema->fields_number - 1;
        char *column = malloc(sizeof(char) * (strlen(arr[i]->schema->field_labels[idCol]) + 1));
        strcpy(column, arr[i]->schema->field_labels[idCol]);
        (*columns)[id] = column;
        id++;
        for (uint16_t j = 0; j < idCol; ++j) {
            char *column = malloc(sizeof(char) * (strlen(arr[i]->schema->field_labels[j]) + 1));
            strcpy(column, arr[i]->schema->field_labels[j]);
            (*columns)[id] = column;
            id++;
        }
    }
    *colsNumber = columnsNumber;
}

void destructColumnNames(char **columns, uint16_t columnsNumber) {
    for (uint16_t i = 0; i < columnsNumber; ++i) free(columns[i]);
    free(columns);
}
