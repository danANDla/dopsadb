//
// Created by danandla on 2/25/23.
//

#include <string.h>
#include <query.h>
#include <string_util.h>

void printTuple(tupleIterator *it) {
    struct _tbSchema *schema = it->schema;
    void *data = *(void **) it->base.current;
    FILE *fd = it->fd;
    printRow(data, schema, fd);
}

void selectQuery(filterTupleIterator* filter){
    printSchemaHeader(filter->schema, filter->fd);
    while (filter->base.moveNext((iterator *) filter)){
        printRow(filter->schema, *(void **) filter->base.current,  filter->fd);
        printf("\n");
    }
}

void updateQuery(filterTupleIterator* filter, void* newValue, uint16_t fieldN){
    while (filter->base.moveNext((iterator *) filter))
        updateTuple((tupleIterator *) filter->from, (void *) newValue, fieldN);
}

void deleteQuery(filterTupleIterator* filter){
    while (filter->base.moveNext((iterator *) filter))
        deleteTuple((tupleIterator *) filter->from);
}

void doQuery(dbQuery query){
    if(query.condition.connective != CMP_CONNECTIVE) return;

    fpTupleTest intFuncs[] = {intEqualFieldTest,
                              intNotEqualFieldTest,
                              intGreaterThanParamFieldTest,
                              intLessThanParamFieldTest};
    fpTupleTest floatFuncs[] = {floatEqualFieldTest,
                                floatNotEqualFieldTest,
                                floatGreaterThanParamFieldTest,
                                floatLessThanParamFieldTest};
    fpTupleTest boolFuncs[] = {boolEqualFieldTest,
                               boolNotEqualFieldTest,
                               boolEqualFieldTest,
                               boolNotEqualFieldTest};
    fpTupleTest sFuncs[] = {strEqualFieldTest,
                            strNotEqualFieldTest,
                            strEqualFieldTest,
                            strNotEqualFieldTest};

    fpTupleTest* funcArr;
    switch (query.condition.type) {
        case FIELD_TYPE_STRING:
            funcArr = sFuncs;
            break;
        case FIELD_TYPE_FLOAT32:
            funcArr = floatFuncs;
            break;
        case FIELD_TYPE_INT32:
            funcArr = intFuncs;
            break;
        case FIELD_TYPE_BOOL:
            funcArr = boolFuncs;
            break;
    }

    fpTupleTest tupleTest;
    switch (query.condition.cmp) {
        case CMP_EQUAL:
            tupleTest = funcArr[0];
            break;
        case CMP_NEQUAL:
            tupleTest = funcArr[1];
            break;
        case CMP_GREATER:
            tupleTest = funcArr[2];
            break;
        case CMP_LOWER:
            tupleTest = funcArr[3];
            break;
    }

    filterTupleIterator* filter = filterTupleIt(query.srcTable,
                                                tupleTest,
                                                query.condition.fieldN,
                                                query.condition.data);
    switch (query.action) {
        case ACTION_SELECT:
            selectQuery(filter);
            break;
        case ACTION_UPDATE:{
            updateQuery(filter, query.updateData, query.condition.fieldN);
            break;
        }
        case ACTION_DELETE:
            deleteQuery(filter);
            break;
    }
}
