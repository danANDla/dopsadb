//
// Created by danandla on 11/15/22.
//

#include <string.h>
#include <schema_util.h>
#include <page_writer.h>
#include <string_util.h>
#include <ast.h>

struct _tbSchema *init_table_schema() {
    struct _tbSchema *sh = malloc(sizeof(struct _tbSchema));
    sh->fields_number = 0;
    sh->fields = malloc(sizeof(enum data_type));
    sh->field_labels = malloc(sizeof(char *));
    return sh;
}


enum schema_status expand_table_schema(struct _tbSchema *schema, enum data_type field_type, char *field_label) {
    uint16_t schema_size = schema->fields_number;

    enum data_type *tmp_fields = realloc(schema->fields, (schema_size + 1) * sizeof(enum data_type));
    if (!tmp_fields) return EXPAND_SCHEMA_ERROR;

    char **tmp_fields_labels = realloc(schema->field_labels, (schema_size + 1) * sizeof(char *));
    if (!tmp_fields_labels) return EXPAND_SCHEMA_ERROR;

    schema->fields = tmp_fields;
    schema->field_labels = tmp_fields_labels;

    schema->fields[schema_size] = field_type;
    schema->field_labels[schema_size] = field_label;
    schema->fields_number += 1;

    return EXPAND_SCHEMA_OK;
}

void print_schema(const struct _tbSchema *const schema) {
    for (int i = 0; i < schema->fields_number; ++i) {
        printf("%s, type: %d\n", schema->field_labels[i], (int) schema->fields[i]);
    }
}

uint16_t getSchemaSize(const struct _tbSchema *const schema) {
    uint16_t sz = 0;
    sz += sizeof(schema->fields_number);
    sz += sizeof(enum data_type) * schema->fields_number;
    for (int i = 0; i < schema->fields_number; ++i) {
        sz += sizeof(schema->field_labels[i]);
        sz += sizeof(size_t);
    }
    return sz;
}

void fillSchemaBuff(struct _tbSchema *schema, void **schemaBuff, uint16_t *schemaSize) {
    *schemaSize = getSchemaSize(schema);
    uint16_t sz = 0;
    *schemaBuff = malloc(*schemaSize);

    memcpy(*schemaBuff, &schema->fields_number, sizeof(schema->fields_number));
    sz += sizeof(schema->fields_number);
    for (uint16_t i = 0; i < schema->fields_number; ++i) {
        enum data_type dtype = schema->fields[i];
        memcpy(*((char **) schemaBuff) + sz, &dtype, sizeof(dtype));
        sz += sizeof(dtype);
    }

    for (uint16_t i = 0; i < schema->fields_number; ++i) {
        sprintf(*((char **) schemaBuff) + sz, "%s", schema->field_labels[i]);
        sz += strlen(schema->field_labels[i]) + 1;
    }
}

enum writeStatus writeSchema(FILE *out, struct _tbSchema *schema, struct tid *id) {
    expand_table_schema(schema, FIELD_TYPE_INT32, "id");
    void *schemaBuff;
    uint16_t sz;
    fillSchemaBuff(schema, &schemaBuff, &sz);

    struct superHeader super;
    readSuperHeader(out, &super);
    int64_t pos = super.schemasPage;
    struct dataPageHeader schemaPage;
    readDataPageHeader(out, pos, &schemaPage);

    uint16_t pageId = 0;
    while (schemaPage.next != -1 &&
           schemaPage.pgLower + sizeof(struct linePointer) >= schemaPage.pgUpper - sz) {
        pos = schemaPage.next;
        readDataPageHeader(out, pos, &schemaPage);
        pageId++;
    }
    if (schemaPage.pgLower + sizeof(struct linePointer) >= schemaPage.pgUpper - sz) {
        int64_t newPos;
        allocFreePage(out, &newPos);
        schemaPage.next = newPos;
        updateDataPageHeader(out, pos, &schemaPage);
        pos = newPos;
        pageId++;
    }
    addDataToPage(out, pos, sz, schemaBuff, id);
    id->pageId = pageId;
    free(schemaBuff);
    return WRITE_OK;
}

enum schema_status readSchema(FILE *in, struct tid id, struct _tbSchema *schema) {
    struct superHeader super;
    readSuperHeader(in, &super);

    struct dataPageHeader schemasPage;
    int64_t schemaPos = super.schemasPage;
    readDataPageHeader(in, schemaPos, &schemasPage);
    for (int i = 0; i < id.pageId; ++i) {
        schemaPos = schemasPage.next;
        readDataPageHeader(in, schemaPos, &schemasPage);
    }
    struct linePointer schemaPointer;
    fseek(in, schemaPos
              + schemasPage.pgLower
              - (schemasPage.tuplesNumber - id.tupleId) * sizeof(struct linePointer),
          SEEK_SET);
    fread(&schemaPointer, sizeof(struct linePointer), 1, in);
    uint8_t *schemaBuff = malloc(schemaPointer.length * sizeof(uint8_t));
    fseek(in, schemaPos + schemaPointer.offset, SEEK_SET);
    fread(schemaBuff, schemaPointer.length, 1, in);


    uint16_t fieldsNumber;
    memcpy(&fieldsNumber, schemaBuff, sizeof(uint16_t));
    size_t offt = sizeof(uint16_t);
    schema->fields_number = fieldsNumber;

    schema->fields = realloc(schema->fields, sizeof(enum data_type) * fieldsNumber);
    for (uint16_t i = 0; i < fieldsNumber; ++i) {
        enum data_type dtype;
        memcpy(&dtype, schemaBuff + offt, sizeof(enum data_type));
        schema->fields[i] = dtype;
        offt += sizeof(enum data_type);
    }

    schema->field_labels = realloc(schema->field_labels, sizeof(char *) * fieldsNumber);
    for (uint16_t i = 0; i < fieldsNumber; ++i) {
        char *str;
        sscanf(schemaBuff + offt, "%ms", &str);
        offt += strlen(str) + 1;
        schema->field_labels[i] = (char *) malloc(sizeof(char) * (strlen(str) + 1));
        strcpy(schema->field_labels[i], str);
        free(str);
    }

    free(schemaBuff);
    return READ_SCHEMA_OK;
}

void schemaDestruct(struct _tbSchema *schema) {
    for (uint16_t i = 0; i < schema->fields_number; ++i) free(schema->field_labels[i]);
    free(schema->field_labels);
    free(schema->fields);
    free(schema);
}

size_t getFieldTypeSize(enum data_type *f) {
    size_t sz;
    switch (*f) {
        case FIELD_TYPE_INT32: {
            sz = sizeof(int32_t);
            break;
        }
        case FIELD_TYPE_FLOAT32: {
            sz = sizeof(float);
            break;
        }
        case FIELD_TYPE_BOOL: {
            sz = sizeof(uint8_t);
            break;
        }
        case FIELD_TYPE_STRING: {
            sz = sizeof(struct linePointer);
            break;
        }
    }
    return sz;

}

size_t getTupleSize(struct _tbSchema *schema) {
    size_t sz = 0;
    for (uint16_t i = 0; i < schema->fields_number; ++i) sz += getFieldTypeSize(&(schema->fields[i]));
    return sz;
}

int32_t getIntField(uint16_t fieldN, struct _tbSchema *schema, void *tupleAddr) {
    size_t offt = 0;
    for (uint16_t i = 0; i < fieldN; ++i) offt += getFieldTypeSize(&schema->fields[i]);
    int32_t data;
    void *tuple = *(void **) tupleAddr;
    memcpy(&data, (char *) tuple + offt, sizeof data);
    return data;
}

char *getStrField(uint16_t fieldN, struct _tbSchema *schema, void *tupleAddr, FILE *fd) {
    size_t offt = 0;
    for (uint16_t i = 0; i < fieldN; ++i) offt += getFieldTypeSize(&schema->fields[i]);
    struct tid strTid;
    void *tuple = *(void **) tupleAddr;
    memcpy(&strTid, (char *) tuple + offt, sizeof strTid);
    char *data;
    readString(fd, strTid, &data);
    return data;
}

float getFloatField(uint16_t fieldN, struct _tbSchema *schema, void *tupleAddr) {
    size_t offt = 0;
    for (uint16_t i = 0; i < fieldN; ++i) offt += getFieldTypeSize(&schema->fields[i]);
    float data;
    void *tuple = *(void **) tupleAddr;
    memcpy(&data, (char *) tuple + offt, sizeof data);
    return data;
}

uint8_t getBoolField(uint16_t fieldN, struct _tbSchema *schema, void *tupleAddr) {
    size_t offt = 0;
    for (uint16_t i = 0; i < fieldN; ++i) offt += getFieldTypeSize(&schema->fields[i]);
    uint8_t data;
    void *tuple = *(void **) tupleAddr;
    memcpy(&data, (char *) tuple + offt, sizeof data);
    return data;
}

uint8_t strEqualTest(void *leftStr, void *rightStr) {
    return strcmp((char *) leftStr, (char *) rightStr) == 0;
}

uint8_t floatEqualTest(void *left, void *right) {
    return *(float *) left == *(float *) right;
}

uint8_t intEqualTest(void *left, void *right) {
    return *(int *) left == *(int *) right;
}

uint8_t boolEqualTest(void *left, void *right) {
    return (*(uint8_t *) left == 0) == (*(uint8_t *) right == 0);
}

int schemaHeaderToStr(struct _tbSchema *schema, char* buff, size_t buffSz) {
    char* tmp = malloc(sizeof(char) * buffSz);
    sprintf(tmp, "%-15s", schema->field_labels[schema->fields_number - 1]);
    for (int i = 0; i < schema->fields_number - 1; ++i) {
        char* t = malloc(sizeof(char) * buffSz);
        sprintf(t, "%-15s", schema->field_labels[i]);
        tmp = concat(tmp, t);
        free(t);
    }
    if(strlen(tmp) > buffSz) {
        free(tmp);
        return EXIT_FAILURE;
    }
    strcpy(buff, tmp);
    free(tmp);
    return EXIT_SUCCESS;
}

void printSchemaHeader(struct _tbSchema *schema, FILE *fd) {
    size_t offt = getTupleSize(schema) - sizeof(int32_t);
    offt = 0;
    printf("%-15s", schema->field_labels[schema->fields_number - 1]);
    for (int i = 0; i < schema->fields_number - 1; ++i) printf("%-15s", schema->field_labels[i]);
    printf("\n");
}

void printRow(struct _tbSchema *schema, void *data, FILE *fd) {
    size_t offt = getTupleSize(schema) - sizeof(int32_t);
    int32_t id;
    memcpy(&id, (char *) data + offt, sizeof(id));
    printf("%-15" PRId32 "", id);
    offt = 0;
    for (int i = 0; i < schema->fields_number - 1; ++i) {
        switch (schema->fields[i]) {
            case FIELD_TYPE_INT32: {
                int32_t n;
                memcpy(&n, (char *) data + offt, sizeof(n));
                printf("%-15" PRId32 "", n);
                offt += sizeof(n);
                break;
            }
            case FIELD_TYPE_FLOAT32: {
                float n;
                memcpy(&n, (char *) data + offt, sizeof(n));
                printf("%-15f", n);
                offt += sizeof(n);
                break;
            }
            case FIELD_TYPE_BOOL: {
                uint8_t b;
                memcpy(&b, (char *) data + offt, sizeof(b));
                char ch;
                if (b) ch = 't';
                else ch = 'f';
                printf("%-15c", ch);
                offt += sizeof(b);
                break;
            }
            case FIELD_TYPE_STRING: {
                struct tid strTid;
                memcpy(&strTid, (char *) data + offt, sizeof(strTid));
                char *str;
                readString(fd, strTid, &str);
                printf("%-15s", str);
                offt += sizeof(struct tid);
                free(str);
                break;
            }
        }
    }
}

int rowToStr(struct _tbSchema *schema, void *data, FILE* fd, char* buff, size_t buffSz) {
    char* tmp = malloc(sizeof(char) * buffSz);

    size_t offt = getTupleSize(schema) - sizeof(int32_t);
    int32_t id;
    memcpy(&id, (char *) data + offt, sizeof(id));
    sprintf(tmp, "%-15" PRId32 "", id);
    offt = 0;
    for (int i = 0; i < schema->fields_number - 1; ++i) {
        switch (schema->fields[i]) {
            case FIELD_TYPE_INT32: {
                int32_t n;
                memcpy(&n, (char *) data + offt, sizeof(n));

                char* t = malloc(sizeof(char) * buffSz);
                sprintf(t, "%-15" PRId32 "", n);
                tmp = concat(tmp, t);
                free(t);

                offt += sizeof(n);
                break;
            }
            case FIELD_TYPE_FLOAT32: {
                float n;
                memcpy(&n, (char *) data + offt, sizeof(n));

                char* t = malloc(sizeof(char) * buffSz);
                sprintf(t, "%-15f", n);
                tmp = concat(tmp, t);
                free(t);

                offt += sizeof(n);
                break;
            }
            case FIELD_TYPE_BOOL: {
                uint8_t b;
                memcpy(&b, (char *) data + offt, sizeof(b));
                char ch;
                if (b) ch = 't';
                else ch = 'f';

                char* t = malloc(sizeof(char) * buffSz);
                sprintf(t, "%-15c", ch);
                tmp = concat(tmp, t);
                free(t);

                offt += sizeof(b);
                break;
            }
            case FIELD_TYPE_STRING: {
                struct tid strTid;
                memcpy(&strTid, (char *) data + offt, sizeof(strTid));
                char *str;
                readString(fd, strTid, &str);

                char* t = malloc(sizeof(char) * buffSz);
                sprintf(t, "%-15s", str);
                tmp = concat(tmp, t);
                free(t);

                offt += sizeof(struct tid);
                free(str);
                break;
            }
        }
    }
    if(strlen(tmp) > buffSz) {
        free(tmp);
        return EXIT_FAILURE;
    }
    strcpy(buff, tmp);
    free(tmp);
    return EXIT_SUCCESS;
}
