//
// Created by danandla on 2/11/23.
//

#include <stdlib.h>
#include <string.h>
#include <string_util.h>
#include <page_writer.h>


enum readStatus readString(FILE *in, struct tid id, char** str){
    struct superHeader super;
    readSuperHeader(in, &super);

    int64_t pos = super.stringsPage;
    struct dataPageHeader strPage;
    readDataPageHeader(in, pos, &strPage);

    for(uint16_t i = 0; i < id.pageId; ++i){
        pos = strPage.next;
        readDataPageHeader(in, pos, &strPage);
    }

    struct linePointer strTuple;
    fseek(in, pos
              + strPage.pgLower
              - (strPage.tuplesNumber - id.tupleId) * sizeof(struct linePointer),
          SEEK_SET);
    fread(&strTuple, sizeof(struct linePointer), 1, in);
    *str = (char*) malloc(strTuple.length * sizeof(char) + 1);
    fseek(in, pos + strTuple.offset, SEEK_SET);
    fread(*str, strTuple.length, 1, in);
    *(*str + strTuple.length) = '\0';
}

enum writeStatus writeString(FILE *out, char *str, struct tid *id) {
    struct superHeader super;
    readSuperHeader(out, &super);

    int64_t pos = super.lastStrPage;
    struct dataPageHeader strPage;
    readDataPageHeader(out, pos, &strPage);

    uint16_t pageId = super.lastStrPageId;

    while(strPage.next != -1 && strPage.pgLower + sizeof (struct linePointer) >= strPage.pgUpper - strlen(str)){
        pos = strPage.next;
        readDataPageHeader(out, pos, &strPage);
        pageId++;
    }
    if(strPage.pgLower + sizeof (struct linePointer) >= strPage.pgUpper - strlen(str)){
        int64_t newPos;
        allocFreePage(out, &newPos);
        strPage.next = newPos;
        updateDataPageHeader(out, pos, &strPage);
        pos = newPos;
        pageId++;
        super.lastStrPageId = pageId;
        super.lastStrPage = newPos;
        updateSuperHeader(out, &super);
    }
    addDataToPage(out, pos, strlen(str), str, id);
    id->pageId = pageId;

    return WRITE_OK;
}
