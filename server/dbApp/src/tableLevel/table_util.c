//
// Created by danandla on 11/15/22.
//

#include <string.h>
#include <table_util.h>
#include <string_util.h>

enum writeStatus updateTableHeader(FILE *out, int64_t tablePos, const struct tableHeader *const table);

enum writeStatus createTable(FILE *out, struct _tbSchema *schema, char *tName) {
    struct superHeader super;
    if (readSuperHeader(out, &super) != READ_OK) return WRITE_ERROR;

    int64_t free;
    allocFreePage(out, &free);

    struct dataPageHeader freePage;
    if (readDataPageHeader(out, free, &freePage) != READ_OK) return WRITE_ERROR;
    freePage.pgLower += sizeof(struct tableHeader);
    freePage.firstTuple = freePage.pgLower;
    if (updateDataPageHeader(out, free, &freePage) != WRITE_OK) return WRITE_ERROR;

    if (readSuperHeader(out, &super) != READ_OK) return WRITE_ERROR;

    struct tid id;
    if (writeSchema(out, schema, &id) != WRITE_OK) return WRITE_ERROR;

    struct tid strId;
    if (writeString(out, tName, &strId) != WRITE_OK) return WRITE_ERROR;

    struct tableHeader table = {
            .table_id = super.tableId,
            .pages_cnt = 1,
            .schema = id,
            .next = -1,
            .lastPage = free,
            .id = 0,
            .tableName = strId
    };

    fseek(out, free + sizeof(struct dataPageHeader), SEEK_SET);
    if (fwrite(&table, sizeof(struct tableHeader), 1, out) < 1) return WRITE_ERROR;

    if (super.dataPagesNumber < 1) {
        super.firstTable = free;
        super.lastTable = free;
        super.dataPagesNumber = 0;
    } else {
        struct tableHeader lastTable;
        if (readTableHeader(out, super.lastTable, &lastTable) != READ_OK) return WRITE_ERROR;
        lastTable.next = free;
        fseek(out, super.lastTable + sizeof(struct dataPageHeader), SEEK_SET);
        if (fwrite(&lastTable, sizeof(struct tableHeader), 1, out) < 1) return WRITE_ERROR;
        super.lastTable = free;
    }
    super.dataPagesNumber += 1;
    super.tableId += 1;

    if (updateSuperHeader(out, &super) != WRITE_OK) return WRITE_ERROR;
    return WRITE_OK;
}

enum readStatus getTableIdByName(FILE *in, char *tname, uint64_t *id) {
    struct superHeader super;
    readSuperHeader(in, &super);

    struct tableHeader table;
    int64_t tablePos = super.firstTable;
    readTableHeader(in, tablePos, &table);

    char *tableName;
    readString(in, table.tableName, &tableName);
    *id = table.table_id;
    while (strcmp(tableName, tname) != 0 && table.next != -1) {
        tablePos = table.next;
        readTableHeader(in, tablePos, &table);
        free(tableName);
        readString(in, table.tableName, &tableName);
        *id = table.table_id;
    }
    if (strcmp(tableName, tname) != 0) {
        *id = -1;
        free(tableName);
        return READ_ERROR;
    }
    free(tableName);
    return READ_OK;
}

enum readStatus getSchemaByTableId(FILE *in, uint64_t id, struct _tbSchema **schema) {
    struct tableHeader table;
    int64_t tablePos;
    if (getTableById(in, &table, id, &tablePos) != READ_OK) return READ_ERROR;
    struct tid schemaId = table.schema;
    *schema = init_table_schema();
    readSchema(in, schemaId, *schema);
    return READ_OK;
}

enum readStatus getColumnIdByName(FILE *in, uint64_t id, char *cname, uint16_t *columnId) {
    struct _tbSchema *schema;
    getSchemaByTableId(in, id, &schema);
    for (uint16_t i = 0; i < schema->fields_number; ++i) {
        if (strcmp(cname, schema->field_labels[i]) == 0) {
            *columnId = i;
            schemaDestruct(schema);
            return READ_OK;
        }
    }
    schemaDestruct(schema);
    return READ_ERROR;
}

enum readStatus getTableById(FILE *const in, struct tableHeader *table, uint64_t id, int64_t *tablePos) {
    struct superHeader super;
    readSuperHeader(in, &super);

    *tablePos = super.firstTable;
    readTableHeader(in, *tablePos, table);
    while (table->table_id != id && table->next != -1) {
        *tablePos = table->next;
        readTableHeader(in, *tablePos, table);
    }
    if (table->table_id != id) {
        *tablePos = -1;
        return READ_ERROR;
    }
    return READ_OK;
}

enum writeStatus pushPageToTable(FILE *out, const int64_t tablePagePos) {
    struct superHeader super;
    if (readSuperHeader(out, &super) != READ_OK) return WRITE_ERROR;

    struct tableHeader table;
    if (readTableHeader(out, tablePagePos, &table) != READ_OK) return WRITE_ERROR;

    struct dataPageHeader lastTablePage;
    int64_t lastPagePos = table.lastPage;
    if (readDataPageHeader(out, table.lastPage, &lastTablePage) != READ_OK) return WRITE_ERROR;

    int64_t newPage;
    allocFreePage(out, &newPage);

    if (readSuperHeader(out, &super) != READ_OK) return WRITE_ERROR;
    super.dataPagesNumber += 1;

    lastTablePage.next = newPage;
    table.lastPage = newPage;
    table.pages_cnt += 1;

    if (updateDataPageHeader(out, lastPagePos, &lastTablePage) != WRITE_OK) return WRITE_ERROR;
    if (updateTableHeader(out, tablePagePos, &table) != WRITE_OK) return WRITE_ERROR;
    if (updateSuperHeader(out, &super) != WRITE_OK) return WRITE_ERROR;
    return WRITE_OK;
}

enum writeStatus deletePageFromTable(FILE *out, const int64_t tablePagePos, uint32_t pageNumber) {
    struct superHeader super;
    if (readSuperHeader(out, &super) != READ_OK) return WRITE_ERROR;

    struct tableHeader table;
    if (readTableHeader(out, tablePagePos, &table) != READ_OK) return WRITE_ERROR;

    if (pageNumber > table.pages_cnt - 1 || pageNumber == 0) return WRITE_ERROR;

    struct dataPageHeader tablePage;
    int64_t page = tablePagePos;
    if (readDataPageHeader(out, page, &tablePage) != READ_OK) return WRITE_ERROR;

    for (int32_t i = 1; i < pageNumber; i++) {
        page = tablePage.next;
        if (readDataPageHeader(out, page, &tablePage) != READ_OK) return WRITE_ERROR;
    }

    struct dataPageHeader prevPage = tablePage;
    int64_t prevPagePos = page;
    int64_t deletingPagePos = tablePage.next;
    if (pageNumber == table.pages_cnt - 1) {
        table.lastPage = prevPagePos;
        prevPage.next = -1;
    } else {
        struct dataPageHeader deletingPage;
        if (readDataPageHeader(out, deletingPagePos, &deletingPage) != READ_OK) return WRITE_ERROR;
        prevPage.next = deletingPage.next;
    }
    table.pages_cnt -= 1;

    freeDataPage(out, &deletingPagePos);

    if (readSuperHeader(out, &super) != READ_OK) return WRITE_ERROR;
    super.dataPagesNumber -= 1;

    if (updateSuperHeader(out, &super) != WRITE_OK) return WRITE_ERROR;
    if (updateTableHeader(out, tablePagePos, &table) != WRITE_OK) return WRITE_ERROR;
    if (updateDataPageHeader(out, prevPagePos, &prevPage) != WRITE_OK) return WRITE_ERROR;
    return WRITE_OK;
}

enum writeStatus updateTableHeader(FILE *out, int64_t tablePos, const struct tableHeader *const table) {
    fseek(out, tablePos + sizeof(struct dataPageHeader), SEEK_SET);
    if (fwrite(table, sizeof(struct tableHeader), 1, out) < 1) return WRITE_ERROR;
    return WRITE_OK;
}

enum readStatus readTableHeader(FILE *const in, const int64_t pagePos, struct tableHeader *header) {
    fseek(in, pagePos + sizeof(struct dataPageHeader), SEEK_SET);
    if (!fread(header, sizeof(struct tableHeader), 1, in)) return READ_ERROR;
    return READ_OK;
}

void printTableHeader(const struct tableHeader *const header) {
    printf("table id: %" PRIu16 "\n", header->table_id);
    printf("number of pages: %" PRIu32 "\n", header->pages_cnt);
    printf("last page: %" PRId64 "\n", header->lastPage);
    printf("schema tid: %" PRId16 " page %" PRId16 " tuple\n", header->schema.pageId, header->schema.tupleId);
}

void printAllPagesAddresses(FILE *in, const int64_t tablePagePos) {
    struct tableHeader table;
    readTableHeader(in, tablePagePos, &table);
    printf("table [%" PRIu16 "]\n", table.table_id);
    printf("%" PRIu32 "pages\n", table.pages_cnt);

    struct dataPageHeader page;
    int64_t pagePos = tablePagePos;
    readDataPageHeader(in, tablePagePos, &page);
    printf("%" PRId64, pagePos);

    while (page.next != -1) {
        pagePos = page.next;
        readDataPageHeader(in, pagePos, &page);
        printf(" -> %" PRId64, pagePos);
    }
    printf("\n");
}

enum writeStatus addDataToTable(FILE *out, const uint64_t tableId, void **data, int32_t* id) {
    struct tableHeader table;
    int64_t tablePos;
    if (getTableById(out, &table, tableId, &tablePos) != READ_OK) return WRITE_ERROR;

    struct _tbSchema *schema = init_table_schema();
    readSchema(out, table.schema, schema);
    size_t offt = getTupleSize(schema);

    struct dataPageHeader page;
    int64_t pagePos = table.lastPage;
    readDataPageHeader(out, pagePos, &page);
    while (page.next != -1 && page.pgLower + sizeof(struct linePointer) >= page.pgUpper - offt) {
        pagePos = page.next;
        readDataPageHeader(out, pagePos, &page);
    }
    if (page.next == -1 && page.pgLower + sizeof(struct linePointer) >= page.pgUpper - offt) {
        pushPageToTable(out, tablePos);
        readDataPageHeader(out, pagePos, &page);
        pagePos = page.next;
        readDataPageHeader(out, pagePos, &page);
        table.pages_cnt += 1;
        table.lastPage = pagePos;
    }

    *data = realloc(*data, offt);
    if(id != NULL){
        memcpy(*((char **) data) + offt - sizeof(table.id), id, sizeof(table.id));
    } else {
        memcpy(*((char **) data) + offt - sizeof(table.id), &table.id, sizeof(table.id));
        table.id += 1;
    }
    addDataToPage(out, pagePos, offt, *data, NULL);
    updateTableHeader(out, tablePos, &table);
    schemaDestruct(schema);
    return WRITE_OK;
}
