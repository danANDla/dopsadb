import struct
import matplotlib.pyplot as plt


def readGraphPoints(fname):
    f = open(fname, mode="rb")
    xPoint = []
    yPoint = []
    while True:
        b = f.read(4)
        if b == '' or len(b) < 4:
            break
        t = struct.unpack('f', b)[0]
        b = f.read(4)
        tuplesN = struct.unpack('i', b)[0]
        # b = f.read(8)
        # fileSZ = struct.unpack('l', b)[0]
        print(tuplesN)
        xPoint.append(tuplesN)
        yPoint.append(t)
    return xPoint, yPoint


def plot(x, y):
    plt.xlabel('number of rows')
    plt.ylabel('size of file, b')
    plt.plot(x, y)

    plt.show()


if __name__ == '__main__':
    x, y = readGraphPoints("graphPoints/deleteGraph")
    lesX = [x[j] for j in range(0, len(x), 1)]
    lesY = [y[j] for j in range(0, len(x), 1)]
    plot(lesX, lesY)
