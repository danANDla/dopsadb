//
// Created by danandla on 3/16/23.
//

#include <mytypes.h>
#include <stdlib.h>
#include <string.h>

#define BUFF_SZ 20

char *getDataTypeStr(dataType t) {
    char *out = malloc(sizeof(char) * BUFF_SZ);
    switch (t) {
        case INT32_T:
            strcpy(out, "int32");
            break;
        case FLOAT_T:
            strcpy(out, "float");
            break;
        case STRING_T:
            strcpy(out, "string");
            break;
        case BOOL_T:
            strcpy(out, "bool");
            break;
    }
    return out;
}

dataType getDataType(const char* str){
    if(strcmp(str, "int32") == 0){
        return INT32_T;
    }
    if(strcmp(str, "float") == 0){
        return FLOAT_T;
    }
    if(strcmp(str, "string") == 0){
        return STRING_T;
    }
    if(strcmp(str, "bool") == 0){
        return BOOL_T;
    }
}

char *getOpTypeStr(opType t){
    char *out = malloc(sizeof(char) * BUFF_SZ);
    switch (t){
        case GR:
            strcpy(out, ">");
            break;
        case LE:
            strcpy(out, "<");
            break;
        case NEQ:
            strcpy(out, "!=");
            break;
        case EQ:
            strcpy(out, "=");
            break;
        case AND:
            strcpy(out, "and");
            break;
        case OR:
            strcpy(out, "or");
            break;
        case NOT:
            strcpy(out, "not");
            break;
    }
    return out;
}

opType getOpType(const char* str){
    if(strcmp(str, "<") == 0){
        return LE;
    }
    if(strcmp(str, ">") == 0){
        return GR;
    }
    if(strcmp(str, "=") == 0){
        return EQ;
    }
    if(strcmp(str, "!=") == 0){
        return NEQ;
    }
    if(strcmp(str, "and") == 0){
        return AND;
    }
    if(strcmp(str, "OR") == 0){
        return OR;
    }
    if(strcmp(str, "not") == 0){
        return NOT;
    }
}

char *getStatementTypeStr(statementType t) {
    char *out = malloc(sizeof(char) * BUFF_SZ);
    switch (t) {
        case UNARY:
            strcpy(out, "unary");
            break;
        case BINARY:
            strcpy(out, "binary");
            break;
        case COLUMN:
            strcpy(out, "column");
            break;
        case IMMVAL:
            strcpy(out, "immediate");
            break;
    }
    return out;
}

statementType getStatementType(const char* str){
    if(strcmp(str, "immediate") == 0){
        return IMMVAL;
    }
    if(strcmp(str, "column") == 0){
        return COLUMN;
    }
    if(strcmp(str, "binary") == 0){
        return BINARY;
    }
    if(strcmp(str, "unary") == 0){
        return UNARY;
    }
}

char *getAstTypeStr(astType t) {
    char *out = malloc(sizeof(char) * BUFF_SZ);
    switch (t) {
        case AST_NODE:
            strcpy(out, "astNode");
            break;
        case AST_COLUMN:
            strcpy(out, "astColumn");
            break;
        case AST_COLUMN_LIST:
            strcpy(out, "astColumnList");
            break;
        case AST_COLUMNS:
            strcpy(out, "astColumns");
            break;
        case AST_TABLE:
            strcpy(out, "astTable");
            break;
        case AST_JOIN:
            strcpy(out, "astJoin");
            break;
        case AST_JOIN_LIST:
            strcpy(out, "astJoinList");
            break;
        case AST_SRC_TABLE:
            strcpy(out, "astSrcTable");
            break;
        case AST_STATEMENT:
            strcpy(out, "astStatement");
            break;
        case AST_SELECT:
            strcpy(out, "astSelect");
            break;
        case AST_VALUE:
            strcpy(out, "astValue");
            break;
    }
    return out;
}
