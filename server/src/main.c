//
// Created by danandla on 3/17/23.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <json-c/json.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <server.h>
#include <jsonEncoder.h>
#include <offline.h>

#define MAX_SIZE 4096
#define ERR_SIZE 1024

int main() {
    int option;
    printf("(1) serverStart "
           "(2) offlineMode\n"
           "(0) exit\n");
    scanf("%d", &option);
    if(option == 2) startMain();
    else if(option != 1) return 0;

    int listenfd = 0, connfd = 0;    //related with the server
    struct sockaddr_in serv_addr;

    uint8_t buf[158];
    memset(&buf, '0', sizeof(buf));
    listenfd = socket(AF_INET, SOCK_STREAM, 0);

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(8888);

    bind(listenfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
    printf("binding\n");

    listen(listenfd, 5);
    printf("listening\n");
    connfd = accept(listenfd, (struct sockaddr *) NULL, NULL);

    printf("Reading from client\n");

    ssize_t r;

    char buff[MAX_SIZE];
    char err[ERR_SIZE];

    for (;;) {
        r = read(connfd, buff, MAX_SIZE);

        if (r == -1) {
            perror("read");
            return EXIT_FAILURE;
        }
        if (r == 0)
            break;

        printf("\n------------------------------------\n");
        ResponseStatus respStatus = workJsonQuery(buff, err, connfd, buff);
        if(respStatus != SERVER_OK){
            printf("[server error] %s\n", err);
            char* resp = makeErrorResp(respStatus, err);
            write(connfd, resp, strlen(resp) + 1);
            free(resp);
        }
        printf("------------------------------------\n");

    }

    return EXIT_SUCCESS;
}
