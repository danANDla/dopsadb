//
// Created by danandla on 3/18/23.
//

#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include <server.h>
#include <jsonDecoder.h>
#include <jsonEncoder.h>
#include <dbManager.h>
#include <iter.h>
#include <join.h>
#include <fileUtil.h>

#define MAX_SIZE 4096


ResponseStatus workJsonSelectNext(char *json, char *err) {
    printf("json received: %s\n", json);
    json_object *jobj = parseJoin(json);
    QueryHeader header = getQueryHeader(jobj);
    if (header == SELECT_NEXT_HEADER) {
        json_object_put(jobj);
        return SERVER_OK;
    } else {
        char e[] = "wrong header, expected move next";
        strcpy(err, e);
        err[strlen(e)] = '\0';
        json_object_put(jobj);
        return SERVER_ERR;
    }
}

ExecStatus workSelectNext(int connfd, char *buff, char *err, char *data, bool isLast) {
    int r = read(connfd, buff, MAX_SIZE);
    if (r == -1) {
        perror("read");
        return EXEC_FAILED;
    }
    if (r == 0) return EXEC_OK;
    ResponseStatus respStatus = workJsonSelectNext(buff, err);
    if (respStatus != SERVER_OK) {
        printf("[server error] %s\n", err);
        const char *resp = makeErrorResp(respStatus, err);
        write(connfd, resp, strlen(resp) + 1);
        return EXEC_FAILED;
    }
    char *resp;
    if (isLast) {
        resp = makeSelectResp(respStatus, SELECT_END, NULL);
    } else {
        resp = makeSelectResp(respStatus, SELECT_NEXT, data);
    }
    write(connfd, resp, strlen(resp) + 1);
    free(resp);
    return EXEC_OK;
}

ExecStatus workSelectQuery(json_object *jobj, char *err, int connfd, char *buff) {
    ExecStatus res;
    AstSelect *select = getSelectQuery(jobj);

    void *resultSet;
    FILE *fd;
    res = execSelectQuery(select, err, &resultSet, &fd);
    bool isJoin = select->srcTable->isJoin;
    bool hasFilter = select->hasFilter;

    if (res == EXEC_FAILED) {
        close_file(fd);
        delete(select);
        return EXEC_FAILED;
    }

    char *selectGood = makeSelectResp(SERVER_OK, SELECT_GOOD, NULL);
    write(connfd, selectGood, strlen(selectGood) + 1);
    free(selectGood);

    char data[MAX_SIZE];
    if (isJoin) {
        if (joinedRowsHeaderToStr((joinOperator*) resultSet, fd, data, MAX_SIZE) != EXEC_OK) {
            char start[] = "internal";
            strcpy(err, start);
            close_file(fd);
            free(resultSet);
            delete(select);
            return EXEC_FAILED;
        }
    } else {
        struct _tbSchema *schema;
        if (hasFilter) schema = ((filterTupleIterator *) resultSet)->schema;
        else schema = ((tupleIterator *) resultSet)->schema;
        if (schemaHeaderToStr(schema, data, MAX_SIZE) != EXIT_SUCCESS) {
            char start[] = "internal";
            strcpy(err, start);
            close_file(fd);
            free(resultSet);
            delete(select);
            return EXEC_FAILED;
        }
    }
    if (workSelectNext(connfd, buff, err, data, false) != EXEC_OK) {
        close_file(fd);
        free(resultSet);
        delete(select);
        return EXEC_FAILED;
    }

    if (isJoin) {
        while (((joinOperator *) resultSet)->moveNext((joinOperator *) resultSet)) {
            if (joinedRowsToStr((joinOperator *) resultSet, fd, data, MAX_SIZE) != EXEC_OK) {
                char start[] = "internal";
                strcpy(err, start);
                close_file(fd);
                free(resultSet);
                delete(select);
                return EXEC_FAILED;
            }
            if (workSelectNext(connfd, buff, err, data, false) != EXEC_OK) {
                close_file(fd);
                free(resultSet);
                delete(select);
                return EXEC_FAILED;
            }
        }
    } else {
        while (((tupleIterator *) resultSet)->base.moveNext((iterator *) resultSet)) {
            struct _tbSchema *schema;
            FILE *filed;
            if (hasFilter) {
                schema = ((filterTupleIterator *) resultSet)->schema;
                filed = ((filterTupleIterator *) resultSet)->fd;
            } else {
                schema = ((tupleIterator *) resultSet)->schema;
                filed = ((tupleIterator *) resultSet)->fd;
            }
            if (rowToStr(schema,
                         *(void **) ((tupleIterator *) resultSet)->base.current,
                         filed, data, MAX_SIZE) != EXIT_SUCCESS) {
                char start[] = "internal";
                strcpy(err, start);
                close_file(fd);
                free(resultSet);
                delete(select);
                return EXEC_FAILED;
            }
            if (workSelectNext(connfd, buff, err, data, false) != EXEC_OK) {
                close_file(fd);
                free(resultSet);
                delete(select);
                return EXEC_FAILED;
            }
        }
    }

    if (workSelectNext(connfd, buff, err, data, true) != EXEC_OK) {
        close_file(fd);
        free(resultSet);
        delete(select);
        return EXEC_FAILED;
    }

    if (isJoin) free(resultSet);
    delete(select);
    close_file(fd);
    return EXEC_OK;
}

ExecStatus workDbQuery(json_object *jobj, char *err, int connfd, char *buff) {
    char *t;
    dbQueryType dbQuery = getDbQueryType(jobj);
    t = getDbQueryStr(dbQuery);
    printf("'%s' dbQuery\n", t);
    free(t);

    if (dbQuery == DBQUERY_SELECT) {
        return workSelectQuery(jobj, err, connfd, buff);
    }
    char e[] = "unsupported dbQueryType";
    strcpy(err, e);
    err[strlen(e)] = '\0';
    return EXEC_FAILED;
}

ResponseStatus workJsonQuery(char *json, char *err, int connfd, char *buff) {
    printf("json received: %s\n\n", json);
    json_object *jobj = parseJoin(json);

    QueryHeader header = getQueryHeader(jobj);
    if (header == DBQUERY_HEADER) {
        if (workDbQuery(jobj, err, connfd, buff) != EXEC_OK) {
            json_object_put(jobj);
            return SERVER_ERR;
        }
    } else if (header == SELECT_NEXT_HEADER) printf("SELECT NEXT\n");
    else {
        char e[] = "unsupported query";
        strcpy(err, e);
        err[strlen(e)] = '\0';
        json_object_put(jobj);
        return SERVER_ERR;
    }
    json_object_put(jobj);
    return SERVER_OK;
}
