//
// Created by danandla on 3/18/23.
//

#include <serverTypes.h>
#include <stdlib.h>
#include <string.h>

char* getHeaderStr(QueryHeader header){
    char *ret = malloc(sizeof(char) * 20);
    switch(header){
        case DBQUERY_HEADER:
            strcpy(ret, "dbQuery");
            break;
        case SELECT_NEXT_HEADER:
            strcpy(ret, "invalid");
            break;
        case INVALID_QUERY_HEADER:
            strcpy(ret, "invalid");
            break;
        default:
            strcpy(ret, "invalid");
            break;
    }
    return ret;
}

char* getDbQueryStr(dbQueryType type){
    char *ret = malloc(sizeof(char) * 20);
    switch(type){
        case DBQUERY_SELECT:
            strcpy(ret, "select");
            break;
        case INVALID_DBQUERY:
            strcpy(ret, "invalid");
            break;
        default:
            strcpy(ret, "invalid");
            break;
    }
    return ret;
}

char* getResponseStatusStr(ResponseStatus st){
    char *ret = malloc(sizeof(char) * 20);
    switch (st){
        case SERVER_OK:
            strcpy(ret, "ok");
            break;
        case SERVER_ERR:
            strcpy(ret, "error");
            break;
    }
    return ret;
}

char* getResponseStr(Response st){
    char *ret = malloc(sizeof(char) * 20);
    switch (st){
        case SELECT_GOOD:
            strcpy(ret, "select_good");
            break;
        case SELECT_NEXT:
            strcpy(ret, "select_next");
            break;
        case SELECT_END:
            strcpy(ret, "select_end");
            break;
    }
    return ret;
}
