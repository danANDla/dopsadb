//
// Created by danandla on 3/22/23.
//

#include <offline.h>

#include <stdio.h>

#include <test.h>
#include <performanceTest.h>
#include <basicOperations.h>

void startMain() {
    char fname[] = "/home/danandla/BOTAY/llpYuko/dbApp/cmake-build-debug/database";

    int option = -1;

    char* dataFileName = "bufferCocktails";

    while (option != 0){
        printf("(1) initDb "
               "(2) superHeader "
               "(3) getTableById\n"
               "(4) select "
               "(5) update "
               "(6) delete "
               "(7) join "
               "(8) joinTree "
               "(9) fill3TablesCocktails"
               "(99) fillTablesNorthwind\n"
               "(10) insertTest "
               "(11) insertTestGraph "
               "(12) selectTestGraph "
               "(13) updateTestGraph "
               "(14) deleteTestGraph \n"
               "(0) exit\n");
        scanf("%d", &option);
        if(option == 0) return;

        enum test_status testRes;
        switch (option) {
            case 1 : {
                testRes = init_db(fname, 4096);
                break;
            }
            case 2 : {
                testRes = seeSuperHeader(fname);
                break;
            }
            case 3:{
                int id;
                scanf("%d", &id);
                testRes = getTable(fname, id);
                break;
            }
            case 4:{
                int id;
                scanf("%d", &id);
                testRes = doSelectQuery(fname, id);
                break;
            }
            case 5:{
                testRes = doUpdateQuery(fname, 0);
                break;
            }
            case 6:{
                testRes = doDeleteQuery(fname, 0);
                break;
            }
            case 7:{
                testRes = joinCheck(fname);
                break;
            }
            case 8:{
                testRes = joinTreeCheck(fname);
                break;
            }
            case 9:{
                testRes = fill3TablesCocktails();
                break;
            }
            case 99:{
                testRes = fillTablesNorthwind();
                break;
            }
            case 10 : {
                int32_t id;
                scanf("%d", &id);
                testRes = insertTest(dataFileName, getCocktailsSchema(), 1, &id);
                break;
            }
            case 11: {
                int32_t id;
                scanf("%d", &id);
                char* outputFileName = "insertGraph";
                testRes = graphPointsInsertTest(dataFileName, outputFileName, getCocktailsSchema(), id);
                break;
            }
            case 12: {
                int32_t id;
                scanf("%d", &id);
                char* outputFileName = "selectGraph";
                testRes = graphPointsSelectTest(dataFileName, outputFileName, getCocktailsSchema(), id);
                break;
            }
            case 13: {
                int32_t id;
                scanf("%d", &id);
                char* outputFileName = "updateGraph";
                testRes = graphPointsUpdateTest(dataFileName, outputFileName, getCocktailsSchema(), id);
                break;
            }
            case 14: {
                int32_t id;
                scanf("%d", &id);
                char* outputFileName = "deleteGraph";
                testRes = graphPointsDeleteTest(dataFileName, outputFileName, getCocktailsSchema(), id);
                break;
            }
            default:{
                break;
            }
        }
        switch (testRes) {
            case TEST_OK:
                printf("test ok \n");
                break;
            case TEST_ERROR:
                printf("test error \n");
                break;
            case TEST_FAIL:
                printf("test fail \n");
                break;
            default:
                printf("test hasn't executed\n");
                break;
        }
    }

    return;
}
