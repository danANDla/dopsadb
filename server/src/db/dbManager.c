//
// Created by danandla on 3/18/23.
//
//

#include <string.h>

#include <ast.h>
#include <dbManager.h>
#include <table_util.h>
#include <join.h>

ExecStatus canGetTableByName(FILE *in, char *tname, struct tableHeader *table, int64_t *tablePos, uint64_t *id) {
    if (getTableIdByName(in, tname, id) != READ_OK ||
        getTableById(in, table, *id, tablePos) != READ_OK)
        return EXEC_FAILED;
    return EXEC_OK;
}

ExecStatus checkTable(FILE *out, AstTable *asttable, char *err) {
    struct tableHeader table;
    int64_t tablePos;
    uint64_t tableId;
    char *tname = asttable->name;
    if (canGetTableByName(out, tname, &table, &tablePos, &tableId) != EXEC_OK) {
        char start[] = "can't find table with '";
        char *tmp = malloc(sizeof(char) * (strlen(start) + 1));
        strcpy(tmp, start);
        tmp = concat(tmp, tname);
        tmp = concat(tmp, "' name");
        strcpy(err, tmp);
        free(tmp);
        return EXEC_FAILED;
    }
    return EXEC_OK;
}

ExecStatus checkJoinList(FILE *out, AstJoinList *jlist, char *err) {
    for (uint16_t i = 0; i < jlist->joinsNumber; ++i) {
        AstJoin *join = jlist->list[i];
        if (strcmp("left", join->left->name) != 0)
            if (checkTable(out, join->left, err) != EXEC_OK) return EXEC_FAILED;
        if (checkTable(out, join->right, err) != EXEC_OK) return EXEC_FAILED;
    }
    return EXEC_OK;
}

ExecStatus checkSrcTable(FILE *out, AstSrcTable *srcTable, char *err) {
    if (checkTable(out, srcTable->src, err) != EXEC_OK) return EXEC_FAILED;
    if (srcTable->isJoin) {
        if (checkJoinList(out, srcTable->joinList, err) != EXEC_OK) return EXEC_FAILED;
    }
    return EXEC_OK;
}

ExecStatus canGetColumnByName(FILE *in, char *cname, uint64_t tableId) {
    uint16_t cId;
    if (getColumnIdByName(in, tableId, cname, &cId) != READ_OK) return EXEC_FAILED;
    return EXEC_OK;
}

ExecStatus isCorrectTableInQuerySrc(AstColumn *column, AstSrcTable *src, char *err) {
    bool isOk = false;

    if (strcmp(src->src->name, column->tableName) == 0) isOk = true;
    else if (src->isJoin) {
        for (uint16_t j = 0; j < src->joinList->joinsNumber; ++j) {
            char *jname = src->joinList->list[j]->left->name;
            if (strcmp(column->tableName, jname) == 0) isOk = true;
            jname = src->joinList->list[j]->right->name;
            if (strcmp(column->tableName, jname) == 0) isOk = true;
        }
    }
    if (!isOk) {
        char start[] = "table '";
        char *tmp = malloc(sizeof(char) * (strlen(start) + 1));
        strcpy(tmp, start);
        tmp = concat(tmp, column->tableName);
        tmp = concat(tmp, "' from  column '");
        tmp = concat(tmp, column->columnName);
        tmp = concat(tmp, ".");
        tmp = concat(tmp, column->tableName);
        tmp = concat(tmp, "' not found in source/join tables");
        strcpy(err, tmp);
        free(tmp);
        return EXEC_FAILED;
    }
    return EXEC_OK;
}

ExecStatus checkColumns(FILE *in, AstColumns *columns, AstSrcTable *src, char *err) {
    if (columns->isAll) return EXEC_OK;

    AstColumnList *clist = columns->columns;
    for (uint16_t i = 0; i < clist->columnsNumber; ++i) {
        AstColumn *column = clist->list[i];
        uint64_t tableId;
        char *tname;
        char *cname = column->columnName;
        if (src->isJoin) {
            if (!column->isTableNamed) {
                char start[] = "column '";
                char *tmp = malloc(sizeof(char) * (strlen(start) + 1));
                strcpy(tmp, start);
                tmp = concat(tmp, cname);
                tmp = concat(tmp, "' without tableName in multiTable query (unsupported operation )");
                strcpy(err, tmp);
                free(tmp);
                return EXEC_FAILED;
            }
            if (isCorrectTableInQuerySrc(column, src, err) != EXEC_OK) return EXEC_FAILED;
            tname = column->tableName;
        } else {
            tname = src->src->name;
        }
        getTableIdByName(in, tname, &tableId);

        if (canGetColumnByName(in, cname, tableId) != EXEC_OK) {
            char start[] = "can't find column with '";
            char *tmp = malloc(sizeof(char) * (strlen(start) + 1));
            strcpy(tmp, start);
            tmp = concat(tmp, cname);
            tmp = concat(tmp, "' name in table '");
            tmp = concat(tmp, tname);
            tmp = concat(tmp, "'");
            strcpy(err, tmp);
            free(tmp);
            return EXEC_FAILED;
        }
    }
    return EXEC_OK;
}

ExecStatus checkColumn(FILE *in, AstColumn *column, uint64_t tableId, char *tname, char *err) {
    char *cname = column->columnName;
    if (canGetColumnByName(in, cname, tableId) != EXEC_OK) {
        char start[] = "can't find column with '";
        char *tmp = malloc(sizeof(char) * (strlen(start) + 1));
        strcpy(tmp, start);
        tmp = concat(tmp, cname);
        tmp = concat(tmp, "' name in table '");
        tmp = concat(tmp, tname);
        tmp = concat(tmp, "'");
        strcpy(err, tmp);
        free(tmp);
        return EXEC_FAILED;
    }
    return EXEC_OK;
}

ExecStatus checkColumnType(FILE *in, AstValue *val, AstColumn *column, uint64_t tableId, char *err) {
    char *cname = column->columnName;
    uint16_t cId;
    if (getColumnIdByName(in, tableId, cname, &cId) != READ_OK) return EXEC_FAILED;
    struct _tbSchema *schema;
    if (getSchemaByTableId(in, tableId, &schema) != READ_OK) return EXEC_FAILED;

    switch (schema->fields[cId]) {
        case FIELD_TYPE_STRING: {
            if (val->dtype != STRING_T) {
                char start[] = "comparing value should be of type string";
                strcpy(err, start);
                schemaDestruct(schema);
                return EXEC_FAILED;
            }
            break;
        }
        case FIELD_TYPE_INT32: {
            if (val->dtype != INT32_T) {
                char start[] = "comparing value should be of type int";
                strcpy(err, start);
                schemaDestruct(schema);
                return EXEC_FAILED;
            }
            break;
        }
        case FIELD_TYPE_FLOAT32: {
            if (val->dtype != FLOAT_T) {
                char start[] = "comparing value should be of type float";
                strcpy(err, start);
                schemaDestruct(schema);
                return EXEC_FAILED;
            }
            break;
        }
        case FIELD_TYPE_BOOL: {
            if (val->dtype != BOOL_T) {
                char start[] = "comparing value should be of type bool";
                strcpy(err, start);
                schemaDestruct(schema);
                return EXEC_FAILED;
            }
            break;
        }
    }
    schemaDestruct(schema);
    return EXEC_OK;
}

bool isGoodSt(AstStatementBinary *st, char *err) {
    AstStatement *lst = st->leftOperand;
    AstStatement *rst = st->rightOperand;
    if (!(lst->type == COLUMN && rst->type == IMMVAL &&
          (((AstStatementBinary *) st)->operation == NEQ ||
           ((AstStatementBinary *) st)->operation == EQ ||
           ((AstStatementBinary *) st)->operation == GR ||
           ((AstStatementBinary *) st)->operation == LE))) {

        char start[] = "wrong format of statement\nexpected: column COMPARE immvalue";
        strcpy(err, start);
        return false;
    } else return true;
}

ExecStatus checkStatement(FILE *out, AstSrcTable *src, AstStatement *st, char *err) {
    if (st->type == BINARY) {
        AstStatement *lst = ((AstStatementBinary *) st)->leftOperand;
        AstStatement *rst = ((AstStatementBinary *) st)->rightOperand;
        if (src->isJoin) {
            if (lst->type == BINARY && rst->type == BINARY) {
                switch (((AstStatementBinary *) st)->operation){
                    case OR || NOT:{
                        char start[] = "wrong format of statement\nexpected: (column COMPARE immvalue) and (column COMPARE immvalue)";
                        strcpy(err, start);
                        return EXEC_FAILED;
                    }
                    default:
                        break;
                }
                switch (((AstStatementBinary *) lst)->operation){
                    case AND || OR || NOT:{
                        char start[] = "wrong format of statement\nexpected: (column COMPARE immvalue) and (column COMPARE immvalue)";
                        strcpy(err, start);
                        return EXEC_FAILED;
                    }
                    default:
                        break;
                }
                switch (((AstStatementBinary *) rst)->operation){
                    case AND || OR || NOT:{
                        char start[] = "wrong format of statement\nexpected: (column COMPARE immvalue) and (column COMPARE immvalue)";
                        strcpy(err, start);
                        return EXEC_FAILED;
                    }
                    default:
                        break;
                }
                if (!isGoodSt((AstStatementBinary *) lst, err)) return EXEC_FAILED;
                AstStatementColumn *lcol = (AstStatementColumn *) ((AstStatementBinary *) lst)->leftOperand;

                if (!isGoodSt((AstStatementBinary *) rst, err)) return EXEC_FAILED;
                AstStatementColumn *rcol = (AstStatementColumn *) ((AstStatementBinary *) rst)->leftOperand;

                if (!(lcol->column->isTableNamed && rcol->column->isTableNamed)) {
                    char *cname;
                    if (!(lcol->column->isTableNamed)) {
                        cname = rcol->column->columnName;
                    } else cname = lcol->column->columnName;
                    char start[] = "column '";
                    char *tmp = malloc(sizeof(char) * (strlen(start) + 1));
                    strcpy(tmp, start);
                    tmp = concat(tmp, cname);
                    tmp = concat(tmp, "' without tableName in multiTable query (unsupported operation )");
                    strcpy(err, tmp);
                    free(tmp);
                    return EXEC_FAILED;
                }
                uint64_t lTable;
                getTableIdByName(out, lcol->column->tableName, &lTable);
                if (checkColumn(out, lcol->column, lTable, lcol->column->tableName, err) != EXEC_OK) return EXEC_FAILED;
                uint64_t rTable;
                getTableIdByName(out, rcol->column->tableName, &rTable);
                if (checkColumn(out, rcol->column, rTable, rcol->column->tableName, err) != EXEC_OK) return EXEC_FAILED;

                AstJoin *join = src->joinList->list[0];
                int checked[2] = {0, 0};
                checked[0] = (strcmp(join->left->name, lcol->column->tableName) == 0) +
                             (strcmp(join->left->name, rcol->column->tableName) == 0);
                checked[1] = (strcmp(join->right->name, lcol->column->tableName) == 0) +
                             (strcmp(join->right->name, rcol->column->tableName) == 0);
                if (!(checked[0] == 1 && checked[1] == 1)) {
                    char start[] = "columns in statement should be from joining tables";
                    strcpy(err, start);
                    return EXEC_FAILED;
                }
                AstStatementImm *lval = (AstStatementImm *) ((AstStatementBinary *) lst)->rightOperand;
                if (checkColumnType(out, lval->value, lcol->column, lTable, err) != EXEC_OK) return EXEC_FAILED;
                AstStatementImm *rval = (AstStatementImm *) ((AstStatementBinary *) rst)->rightOperand;
                if (checkColumnType(out, rval->value, rcol->column, rTable, err) != EXEC_OK) return EXEC_FAILED;
            } else if (isGoodSt((AstStatementBinary *) st, err)) {
                switch (((AstStatementBinary *) st)->operation){
                    case AND || OR || NOT:{
                        char start[] = "wrong format of statement\nexpected: column COMPARE immvalue";
                        strcpy(err, start);
                        return EXEC_FAILED;
                    }
                    default:
                        break;
                }
                AstStatementColumn *col = (AstStatementColumn *) ((AstStatementBinary *) st)->leftOperand;
                if (!col->column->isTableNamed) {
                    char *cname = col->column->columnName;
                    char start[] = "column '";
                    char *tmp = malloc(sizeof(char) * (strlen(start) + 1));
                    strcpy(tmp, start);
                    tmp = concat(tmp, cname);
                    tmp = concat(tmp, "' without tableName in multiTable query (unsupported operation )");
                    strcpy(err, tmp);
                    free(tmp);
                    return EXEC_FAILED;
                }
                uint64_t table;
                getTableIdByName(out, col->column->tableName, &table);
                if (checkColumn(out, col->column, table, col->column->tableName, err) != EXEC_OK) return EXEC_FAILED;
                AstJoin *join = src->joinList->list[0];
                int checked[2] = {0, 0};
                checked[0] = (strcmp(join->left->name, col->column->tableName) == 0);
                checked[1] = (strcmp(join->right->name, col->column->tableName) == 0);
                if (checked[0] == 0 && checked[1] == 0) {
                    char start[] = "column in statement should be from one of the joining tables";
                    strcpy(err, start);
                    return EXEC_FAILED;
                }
                AstStatementImm *val = (AstStatementImm *) ((AstStatementBinary *) st)->rightOperand;
                if (checkColumnType(out, val->value, col->column, table, err) != EXEC_OK) return EXEC_FAILED;
            } else return EXEC_FAILED;
        } else {
            switch (((AstStatementBinary *) st)->operation){
                case AND || OR || NOT:{
                    char start[] = "wrong format of statement\nexpected: column COMPARE immvalue";
                    strcpy(err, start);
                    return EXEC_FAILED;
                }
                default:
                    break;
            }
            if (!isGoodSt((AstStatementBinary *) st, err)) return EXEC_FAILED;
            AstStatementColumn *col = (AstStatementColumn *) ((AstStatementBinary *) st)->leftOperand;
            uint64_t table;
            if (col->column->isTableNamed) {
                getTableIdByName(out, col->column->tableName, &table);
                if (checkColumn(out, col->column, table, col->column->tableName, err) != EXEC_OK) return EXEC_FAILED;
            } else {
                getTableIdByName(out, src->src->name, &table);
                if (checkColumn(out, col->column, table, src->src->name, err) != EXEC_OK) return EXEC_FAILED;
            }
            AstStatementImm *val = (AstStatementImm *) ((AstStatementBinary *) st)->rightOperand;
            if (checkColumnType(out, val->value, col->column, table, err) != EXEC_OK) return EXEC_FAILED;
        }
    }
    return EXEC_OK;
}

fpTupleTest getTestFromAst(AstStatement *_st) {
    AstStatementBinary *st = (AstStatementBinary *) _st;
    AstStatementImm *val = (AstStatementImm *) st->rightOperand;
    switch (val->value->dtype) {
        case STRING_T: {
            if (st->operation == EQ) return strEqualFieldTest;
            else return strNotEqualFieldTest;
            break;
        }
        case INT32_T: {
            if (st->operation == EQ) return intEqualFieldTest;
            else if (st->operation == NEQ) return intNotEqualFieldTest;
            else if (st->operation == GR) return intGreaterThanParamFieldTest;
            else if (st->operation == LE) return intLessThanParamFieldTest;
            else return intNotEqualFieldTest;
            break;
        }
        case FLOAT_T: {
            if (st->operation == EQ) return floatEqualFieldTest;
            else if (st->operation == NEQ) return floatNotEqualFieldTest;
            else if (st->operation == GR) return floatGreaterThanParamFieldTest;
            else if (st->operation == LE) return floatLessThanParamFieldTest;
            else return floatNotEqualFieldTest;
            break;
        }
        case BOOL_T: {
            if (st->operation == EQ) return boolEqualFieldTest;
            else return boolNotEqualFieldTest;
            break;
        }
    }
}

ExecStatus execSelectQuery(AstSelect *select, char *err, void **iter, FILE **out) {
    char dbname[] = "/home/danandla/BOTAY/llpYuko/dbApp/cmake-build-debug/database";
//    init_db(dbname, 4096);
    open_u_file(dbname, out);
//    fill3Tables();
    if (checkSrcTable(*out, select->srcTable, err) != EXEC_OK) return EXEC_FAILED;
    if (checkColumns(*out, select->columns, select->srcTable, err) != EXEC_OK) return EXEC_FAILED;
    if (select->hasFilter)
        if (checkStatement(*out, select->srcTable, select->filter, err) != EXEC_OK) return EXEC_FAILED;

    if (select->srcTable->isJoin && select->srcTable->joinList->joinsNumber > 1) {
        char start[] = "multiple joins unsupported";
        strcpy(err, start);
        return EXEC_FAILED;
    } else if (!(select->columns->isAll)) {
        char start[] = "variable columns unsupported";
        strcpy(err, start);
        return EXEC_FAILED;
    }


    if (select->srcTable->isJoin) {
        uint64_t leftTableId;
        getTableIdByName(*out, select->srcTable->joinList->list[0]->left->name, &leftTableId);
        struct tableHeader leftTable;
        int64_t leftTablePos;
        getTableById(*out, &leftTable, leftTableId, &leftTablePos);

        uint64_t rightTableId;
        getTableIdByName(*out, select->srcTable->joinList->list[0]->right->name, &rightTableId);
        struct tableHeader rightTable;
        int64_t rightTablePos;
        getTableById(*out, &rightTable, rightTableId, &rightTablePos);

        uint16_t leftColumn;
        getColumnIdByName(*out, leftTableId, select->srcTable->joinList->list[0]->leftColumn->columnName, &leftColumn);
        uint16_t rightColumn;
        getColumnIdByName(*out, rightTableId, select->srcTable->joinList->list[0]->rightColumn->columnName,
                          &rightColumn);

        tupleIterator *leftTuple;
        tupleIterator *rightTuple;
        tupleIterator *leftTupleIt = tupleIt(leftTablePos, *out);
        tupleIterator *rightTupleIt = tupleIt(rightTablePos, *out);
        bool isLeftFilter = false;
        bool isRightFilter = false;
        filterInfo leftInfo;
        filterInfo rightInfo;
        leftTuple = leftTupleIt;
        rightTuple = rightTupleIt;
        if (select->hasFilter) {
            AstStatement *lst = ((AstStatementBinary *) select->filter)->leftOperand;
            AstStatement *rst = ((AstStatementBinary *) select->filter)->rightOperand;
            if (lst->type == COLUMN) {
                AstColumn *column = ((AstStatementColumn *) lst)->column;
                AstValue *val = ((AstStatementImm *) rst)->value;
                if (strcmp(column->tableName, select->srcTable->joinList->list[0]->left->name) == 0) {
                    isLeftFilter = true;
                    uint16_t filtercolId;
                    getColumnIdByName(*out, leftTableId, column->columnName, &filtercolId);
                    leftInfo = (filterInfo) {.test = getTestFromAst(
                            select->filter), .param = val->data, .colId = filtercolId};
                    leftTuple = filterTupleIt(leftTupleIt, leftInfo.test, leftInfo.colId, leftInfo.param);
                } else {
                    isRightFilter = true;
                    uint16_t filtercolId;
                    getColumnIdByName(*out, rightTableId, column->columnName, &filtercolId);
                    rightInfo = (filterInfo) {.test = getTestFromAst(
                            select->filter), .param = val->data, .colId = filtercolId};
                    rightTuple = filterTupleIt(rightTupleIt, rightInfo.test, rightInfo.colId, rightInfo.param);
                }
            } else {
                AstColumn *lcolumn = ((AstStatementColumn *) ((AstStatementBinary *) lst)->leftOperand)->column;
                AstValue *lval = ((AstStatementImm *) ((AstStatementBinary *) lst)->rightOperand)->value;
                if (strcmp(lcolumn->tableName, select->srcTable->joinList->list[0]->left->name) == 0) {
                    isLeftFilter = true;
                    uint16_t filtercolId;
                    getColumnIdByName(*out, leftTableId, lcolumn->columnName, &filtercolId);
                    leftInfo = (filterInfo) {.test = getTestFromAst(((AstStatementBinary*)select->filter)->leftOperand),
                                             .param = lval->data, .colId = filtercolId};
                    leftTuple = filterTupleIt(leftTupleIt, leftInfo.test, leftInfo.colId, leftInfo.param);
                } else {
                    isRightFilter = true;
                    uint16_t filtercolId;
                    getColumnIdByName(*out, rightTableId, lcolumn->columnName, &filtercolId);
                    rightInfo = (filterInfo) {.test = getTestFromAst(((AstStatementBinary*)select->filter)->rightOperand),
                                              .param = lval->data, .colId = filtercolId};
                    rightTuple = filterTupleIt(rightTupleIt, rightInfo.test, rightInfo.colId, rightInfo.param);
                }

                AstColumn *rcolumn = ((AstStatementColumn *) ((AstStatementBinary *) rst)->leftOperand)->column;
                AstValue *rval = ((AstStatementImm *) ((AstStatementBinary *) rst)->rightOperand)->value;
                if (strcmp(rcolumn->tableName, select->srcTable->joinList->list[0]->left->name) == 0) {
                    isLeftFilter = true;
                    uint16_t filtercolId;
                    getColumnIdByName(*out, leftTableId, rcolumn->columnName, &filtercolId);
                    leftInfo = (filterInfo) {.test = getTestFromAst(((AstStatementBinary*)select->filter)->leftOperand),
                            .param = rval->data, .colId = filtercolId};
                    leftTuple = filterTupleIt(leftTupleIt, leftInfo.test, leftInfo.colId, leftInfo.param);
                } else {
                    isRightFilter = true;
                    uint16_t filtercolId;
                    getColumnIdByName(*out, rightTableId, rcolumn->columnName, &filtercolId);
                    rightInfo = (filterInfo) {.test = getTestFromAst(((AstStatementBinary*)select->filter)->rightOperand),
                            .param = rval->data, .colId = filtercolId};
                    rightTuple = filterTupleIt(rightTupleIt, rightInfo.test, rightInfo.colId, rightInfo.param);
                }
            }
        }

        *iter = malloc(sizeof(joinOperator));
        joinStatus st = joinInit((joinOperator *) *iter,
                                 leftTuple,
                                 rightTuple,
                                 leftColumn,
                                 rightColumn,
                                 isLeftFilter,
                                 isRightFilter,
                                 leftInfo,
                                 rightInfo);

        if (st == JOIN_DIFFERENT_COLUMN_TYPES) {
            char start[] = "columns in join should have same types";
            strcpy(err, start);
            free(*iter);
            return EXEC_FAILED;
        } else if (st == JOIN_ERR) {
            char start[] = "joinInit internal error";
            strcpy(err, start);
            free(*iter);
            return EXEC_FAILED;
        }
    } else {
        uint64_t tableId;
        getTableIdByName(*out, select->srcTable->src->name, &tableId);
        struct tableHeader table;
        int64_t tablePos;
        getTableById(*out, &table, tableId, &tablePos);
        if (select->hasFilter) {
            tupleIterator *from = tupleIt(tablePos, *out);
            uint16_t n;
            getColumnIdByName(*out, tableId,
                              ((AstStatementColumn *) ((AstStatementBinary *) (select->filter))->leftOperand)->column->columnName,
                              &n);
            *iter = filterTupleIt(from, getTestFromAst(select->filter), n,
                                  ((AstStatementImm *) ((AstStatementBinary *) (select->filter))->rightOperand)->value->data);
        } else *iter = tupleIt(tablePos, *out);
    }
    return EXEC_OK;
};
