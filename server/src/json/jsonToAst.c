//
// Created by danandla on 3/18/23.
//

#include <jsonToAst.h>
#include <string.h>
#include <stdio.h>

AstColumn *jsonToColumn(json_object *jobj) {
    AstColumn *col;
    json_object *cname = json_object_object_get(jobj, "name");

    json_object *tname = json_object_object_get(jobj, "table");
    if (tname != NULL) {
        col = new(&_AstColumn, AST_COLUMN,
                  json_object_get_string(cname), 1,
                  json_object_get_string(tname));
    } else {
        col = new(&_AstColumn, AST_COLUMN,
                  json_object_get_string(cname), 0);
    }
    return col;
}

AstTable *jsonToTable(json_object *jobj) {
    AstTable *table;
    json_object *tname = json_object_object_get(jobj, "name");
    table = new(&_AstTable, AST_TABLE, json_object_get_string(tname));
    return table;
}

AstJoin *jsonToJoin(json_object *jobj) {
    AstJoin *join;

    json_object *tname = json_object_object_get(jobj, "columnType");

    AstTable *ltable = jsonToTable(json_object_object_get(jobj, "leftTable"));
    AstTable *rtable = jsonToTable(json_object_object_get(jobj, "rightTable"));
    AstColumn *lcolumn = jsonToColumn(json_object_object_get(jobj, "leftColumn"));
    AstColumn *rcolumn = jsonToColumn(json_object_object_get(jobj, "rightColumn"));

    join = new(&_AstJoin, AST_JOIN,
               getDataType(json_object_get_string(tname)),
               AST_TABLE, ltable->name,
               AST_TABLE, rtable->name,
               AST_COLUMN, lcolumn->columnName, 1, lcolumn->tableName,
               AST_COLUMN, rcolumn->columnName, 1, rcolumn->tableName);

    delete(lcolumn);
    delete(rcolumn);
    delete(ltable);
    delete(rtable);

    return join;
}

AstJoinList *jarrayToJoinList(json_object *jarray) {
    AstJoinList *jlist;

    AstJoin *first = jsonToJoin(json_object_array_get_idx(jarray, 0));

    jlist = new(&_AstJoinList, AST_JOIN_LIST, 1,
                AST_JOIN, first->columnType,
                AST_TABLE, first->left->name,
                AST_TABLE, first->right->name,
                AST_COLUMN, first->leftColumn->columnName, 1, first->leftColumn->tableName,
                AST_COLUMN, first->rightColumn->columnName, 1, first->rightColumn->tableName);

    delete(first);
    for(int i = 1; i < json_object_array_length(jarray); ++i){
        AstJoin* j = jsonToJoin(json_object_array_get_idx(jarray, i));
        jlist->push(jlist, j);
        delete(j);
    }
    return jlist;
}

AstSrcTable *jsonToSrcTable(json_object *jobj){
    AstSrcTable *src;
    AstTable* table = jsonToTable(json_object_object_get(jobj, "sourceTable"));
    json_object *joins = json_object_object_get(jobj, "joins");
    if(joins != NULL){
        AstJoinList* jlist = jarrayToJoinList(joins);
        src = new(&_AstSrcTable, AST_SRC_TABLE, 1, table, 1, jlist);
        delete(jlist);
    } else{
        src = new(&_AstSrcTable, AST_SRC_TABLE, 1, table, 0);
    }
    delete(table);
    return src;
}

AstColumnList* jarrayToColumnList(json_object* jarray){
    AstColumnList *clist;

    AstColumn *first = jsonToColumn(json_object_array_get_idx(jarray, 0));

    if(first->isTableNamed){
        clist = new(&_AstColumnList, AST_COLUMN_LIST, 1,
                    AST_COLUMN, first->columnName, 1, first->tableName);
    } else{
        clist = new(&_AstColumnList, AST_COLUMN_LIST, 1,
                    AST_COLUMN, first->columnName, 0);
    }
    delete(first);

    for(int i = 1; i < json_object_array_length(jarray); ++i){
        AstColumn* c = jsonToColumn(json_object_array_get_idx(jarray, i));
        clist->push(clist, c);
        delete(c);
    }
    return clist;
}

AstColumns* jsonToColumns(json_object* jobj){
    AstColumns* columns;
    json_object* jIsAll = json_object_object_get(jobj, "isAll");
    const char* isAll = json_object_get_string(jIsAll);
    if(strcmp(isAll, "yes") == 0){
        columns = new(&_AstColumns, AST_COLUMNS, 1, 1);
    } else{
        json_object* columnList = json_object_object_get(jobj, "columnList");
        AstColumnList* clist = jarrayToColumnList(columnList);
        columns = new(&_AstColumns, AST_COLUMNS, 1, 0, clist);
        delete(clist);
    }
    return columns;
}

AstStatementImm* jsonToStImm(json_object* jobj){
    json_object* jdtype = json_object_object_get(jobj, "dtype");
    json_object* jdata = json_object_object_get(jobj, "data");
    dataType dtype = getDataType(json_object_get_string(jdtype));
    AstValue* val;
    switch (dtype){
        case STRING_T:{
            const char* str = json_object_get_string(jdata);
            val = new(&_AstValue, AST_VALUE, STRING_T, str);
            break;
        }
        case INT32_T:{
            int n = json_object_get_int(jdata);
            val = new(&_AstValue, AST_VALUE, INT32_T, n);
            break;
        }
        case FLOAT_T:{
            float f = json_object_get_double(jdata);
            val = new(&_AstValue, AST_VALUE, FLOAT_T, f);
            break;
        }
        case BOOL_T:{
            int n = json_object_get_int(jdata);
            val = new(&_AstValue, AST_VALUE, BOOL_T, n);
            break;
        }
    }
    AstStatementImm* imm = new(&_AstStatementImm, AST_STATEMENT, IMMVAL, val);
    delete(val);
    return imm;
}

json_object* jsonToStColumn(json_object* jobj){
    AstStatementColumn* col;
    json_object* jdata = json_object_object_get(jobj, "data");
    AstColumn* column = jsonToColumn(jdata);
    if(column->isTableNamed){
        col = new(&_AstStatementColumn, AST_STATEMENT, COLUMN,
                  AST_COLUMN, column->columnName, 1, column->tableName);
    } else{
        col = new(&_AstStatementColumn, AST_STATEMENT, COLUMN,
                  AST_COLUMN, column->columnName, 0);
    }
    delete(column);
    return col;
}

AstStatementBinary* jsonToStBinary(json_object*);

AstStatementUnary* jsonToStUnary(json_object* jobj){
    AstStatementUnary* unary;
    json_object* joptype = json_object_object_get(jobj, "operation");
    opType optype = getOpType(json_object_get_string(joptype));

    json_object* joperandType = json_object_object_get(jobj, "operandType");
    statementType stType = getStatementType(json_object_get_string(joperandType));

    json_object* operand = json_object_object_get(jobj, "operand");
    AstStatement* st;
    switch (stType){
        case IMMVAL:
            st = jsonToStImm(operand);
            break;
        case COLUMN:
            st = jsonToStColumn(operand);
            break;
        case UNARY:
            st = jsonToStUnary(operand);
            break;
        case BINARY:
            st = jsonToStBinary(operand);
            break;
    }

    unary = new(&_AstStatementUnary, AST_STATEMENT, UNARY, 1, optype, st);
    delete(st);
    return unary;
}

AstStatementBinary* jsonToStBinary(json_object* jobj){
    AstStatementBinary* binary;
    json_object* joptype = json_object_object_get(jobj, "operation");
    opType optype = getOpType(json_object_get_string(joptype));

    json_object* ljoperandType = json_object_object_get(jobj, "leftOperandType");
    statementType lstType = getStatementType(json_object_get_string(ljoperandType));
    json_object* loperand = json_object_object_get(jobj, "leftOperand");
    AstStatement* lst;
    switch (lstType){
        case IMMVAL:
            lst = jsonToStImm(loperand);
            break;
        case COLUMN:
            lst = jsonToStColumn(loperand);
            break;
        case UNARY:
            lst = jsonToStUnary(loperand);
            break;
        case BINARY:
            lst = jsonToStBinary(loperand);
            break;
    }

    json_object* rjoperandType = json_object_object_get(jobj, "rightOperandType");
    statementType rstType = getStatementType(json_object_get_string(rjoperandType));
    json_object* roperand = json_object_object_get(jobj, "rightOperand");
    AstStatement* rst;
    switch (rstType){
        case IMMVAL:
            rst = jsonToStImm(roperand);
            break;
        case COLUMN:
            rst = jsonToStColumn(roperand);
            break;
        case UNARY:
            rst = jsonToStUnary(roperand);
            break;
        case BINARY:
            rst = jsonToStBinary(roperand);
            break;
    }

    binary = new(&_AstStatementBinary, AST_STATEMENT, BINARY, 1, optype, lst, rst);
    delete(lst);
    delete(rst);
    return binary;
}

AstStatement* jsonToStatement(json_object* jobj){
    AstStatement* st;

    json_object* jstType = json_object_object_get(jobj, "statementType");
    statementType stType = getStatementType(json_object_get_string(jstType));

    json_object* jst = json_object_object_get(jobj, "statement");

    switch (stType){
        case IMMVAL:
            st = jsonToStImm(jst);
            break;
        case COLUMN:
            st = jsonToStColumn(jst);
            break;
        case UNARY:
            st = jsonToStUnary(jst);
            break;
        case BINARY:
            st = jsonToStBinary(jst);
            break;
    }

    return st;
}

AstSelect* jsonToSelect(json_object* jobj){
    AstSelect* select;
    AstSrcTable* src = jsonToSrcTable(json_object_object_get(jobj, "src"));
    AstColumns* columns = jsonToColumns(json_object_object_get(jobj, "columns"));
    json_object* jfilter = json_object_object_get(jobj, "filter");
    if(jfilter != NULL){
        AstStatement* st = jsonToStatement(jfilter);
        select = new (&_AstSelect, AST_SELECT, 1, columns, src, 1, st);
        delete(st);
    } else {
        select = new (&_AstSelect, AST_SELECT, 1, columns, src, 0);
    }
    delete(src);
    delete(columns);
    return select;
}
