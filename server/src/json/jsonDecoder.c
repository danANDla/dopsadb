//
// Created by danandla on 3/18/23.
//

#include <jsonDecoder.h>
#include <string.h>
#include <json-c/json.h>
#include <stdio.h>
#include <serverTypes.h>
#include <ast.h>
#include <jsonToAst.h>


json_object *parseJoin(char *jsonString) {
    return json_tokener_parse(jsonString);
}

void jsonToDb(char *jsonString) {
}

QueryHeader getQueryHeader(json_object *jobj) {
    json_object *jtype = json_object_object_get(jobj, "method");
    const char *typestr = json_object_get_string(jtype);
    QueryHeader header;
    if (strcmp(typestr, "dbQuery") == 0) {
        header = DBQUERY_HEADER;
    } else if(strcmp(typestr, "selectNext") == 0){
        header = SELECT_NEXT_HEADER;
    } else header = INVALID_QUERY_HEADER;
    return header;
}

const char *getDbQuery(json_object *jobj) {
    json_object *jtype = json_object_object_get(jobj, "body");
    jtype = json_object_object_get(jtype, "query");
    const char *type = json_object_get_string(jtype);
    return type;
}

dbQueryType getDbQueryType(json_object* jobj){
    const char *typestr = getDbQuery(jobj);
    dbQueryType type;

    if (strcmp(typestr, "select") == 0) {
        type = DBQUERY_SELECT;
    } else type = INVALID_DBQUERY;

    return type;
}

AstSelect* getSelectQuery(json_object* jobj){
    return jsonToSelect(json_object_object_get(jobj, "body"));
};
