//
// Created by danandla on 3/18/23.
//

#include <jsonEncoder.h>
#include <json-c/json.h>
#include <string.h>
#include <server.h>

char* makeErrorResp(ResponseStatus status, char* err){
    json_object* jobj = json_object_new_object();
    char * respStr = getResponseStatusStr(status);
    json_object_object_add(jobj, "status", json_object_new_string(respStr));
    free(respStr);
    json_object_object_add(jobj, "errMsg", json_object_new_string(err));
    const char* jstr = json_object_to_json_string(jobj);
    char* out = malloc(sizeof(char) * (strlen(jstr) + 1));
    strcpy(out, jstr);
    json_object_put(jobj);
    return out;
}

char* makeSelectResp(ResponseStatus status, Response resp, char* data){
    json_object* jobj = json_object_new_object();

    char* statusStr = getResponseStatusStr(status);
    char* respStr = getResponseStr(resp);
    json_object_object_add(jobj, "status", json_object_new_string(statusStr));
    json_object_object_add(jobj, "type", json_object_new_string(respStr));
    free(respStr);
    free(statusStr);
    if(resp == SELECT_NEXT){
        json_object_object_add(jobj, "data", json_object_new_string(data));
    }
    const char* jstr = json_object_to_json_string(jobj);
    char* out = malloc(sizeof(char) * (strlen(jstr) + 1));
    strcpy(out, jstr);
    json_object_put(jobj);
    return out;
}
