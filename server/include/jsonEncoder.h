//
// Created by danandla on 3/18/23.
//

#ifndef DOPSADB_JSONENCODER_H
#define DOPSADB_JSONENCODER_H

#include <serverTypes.h>

char* makeErrorResp(ResponseStatus status, char* err);
char* makeSelectResp(ResponseStatus status, Response resp, char* data);
#endif //DOPSADB_JSONENCODER_H
