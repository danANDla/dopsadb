//
// Created by danandla on 3/18/23.
//

#ifndef DOPSADB_JSONTOAST_H
#define DOPSADB_JSONTOAST_H

#include <ast.h>
#include <json-c/json.h>

AstSelect *jsonToSelect(json_object *);
AstSrcTable * jsonToSrcTable(json_object *);


#endif //DOPSADB_JSONTOAST_H
