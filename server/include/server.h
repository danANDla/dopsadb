//
// Created by danandla on 3/18/23.
//

#ifndef DOPSADB_SERVER_H
#define DOPSADB_SERVER_H

#include <serverTypes.h>

ResponseStatus workJsonQuery(char *json, char* err, int connfd, char* buff);

#endif //DOPSADB_SERVER_H
