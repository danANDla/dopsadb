//
// Created by danandla on 3/16/23.
//

#ifndef SQLPARSER_MYTYPES_H
#define SQLPARSER_MYTYPES_H

typedef enum {
    INT32_T,
    FLOAT_T,
    STRING_T,
    BOOL_T
} dataType;

typedef enum {
    AST_COLUMN = 0,
    AST_COLUMN_LIST,
    AST_COLUMNS,
    AST_TABLE,
    AST_JOIN,
    AST_JOIN_LIST,
    AST_SRC_TABLE,
    AST_STATEMENT,
    AST_SELECT,
    AST_NODE,
    AST_VALUE
} astType;

typedef enum {
    IMMVAL,
    COLUMN,
    UNARY,
    BINARY
} statementType;

typedef enum {
    EQ,
    NEQ,
    GR,
    LE,
    AND,
    OR,
    NOT
} opType;


char *getAstTypeStr(astType t);
char *getDataTypeStr(dataType t);
char *getStatementTypeStr(statementType t);
char *getOpTypeStr(opType t);

opType getOpType(const char* str);
dataType getDataType(const char* str);
statementType getStatementType(const char* str);
#endif //SQLPARSER_MYTYPES_H
