//
// Created by danandla on 3/18/23.
//

#ifndef DOPSADB_DBMANAGER_H
#define DOPSADB_DBMANAGER_H

#include <ast.h>
#include <iter.h>

typedef enum ExecStatus{
    EXEC_OK,
    EXEC_FAILED
}ExecStatus;

ExecStatus execSelectQuery(AstSelect *, char *, void**, FILE**);

#endif //DOPSADB_DBMANAGER_H
