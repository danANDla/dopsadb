//
// Created by danandla on 3/18/23.
//

#ifndef DOPSADB_SERVERTYPES_H
#define DOPSADB_SERVERTYPES_H

typedef enum QueryHeader{
    DBQUERY_HEADER = 0,
    SELECT_NEXT_HEADER,
    INVALID_QUERY_HEADER
}QueryHeader;


typedef enum dbQueryType{
    DBQUERY_SELECT = 0,
    INVALID_DBQUERY
}dbQueryType;

typedef enum ResponseStatus{
    SERVER_OK,
    SERVER_ERR
}ResponseStatus;

typedef enum Response{
    SELECT_GOOD,
    SELECT_NEXT,
    SELECT_END
}Response;

char* getHeaderStr(QueryHeader);
char* getDbQueryStr(dbQueryType);
char* getResponseStatusStr(ResponseStatus);
char* getResponseStr(Response);
#endif //DOPSADB_SERVERTYPES_H
