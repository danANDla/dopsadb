//
// Created by danandla on 3/15/23.
//

#ifndef SQLPARSER_OOC_H
#define SQLPARSER_OOC_H

#include <stdlib.h>
#include <stdarg.h>

struct Class {
    size_t size;
    void *(*ctor)(void *self, va_list *app);
    void *(*dtor)(void *self);
    void *(*clone)(const void *self);
};

void *new(const void *_class, ...);
void delete (void * self);

#endif //SQLPARSER_OOC_H
