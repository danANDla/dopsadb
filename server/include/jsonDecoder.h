//
// Created by danandla on 3/18/23.
//

#ifndef DOPSADB_JSONDECODER_H
#define DOPSADB_JSONDECODER_H

#include <json-c/json.h>
#include <server.h>
#include <serverTypes.h>
#include <ast.h>

void jsonToDb(char *);
json_object* parseJoin(char* );

QueryHeader getQueryHeader(json_object*);
dbQueryType getDbQueryType(json_object*);
AstSelect* getSelectQuery(json_object*);

#endif //DOPSADB_JSONDECODER_H
